﻿using System.Web;
using System.Web.Optimization;

namespace LRADPortal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/lodash").Include(
                       "~/Scripts/lodash.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/plugin").Include(
                  "~/Scripts/spectrum.js",
                  "~/Scripts/bootstrap-select.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/angular.js",
                "~/Scripts/angular-ui-router.js",
                "~/Scripts/angular-google-maps.js",
                "~/Scripts/angular-tree-control.js",
                "~/Scripts/angular-strap.js",
                "~/Scripts/angular-strap.tpl.js",
                "~/Scripts/angular-sanitize.js",
                "~/Scripts/angular-messages.js",
                "~/Scripts/angular-websocket.js",
                "~/Scripts/FileAPI.js",
                "~/Scripts/ng-file-upload-shim.js",
                "~/Scripts/ng-file-upload.js",
                "~/Scripts/loading-bar.js",
                "~/Scripts/angular-audio-recorder.js",
                "~/Scripts/app.js",
                "~/Scripts/satellizer.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-controller").Include(
                "~/Scripts/AngularControllers/*.js",
                "~/Scripts/AngularFactories/*.js",
                "~/Scripts/AngularDirectives/*.js",
                "~/Scripts/AngularServices/*.js",
                "~/Scripts/AngularFilters/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                     "~/Scripts/DataTables/jquery.dataTables.js",
                     "~/Scripts/DataTables/dataTables.bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-theme.css",
                      "~/Content/bootstrap-select.css",
                      "~/Content/rzslider.css",
                      "~/Content/tree-control.css",
                      "~/Content/tree-control-attribute.css",
                      "~/Content/spectrum.css",
                      "~/Content/loading-bar.css",
                      "~/Content/sprites.css",
                      "~/Content/fonts.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/DataTables/css").Include(
                      //"~/Content/DataTables/css/jquery.dataTables.css",
                      "~/Content/DataTables/css/dataTables.bootstrap.css",
                      "~/Content/DataTables/css/dataTables.theme.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}
