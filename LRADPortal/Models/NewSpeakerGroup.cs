﻿using System.Collections.Generic;

namespace LRADPortal.Models
{
    public class NewSpeakerGroup
    {
        public string Uid { get; set; }
        public string AreaUid { get; set; }

        public string Name { get; set; }

        public string MultiCastGroup { get; set; }

        public string Color { get; set; }

        public List<Sound> GeneralFilesList { get; set; }

        public List<Sound> EmergencyFilesList { get; set; }

    }
}