﻿using System.Collections.Generic;
using System.Linq;

namespace LRADPortal.Models
{
    public class AreaGroupMap
    {
        public string Uid { get; set; }

        public double? CenterLatitude { get; set; }

        public double? CenterLongitude { get; set; }

        public IQueryable SpeakerGroups { get; set; }

        public bool HasCustomImage { get; set; }

        public IQueryable CustomImageOverlay { get; set; }

        public int? Zoom { get; set; }
    }
}