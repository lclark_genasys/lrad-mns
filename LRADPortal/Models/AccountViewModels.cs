﻿using System;
using System.Collections.Generic;

namespace LRADPortal.Models
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }

    public enum UserStatus
    {
        Active = 0,
        Inactive = 1
    }

    public enum UserAccessLevel
    {
        Administrator = 0,
        Level1User = 1,
        Level2User = 2
    }

    public enum SoundType
    {
        General = 0,
        Emergency = 1
    }
}
