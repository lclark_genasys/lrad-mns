using System;
namespace LRADPortal.Models
{
    public class ApiSpeaker
    {
        public ApiSpeaker()
        {
            
        }
        public ApiSpeaker(Speaker sp)
        {
            Uid = sp.Uid.ToString();
            Name = sp.NetworkName;
            IPAddress = sp.IPAddress;
            MacAddress = sp.MacAddress;
            Longitude = sp.Longitude;
            Latitude = sp.Latitude;
            //if (sp.SpeakerType != null)
            //    SpeakerType = sp.SpeakerType.Name;
        }
        public string Uid { get; set; }
        public string Name { get; set; }
        public string IPAddress { get; set; }
        public string MacAddress { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public string Type { get; set; }
        public string SpeakerType { get; set; }
        public string Status { get; set; }
        public string ConnectionState { get; set; }
    }
}