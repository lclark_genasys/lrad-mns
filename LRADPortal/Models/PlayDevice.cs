﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LRADPortal.Models
{
    public class PlayDevice
    {
        public string Uid { get; set; }
        public string Key { get; set; }
        public short Attenuation { get; set; }
        public bool Repeat { get; set; }
    }
}