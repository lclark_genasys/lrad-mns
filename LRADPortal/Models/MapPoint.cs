﻿namespace LRADPortal.Models
{
    public class MapPoint
    {
        private double _lat = 37.4419;
        private double _lng = -122.1419;

        public double Lat
        {
            get { return _lat; }
            set { _lat = value; }
        }

        public double Lng
        {
            get { return _lng; }
            set { _lng = value; }
        }
    }
}