﻿namespace LRADPortal.Models
{
    public class Sound
    {
        public string Uid { get; set; }

        public string Name { get; set; }

        public string Filename { get; set; }

        public SoundType Type { get; set; }
    }
}