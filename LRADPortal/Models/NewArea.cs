﻿namespace LRADPortal.Models
{
    public class NewArea
    {
        public string Uid { get; set; }

        public string Name { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int Zoom { get; set; }

        public AreaImage AreaImage { get; set; }

    }
}