using System.Collections.Generic;

namespace LRADPortal.Models
{
    public class UserManagementInfo
    {
        public string UserInfoId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public string Uid { get; set; }

        public bool IsSelf { get; set; }

        public UserAccessLevel AccessLevel { get; set; }

        public UserStatus Status { get; set; }

        public IEnumerable<AreaSpeakerGroup> AreaSpeakerList { get; set; }

    }
}