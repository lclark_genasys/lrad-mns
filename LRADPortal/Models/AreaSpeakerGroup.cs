﻿namespace LRADPortal.Models
{
    public class AreaSpeakerGroup
    {
        public string AreaName { get; set; }

        public string AreaUid { get; set; }

        public string SpeakerGroupName { get; set; }

        public string SpeakerGroupUid { get; set; }
      
    }
}