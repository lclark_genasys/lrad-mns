namespace LRADPortal.Models
{
    public class NewSpeaker
    {
        public string Uid { get; set; }
        public string SpeakerGroupUid { get; set; }
        public string Name { get; set; }
        public string NetworkName { get; set; }
        public string IPAddress { get; set; }
        public string SatelliteID { get; set; }
        public byte Type { get; set; }
        public double GeneralVolume { get; set; }
        public double EmergencyVolume { get; set; }
        public double? latitude { get; set; }
        public double? longitude { get; set; }
        public string SoundUid { get; set; }
        public string RepeatTimes { get; set; }
        public System.DateTime? StartTime { get; set; }
        public string PlayMode { get; set; }
        public string TTSUid { get; set; }
        public string MacAddress { get; set; }
        public int PredefinedSound { get; set; }
        public bool Repeat { get; set; }
    }
}