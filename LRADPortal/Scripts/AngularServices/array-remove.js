﻿app.service('removeArray', function () {
    var arraySplicing = function (scopeObj, from, to) {
        var rest = scopeObj.slice((to || from) + 1 || scopeObj.length);
        scopeObj.length = from < 0 ? scopeObj.length + from : from;

        return scopeObj.push.apply(scopeObj, rest);
    };

    return {
        message: arraySplicing
    }
});