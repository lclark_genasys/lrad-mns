﻿(function() {
    'user strict';
    angular
        .module('LRADPortal')
        .service('speakerService', speakerService);

    function speakerService() {
        var self = this;
        this.speakeTypes = {
            0: { id: 0, name: "360X Single", maxCoverageRadius: 560 },
            1: { id: 1, name: "360X Double", maxCoverageRadius: 850 },
            2: { id: 2, name: "360X Quad", maxCoverageRadius: 1300 },
            3: { id: 3, name: "360X Eight", maxCoverageRadius: 2000 },
            4: { id: 4, name: "360Xm Single", maxCoverageRadius: 250 },
            5: { id: 5, name: "360Xm Double", maxCoverageRadius: 450 }
        };
        this.calculateCoverage = function(speaker) {
            var maxRadius = self.speakeTypes[speaker.Type].maxCoverageRadius;
            var volume = speaker.GeneralVolume;
            return Math.round(maxRadius * (Math.pow(10, volume / 20)));
        };
    }
})();
