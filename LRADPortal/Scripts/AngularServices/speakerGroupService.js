﻿(function () {
    'user strict';
    angular
        .module('LRADPortal')
        .service('speakerGroupService', speakerGroupService);

    function speakerGroupService() {
        var self = this;
        this.findGroupbyId = function (groups, groupId) {
            return _.find(groups, function (group) {
                return group.Uid.toUpperCase() === groupId.toUpperCase();
            });
        }
    }
})();