﻿app.factory('lradDeviceService', ['$http', 'lRADDeviceBaseUrl', function ($http, lRADUpdateEndPoint) {

    //var play = function (speakers) {
    //    return $http.post(lRADUpdateEndPoint + "devices/audio/remote/" + "play", [{ "Guid": "c9e61b22-e4e8-4d4e-b58c-75b896e19943", "Key": "General", "Attenuation": -30, "Repeat": false }]).then(function (response) {
    //        return response.data;
    //    });
    //};

    //var stop = function (speakers) {
    //    return $http.post(lRADUpdateEndPoint + "devices/audio/" + "stop", speakers).then(function (response) {
    //        return response.data;
    //    });
    //};
    var urlBase = 'api/Speakers/';
    var play = function (speakers) {
        return $http.post(urlBase + "play", speakers).then(function (response) {
            return response.data;
        });
    };

    var stop = function (speakers) {
        return $http.post(urlBase + "stop", speakers).then(function (response) {
            return response.data;
        });
    };

    var getAllDevices = function() {
        return $http.get(urlBase + "GetAllLradDevices").then(function (response) {
            return response.data;
        });
    }

    var getDeviceById = function(id) {
        return $http.get(urlBase + "GetDeviceById?id="+ id).then(function (response) {
            return response.data;
        });
    }

    return {
        play: play,
        stop: stop,
        getAllDevices: getAllDevices,
        getDeviceById: getDeviceById
    };
}]);