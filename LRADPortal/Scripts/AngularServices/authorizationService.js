﻿app.service('authorizationService', ['$http', '$q', '$auth', function ($http, $q, $auth) {

    this.isAuthorize = function() {
        return $auth.isAuthenticated() && ($auth.getTokenObject().accessLevel == 0);
    };

}]);