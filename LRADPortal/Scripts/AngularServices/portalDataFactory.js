app.factory('areaGroupDataFactory', ['$http', function ($http) {
    var getAllData = function () {
        return $http.get('api/speakergroups/GetAllSpeakerGroup').then(function (response) {
            return response.data;
        });
    };
    var getAllAreas = function() {
        return $http.get('api/areas/GetAllAreas').then(function (response) {
            return response.data;
        });
    };
    var getAreaById = function (params) {
        return $http.get('api/areas/GetAreaById', { params: params }).then(function (response) {
            return response.data;
        });
    };
    var getAssignedAreas = function () {
        return $http.get('api/areas/GetAssignedAreas').then(function (response) {
            return response.data;
        });
    };
    var getAllSounds = function () {
        return $http.get('api/sounds/GetSounds').then(function (response) {
            return response.data;
        });
    };
    var getSound = function (id) {
        return $http.get('api/Sounds/GetSoundApi', { params: { id: id } }).then(function (response) {
            return response.data;
        });
    };
    var uploadSoundFiles = function (soundFiles) {
        return $http.post('api/Sounds/AddNewSounds', soundFiles).then(function (response) {
            return response.data;
        });
    };
    var addNewArea = function (newAreadata) {
        return $http.post('api/Areas/AddNewArea', newAreadata).then(function (response) {
            return response.data;
        });
    };
    var updateArea = function(area) {
        return $http.put('api/Areas/UpdateArea', area).then(function (response) {
            return response.data;
        });
    }
    var deleteArea = function (id) {
        return $http.delete('api/Areas/DeleteArea', { params: { id: id } }).then(function (response) {
            return response.data;
        });
    }
    var addNewGroup = function(newGroupdata) {
        return $http.post('api/speakergroups/AddNewGroup', newGroupdata).then(function (response) {
            return response.data;
        });
    };
    var addNewSpeaker = function(newSpeakerdata) {
        return $http.post('api/speakers/AddNewSpeaker', newSpeakerdata).then(function (response) {
            return response.data;
        });
    };
    var SpeakerExists = function (speakers) {
        return $http.get('api/Speakers/SpeakerExists', { params: { speakers: JSON.stringify(speakers) } }).then(function (resp) {
            return resp.data;
        }, function (error) {
            console.log('Failed SpeakerExists: ', error);
        });
    }
    var GetDiscoveredSpeakers = function () {
        return $http.post('api/Speakers/GetDiscoveredSpeakers').then(function (resp) {
            return resp.data;
        }, function (error) {
            console.log('Failed GetDiscoveredSpeakers: ', error);
        });
    }
    var updateSpeaker = function(newSpeakerdata) {
        return $http.put('api/speakers/UpdateSpeaker', newSpeakerdata).then(function (response) {
            return response;
        });
    };
    var updateSpeakerGroup = function(newSpeakerGroupdata) {
        return $http.put('api/speakergroups/UpdateSpeakerGroup', newSpeakerGroupdata).then(function (response) {
            return response.data;
        });
    };
    var deleteSpeaker = function(id) {
        return $http.delete('api/speakers/DeleteSpeaker', { params: { id: id } }).then(function(response) {
            return response.data;
        });
    };
    var deleteSpeakerGroup = function (id) {
        return $http.delete('api/speakergroups/DeleteSpeakerGroup', { params: {id: id}}).then(function (response) {
            return response.data;
        });
    };
    var isAreaNameExisting = function (params) {
        return $http.get('api/areas/IsExisting', { params: params }).then(function (response) {
            return response.data;
        });
    }
    var getUnassignedSpeakers = function () {
        return $http.get('api/speakers/GetUnassignedSpeakers').then(function (response) {
            return response.data;
        });
    }
    var soundManagement = function () {
        return $http.get('api/Sounds/SoundManagement', {}).then(function (resp) {
            return resp;
        });
    }
    var soundTypes = function () {
        return $http.get('api/Sounds/SoundTypes', {}).then(function (resp) {
            return resp;
        });
    }
    //var deleteSound = function (id) {
    //    return $http.delete('api/Sounds/DeleteSound', { params: { id: id } }).then(function (resp) {
    //        return resp;
    //    });
    //}

    var getGeneralSounds = function () {
        return $http.get('api/Sounds/GetGeneralSounds').then(function (resp) {
            return resp.data;
        });
    };

    var getEmergencySounds = function () {
        return $http.get('api/Sounds/GetEmergencySounds').then(function (resp) {
            return resp.data;
        });
    };

    var customSounds = function (type) {
        return $http.get('api/Sounds/GetCustomSounds', { params: {type: type}}).then(function (resp) {
            return resp.data;
        });
    };
    var getSpeakersCount = function () {
        return $http.get('api/Speakers/Count').then(function (response) {
            return response.data;
        });
    };

    var addNewSpeech = function (speechToTextData) {
        return $http.post('api/Speeches/AddNewSpeech?body=' + speechToTextData).then(function (resp) {
            return resp.data;
        });
    };

    var getSpeech = function (id) {
        return $http.get('api/Speeches/GetSpeech', { params: { id: id } }).then(function (resp) {
            return resp.data;
        });
    };

    return {
        updateArea: updateArea,
        deleteArea:deleteArea,
        isAreaNameExisting: isAreaNameExisting,
        getAllData: getAllData,
        getAllAreas: getAllAreas,
        getAreaById: getAreaById,
        getAssignedAreas: getAssignedAreas,
        getAllSounds: getAllSounds,
        getSound: getSound,
        manageArea: addNewArea,
        manageGroup: addNewGroup,
        manageSpeaker: addNewSpeaker,
        GetDiscoveredSpeakers: GetDiscoveredSpeakers,
        SpeakerExists: SpeakerExists,
        deleteSpeaker: deleteSpeaker,
        updateSpeaker: updateSpeaker,
        getUnassignedSpeakers: getUnassignedSpeakers,
        deleteSpeakerGroup: deleteSpeakerGroup,
        updateSpeakerGroup: updateSpeakerGroup,
        uploadSoundFiles: uploadSoundFiles,
        soundManagement: soundManagement,
        soundTypes: soundTypes,
        //deleteSound: deleteSound,
        getGeneralSounds: getGeneralSounds,
        getEmergencySounds: getEmergencySounds,
        customSounds: customSounds,
        addNewSpeech: addNewSpeech,
        getSpeech: getSpeech,
        getSpeakersCount: getSpeakersCount
    };
}]);

app.factory('userInfoDataFactory', ['$http', function ($http) {
    var urlBase = 'api/account/';

    var getAllUsers = function () {
        return $http.get(urlBase + 'UserManagementInfo').then(function (response) {
            return response.data;
        });
    };
    var getLoginUserInfo = function (userName) {
        return $http.get(urlBase + "GetLoginUserInfo?id=" + userName).then(function (response) {
            return response.data;
        });
    };
    var changePassword = function (passwordData) {
        return $http.post(urlBase + "ChangeUserPassword", passwordData).then(function (response) {
            return response.data;
        });
    };
    var addNewUser = function (newUserdata) {
        return $http.post(urlBase + "AddNewUser", newUserdata).then(function (response) {
            return response.data;
        });
    };
    var updateUser = function (userdata) {
        return $http.put(urlBase + "UpdateUser", userdata).then(function (response) {
            return response.data;
        });
    };
    var deleteUser = function (deleteUserdata) {
        return $http.delete(urlBase + "DeleteUser?id=" + deleteUserdata).then(function (response) {
            return response.data;
        });
    };

    var manageUser = function(userViewModel) {
        if (userViewModel.Uid !== "") {
            return updateUser(userViewModel);
        } else {
            return addNewUser(userViewModel);
        }
    };

    return {
        getAllUsers: getAllUsers,
        manageUser: manageUser,
        deleteUser: deleteUser,
        getLoginUserInfo: getLoginUserInfo,
        changePassword: changePassword
    };
}]);