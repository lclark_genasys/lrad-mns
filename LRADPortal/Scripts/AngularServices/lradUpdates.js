﻿(function() {
    'user strict';
    angular
        .module('LRADPortal')
        .factory('LRADUpdates', function(LRADUpdateEndPoint, $websocket, $rootScope, $timeout) {
            var ws = $websocket(LRADUpdateEndPoint);

            ws.onMessage(function(event) {
                console.log("Web socket heartbeat");
                var data = JSON.parse(event.data);
                if (new RegExp("DeviceFaults|ConnectionState|Status|BatteryCharge|Temperature|StatusFlags|DioAlerts|PlaybackStatus|SatelliteStatus").test(event.data)) {
                    console.log(event.data);
                    $rootScope.$broadcast("LRAD.DevicePropertyChanged", data);
                }
            });

            ws.onError(function(event) {
                console.log('connection Error', event);
            });

            ws.onClose(function(event) {
                console.log('connection closed', event);
            });

            ws.onOpen(function() {
                console.log('connection open');
            });

            return {
                status: function() {
                    return ws.readyState;
                },
                connect: function () {
                    $timeout(function() {
                        ws.reconnect();
                    });
                },
                close: function() {
                    ws.close();
                }
            };
        });
})();
