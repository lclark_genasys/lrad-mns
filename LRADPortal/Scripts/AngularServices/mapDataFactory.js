﻿app.factory('mapDataFactory', ['$http', function ($http) {
    var speakerImage = function(color, isPlaying, selected, alerts) {
        //return 'api/Map/GetSpeaker?uid=' + uid + "&selected=" + selected.toString();
        //volume = (volume == undefined || volume == null) ? 0 : volume;
        var svg;
        if (isPlaying) {
            svg = '<svg width="100" height="100" xmlns="http://www.w3.org/2000/svg"><g><g id="sp1_1_"><g id="_x34_">' +
                //'<circle id="svg_3" r="42.5%" cy="50%" cx="50%" fill="' + color + '" opacity="0.3"/>' +
                //'<circle id="svg_4" r="33%" cy="50%" cx="50%" fill="' + color + '" opacity="0.7"/>' +
                //'<circle id="svg_5" r="50%" cy="50%" cx="50%" fill="' + color + '" opacity="0.2"/>' +
                '<circle id="innerCircle" r="48.5%" cy="50%" cx="50%" stroke-miterlimit="10" stroke-width="3" stroke="' + (selected ? '#ffffff' : color) + '" fill="none" opacity="0.8"/>' +
                //'<circle id="wave1" r="38%" cy="50%" cx="50%" fill="none" stroke="' + color + '" stroke-width="7" opacity="0.3"/>' +
                //'<circle id="wave2" r="45%" cy="50%" cx="50%" fill="none" stroke="' + color + '" stroke-width="7" opacity="0.2"/>' +
                '<circle id="innerFill" r="47%" cy="50%" cx="50%" fill="' + color + '" opacity="0.2"/>' +
                '</g>' +
                '</g>' +
                (alerts && alerts.length > 0 ?
                '<g id="svg_27">'+
                    '<circle fill="#AB1F24" stroke="#FFFFFF" stroke-miterlimit="10" cx="81.409" cy="29.902" r="15.923"/>' +
                    '<g id="svg_24">'+
                        '<polygon fill="#FFFFFF" points="82.476,31.882 80.188,31.882 79.143,21.797 83.674,21.797"/>' +
                        '<rect x="79.36" y="33.908" fill="#FFFFFF" width="4.098" height="4.098"/>' +
                    '</g>'+
                '</g>':"")+
                '</g></svg>';
        } else {
            svg = '<svg width="100" height="100" xmlns="http://www.w3.org/2000/svg"><g><g id="sp1_1_">' +
                '<g id="svg_1">' +
                //'<circle id="svg_2" r="48.5%" cy="50%" cx="50%" fill="' + color + '" opacity="0.8"/>' +
                '<circle id="svg_3" r="48.5%" cy="50%" cx="50%" stroke-width="3" stroke="' + (selected ? '#ffffff' : color) + '" fill="none"/>' +
                '</g>' +
                '<circle r="48.5%" cy="50%" cx="50%" display="none" fill="' + color + '" opacity="0.8" id="_x31__1_"/>' +
                '</g>' +
                (alerts && alerts.length > 0 ?
                '<g id="svg_27">' +
                    '<circle fill="#AB1F24" stroke="#FFFFFF" stroke-miterlimit="10" cx="81.409" cy="29.902" r="15.923"/>' +
                    '<g id="svg_24">'+
                        '<polygon fill="#FFFFFF" points="82.476,31.882 80.188,31.882 79.143,21.797 83.674,21.797"/>' +
                        '<rect x="79.36" y="33.908" fill="#FFFFFF" width="4.098" height="4.098"/>' +
                    '</g>'+
                '</g>': "") +
                '</g></svg>';
        }
        return 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(svg);
    };

    var speakerClusterImg = function (color, name, sum, isSelected) {
        //return 'api/Map/GetSpeakerGroupCluster?uid=' + groupUid + "&file=ignore&sum=" + sum;
        var clusterWidth = name.width() + 50;
        var height = 35;
        var svg = '<svg width="' + clusterWidth + '" height="' + height + '" xmlns="http://www.w3.org/2000/svg"><g>' +
            '<rect stroke-width="3" stroke="' + (isSelected ? '#ffffff' : color) + '" height="100%" width="100%" fill="' + color + '" opacity="0.8" x="0" id="background"/>' +
            '<text id="svg_5" fill="#FFFFFF" text-anchor="middle" y="50%" x="50%" dy="5" style="text-transform: uppercase; font-size: 16px; font-family:\'Arial\'">' + name + '</text>' +
            '<text id="svg_6" fill="#FFFFFF" text-anchor="middle" y="10pt" x="100%" dx="-10px" dy="2" style="text-transform: uppercase; font-size: 12px; font-family:\'Arial\'">' + sum + '</text>' +
            '</g></svg>';
        return { width: clusterWidth, height: height, svg: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(svg) };
    };

    var speakerGroupImagePath = function (uid) {
        return 'api/Map/GetSpeakerGroup?uid=' + uid;
    };

    return {
        speakerImage: speakerImage,
        speakerClusterImg: speakerClusterImg,
        speakerGroupImagePath: speakerGroupImagePath
    };
}]);


String.prototype.width = function (font) {
    var f = font || '16px arial',
        o = $('<div>' + this + '</div>')
              .css({ 'position': 'absolute', 'float': 'left', 'white-space': 'nowrap', 'visibility': 'hidden', 'font': f, 'text-transform': 'uppercase'})
              .appendTo($('body')),
        w = o.outerWidth();
    o.remove();
    return w;
}