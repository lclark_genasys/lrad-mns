(function (global) {
    'use strict';

    var Recorder, RECORDED_AUDIO_TYPE = "audio/wav";

    Recorder = {
        recorder: null,
        recorderOriginalWidth: 0,
        recorderOriginalHeight: 0,
        uploadFormId: null,
        uploadFieldName: null,
        isReady: false,

        connect: function (name, attempts) {
            if (navigator.appName.indexOf("Microsoft") != -1) {
                Recorder.recorder = window[name];
            } else {
                Recorder.recorder = document[name];
            }

            if (attempts >= 40) {
                return;
            }

            // flash app needs time to load and initialize
            if (Recorder.recorder && Recorder.recorder.init) {
                Recorder.recorderOriginalWidth = Recorder.recorder.width;
                Recorder.recorderOriginalHeight = Recorder.recorder.height;
                if (Recorder.uploadFormId && $) {
                    var frm = $(Recorder.uploadFormId);
                    Recorder.recorder.init(frm.attr('action').toString(), Recorder.uploadFieldName, frm.serializeArray());
                }
                return;
            }

            setTimeout(function () {
                Recorder.connect(name, attempts + 1);
            }, 100);
        },

        playBack: function (name) {
            // TODO: Rename to `playback`
            Recorder.recorder.playBack(name);
        },

        pausePlayBack: function (name) {
            // TODO: Rename to `pausePlayback`
            Recorder.recorder.pausePlayBack(name);
        },

        playBackFrom: function (name, time) {
            // TODO: Rename to `playbackFrom`
            Recorder.recorder.playBackFrom(name, time);
        },

        record: function (name, filename) {
            Recorder.recorder.record(name, filename);
        },

        stopRecording: function () {
            Recorder.recorder.stopRecording();
        },

        stopPlayBack: function () {
            // TODO: Rename to `stopPlayback`
            Recorder.recorder.stopPlayBack();
        },

        observeLevel: function () {
            Recorder.recorder.observeLevel();
        },

        stopObservingLevel: function () {
            Recorder.recorder.stopObservingLevel();
        },

        observeSamples: function () {
            Recorder.recorder.observeSamples();
        },

        stopObservingSamples: function () {
            Recorder.recorder.stopObservingSamples();
        },

        resize: function (width, height) {
            Recorder.recorder.width = width + "px";
            Recorder.recorder.height = height + "px";
        },

        defaultSize: function () {
            Recorder.resize(Recorder.recorderOriginalWidth, Recorder.recorderOriginalHeight);
        },

        show: function () {
            Recorder.recorder.show();
        },

        hide: function () {
            Recorder.recorder.hide();
        },

        duration: function (name) {
            // TODO: rename to `getDuration`
            return Recorder.recorder.duration(name || Recorder.uploadFieldName);
        },

        getBase64: function (name) {
            var data = Recorder.recorder.getBase64(name);
            return 'data:' + RECORDED_AUDIO_TYPE + ';base64,' + data;
        },

        getBlob: function (name) {
            var base64Data = Recorder.getBase64(name).split(',')[1];
            return base64toBlob(base64Data, RECORDED_AUDIO_TYPE);
        },

        getCurrentTime: function (name) {
            return Recorder.recorder.getCurrentTime(name);
        },

        isMicrophoneAccessible: function () {
            return Recorder.recorder.isMicrophoneAccessible();
        },

        updateForm: function () {
            var frm = $(Recorder.uploadFormId);
            Recorder.recorder.update(frm.serializeArray());
        },

        showPermissionWindow: function (options) {
            Recorder.resize(240, 160);
            // need to wait until app is resized before displaying permissions screen
            var permissionCommand = function () {
                if (options && options.permanent) {
                    Recorder.recorder.permitPermanently();
                } else {
                    Recorder.recorder.permit();
                }
            };
            setTimeout(permissionCommand, 1);
        },

        configure: function (rate, gain, silenceLevel, silenceTimeout) {
            rate = parseInt(rate || 22);
            gain = parseInt(gain || 100);
            silenceLevel = parseInt(silenceLevel || 0);
            silenceTimeout = parseInt(silenceTimeout || 4000);
            switch (rate) {
                case 44:
                case 22:
                case 11:
                case 8:
                case 5:
                    break;
                default:
                    throw("invalid rate " + rate);
            }

            if (gain < 0 || gain > 100) {
                throw("invalid gain " + gain);
            }

            if (silenceLevel < 0 || silenceLevel > 100) {
                throw("invalid silenceLevel " + silenceLevel);
            }

            if (silenceTimeout < -1) {
                throw("invalid silenceTimeout " + silenceTimeout);
            }

            Recorder.recorder.configure(rate, gain, silenceLevel, silenceTimeout);
        },

        setUseEchoSuppression: function (val) {
            if (typeof(val) != 'boolean') {
                throw("invalid value for setting echo suppression, val: " + val);
            }

            Recorder.recorder.setUseEchoSuppression(val);
        },

        setLoopBack: function (val) {
            if (typeof(val) != 'boolean') {
                throw("invalid value for setting loop back, val: " + val);
            }

            Recorder.recorder.setLoopBack(val);
        }
    };

    function base64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        return new Blob(byteArrays, {type: contentType});
    }

    global.FWRecorder = Recorder;
})(window);

/**
 * This script adds a new function to a function prototype,
 * which allows a function to be converted to a web worker.
 *
 * Please note that this method copies the function's source code into a Blob, so references to variables
 * outside the function's own scope will be invalid.
 *
 * You can however pass variables that can be serialized into JSON, to this function using the params parameter
 *
 * @usage
 * ```
 * myFunction.toWorker({param1: p1, param2: p2...})
 *```
 *
 */
(function () {
    'use strict';

    var workerToBlobUrl = function (fn, params) {
        if (typeof fn !== 'function') {
            throw("The specified parameter must be a valid function");
        }

        var fnString = fn.toString();
        if (fnString.match(/\[native\s*code\]/i)) {
            throw("You cannot bind a native function to a worker");
        }

        params = params || {};
        if (typeof params !== 'object') {
            console.warn('Params must be an object that is serializable with JSON.stringify, specified is: ' + (typeof params));
        }

        var blobURL = window.URL.createObjectURL(new Blob(['(', fnString, ')(this,', JSON.stringify(params), ')'], {type: 'application/javascript'}));

        return blobURL;
    };

    Function.prototype.toWorker = function (params) {
        var url = workerToBlobUrl(this, params);
        return new Worker(url);
    };
})();

/*License (MIT)

 Copyright © 2013 Matt Diamond

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
 */

(function (win) {
    'use strict';

    var RecorderWorker = function (me) {
        var recLength = 0,
            recBuffersL = [],
            recBuffersR = [],
            bits = 16,
            sampleRate;

        me.onmessage = function (e) {
            switch (e.data.command) {
                case 'init':
                    init(e.data.config);
                    break;
                case 'record':
                    record(e.data.buffer);
                    break;
                case 'exportWAV':
                    exportWAV(e.data.type);
                    break;
                case 'getBuffer':
                    getBuffer();
                    break;
                case 'clear':
                    clear();
                    break;
            }
        };

        function init(config) {
            sampleRate = config.sampleRate;
        }

        function record(inputBuffer) {
            recBuffersL.push(inputBuffer[0]);
            //recBuffersR.push(inputBuffer[1]);
            recLength += inputBuffer[0].length;
        }

        function exportWAV(type) {
            var bufferL = mergeBuffers(recBuffersL, recLength);
            var dataview = encodeWAV(bufferL);
            var audioBlob = new Blob([dataview], {type: type});

            me.postMessage(audioBlob);
        }

        function getBuffer() {
            var buffers = [];
            buffers.push(mergeBuffers(recBuffersL, recLength));
            buffers.push(mergeBuffers(recBuffersR, recLength));
            me.postMessage(buffers);
        }

        function clear() {
            recLength = 0;
            recBuffersL = [];
            recBuffersR = [];
        }

        function mergeBuffers(recBuffers, recLength) {
            var result = new Float32Array(recLength);
            var offset = 0;
            for (var i = 0; i < recBuffers.length; i++) {
                result.set(recBuffers[i], offset);
                offset += recBuffers[i].length;
            }
            return result;
        }

        //function interleave(inputL, inputR) {
        //  var length = inputL.length + inputR.length;
        //  var result = new Float32Array(length);
        //
        //  var index = 0,
        //    inputIndex = 0;
        //
        //  while (index < length) {
        //    result[index++] = inputL[inputIndex];
        //    result[index++] = inputR[inputIndex];
        //    inputIndex++;
        //  }
        //  return result;
        //}

        function floatTo16BitPCM(output, offset, input) {
            for (var i = 0; i < input.length; i++, offset += 2) {
                var s = Math.max(-1, Math.min(1, input[i]));
                output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);
            }
        }

        function writeString(view, offset, string) {
            for (var i = 0; i < string.length; i++) {
                view.setUint8(offset + i, string.charCodeAt(i));
            }
        }


        function encodeWAV(samples) {
            var buffer = new ArrayBuffer(44 + samples.length * 2);
            var view = new DataView(buffer);

            /* RIFF identifier */
            writeString(view, 0, 'RIFF');
            /* file length */
            view.setUint32(4, 32 + samples.length * 2, true);
            /* RIFF type */
            writeString(view, 8, 'WAVE');
            /* format chunk identifier */
            writeString(view, 12, 'fmt ');
            /* format chunk length */
            view.setUint32(16, 16, true);
            /* sample format (raw) */
            view.setUint16(20, 1, true);
            /* channel count */
            //view.setUint16(22, 2, true); /*STEREO*/
            view.setUint16(22, 1, true);
            /*MONO*/
            /* sample rate */
            view.setUint32(24, sampleRate, true);
            /* byte rate (sample rate * block align) */
            //view.setUint32(28, sampleRate * 4, true); /*STEREO*/
            view.setUint32(28, sampleRate * 2, true);
            /*MONO*/
            /* block align (channel count * bytes per sample) */
            //view.setUint16(32, 4, true); /*STEREO*/
            view.setUint16(32, 2, true);
            /*MONO*/
            /* bits per sample */
            view.setUint16(34, 16, true);
            /* data chunk identifier */
            writeString(view, 36, 'data');
            /* data chunk length */
            view.setUint32(40, samples.length * 2, true);

            floatTo16BitPCM(view, 44, samples);

            return view;
        }
    };

    var Recorder = function (source, cfg) {
        var config = cfg || {};
        var bufferLen = config.bufferLen || 4096;
        this.context = source.context;
        this.node = (this.context.createScriptProcessor ||
        this.context.createJavaScriptNode).call(this.context,
            bufferLen, 2, 2);
        var worker = RecorderWorker.toWorker();

        worker.postMessage({
            command: 'init',
            config: {
                sampleRate: this.context.sampleRate
            }
        });

        var recording = false,
            currCallback;

        this.node.onaudioprocess = function (e) {
            if (!recording) return;
            worker.postMessage({
                command: 'record',
                buffer: [
                    e.inputBuffer.getChannelData(0),
                ]
            });
        };

        this.configure = function (cfg) {
            for (var prop in cfg) {
                if (cfg.hasOwnProperty(prop)) {
                    config[prop] = cfg[prop];
                }
            }
        };

        this.record = function () {
            recording = true;
        };

        this.stop = function () {
            recording = false;
        };

        this.clear = function () {
            worker.postMessage({command: 'clear'});
        };

        this.getBuffer = function (cb) {
            currCallback = cb || config.callback;
            worker.postMessage({command: 'getBuffer'})
        };

        this.exportWAV = function (cb, type) {
            currCallback = cb || config.callback;
            type = type || config.type || 'audio/wav';
            if (!currCallback) throw new Error('Callback not set');
            worker.postMessage({
                command: 'exportWAV',
                type: type
            });
        };

        worker.onmessage = function (e) {
            var blob = e.data;
            //console.log("the blob " +  blob + " " + blob.size + " " + blob.type);
            currCallback(blob);
        };

        source.connect(this.node);
        this.node.connect(this.context.destination);    //this should not be necessary
    };

    win.Recorder = Recorder;
})(window);

(function (win) {
    'use strict';

    var MP3ConversionWorker = function (me, params) {
        //should not reference any variable in parent scope as it will executed in its
        //on isolated scope
        //console.log('MP3 conversion worker started.');
        if (typeof lamejs === 'undefined') {
            importScripts(params.lameJsUrl);
        }

        var mp3Encoder, maxSamples = 1152, wav, samples, lame, config, dataBuffer;
        var clearBuffer = function () {
            dataBuffer = [];
        };
        var appendToBuffer = function (mp3Buf) {
            dataBuffer.push(new Int8Array(mp3Buf));
        };
        var init = function (prefConfig) {
            config = prefConfig || {};
            lame = new lamejs();
            clearBuffer();
        };
        var encode = function (arrayBuffer) {
            wav = lame.WavHeader.readHeader(new DataView(arrayBuffer));
            //console.log('wave:', wav);
            samples = new Int16Array(arrayBuffer, wav.dataOffset, wav.dataLen / 2);
            mp3Encoder = new lame.Mp3Encoder(wav.channels, wav.sampleRate, config.bitRate || 128);

            var remaining = samples.length;
            for (var i = 0; remaining >= maxSamples; i += maxSamples) {
                var mono = samples.subarray(i, i + maxSamples);
                var mp3buf = mp3Encoder.encodeBuffer(mono);
                appendToBuffer(mp3buf);
                remaining -= maxSamples;
            }
        };
        var finish = function () {
            var mp3buf = mp3Encoder.flush();
            appendToBuffer(mp3buf);
            self.postMessage({cmd: 'end', buf: dataBuffer});
            //console.log('done encoding');
            clearBuffer();//free up memory
        };

        me.onmessage = function (e) {
            switch (e.data.cmd) {
                case 'init':
                    init(e.data.config);
                    break;

                case 'encode':
                    encode(e.data.rawInput);
                    break;

                case 'finish':
                    finish();
                    break;
            }
        };
    };

    var SCRIPT_BASE = (function () {
        var scripts = document.getElementsByTagName('script');
        var myUrl = scripts[scripts.length - 1].getAttribute('src');
        var path = myUrl.substr(0, myUrl.lastIndexOf('/') + 1);

        if (path && !path.match(/:\/\//)) {
            var a = document.createElement('a');
            a.href = path;
            return a.href;
        }

        return path;
    }());
    var MP3Converter = function (config) {

        config = config || {};
        //console.log('SCRIPT_BASE', SCRIPT_BASE);
        config.lameJsUrl = SCRIPT_BASE + 'lib/lame.js';
        var busy = false;
        var mp3Worker = MP3ConversionWorker.toWorker(config);

        this.isBusy = function () {
            return busy
        };

        this.convert = function (blob) {
            var conversionId = 'conversion_' + Date.now(),
                tag = conversionId + ":";

            //console.log(tag, 'Starting conversion');

            var preferredConfig = {}, onSuccess, onError;

            switch (typeof arguments[1]) {
                case 'object':
                    preferredConfig = arguments[1];
                    break;
                case 'function':
                    onSuccess = arguments[1];
                    break;
                default:
                    throw "parameter 2 is expected to be an object (config) or function (success callback)"
            }

            if (typeof arguments[2] === 'function') {
                if (onSuccess) {
                    onError = arguments[2];
                } else {
                    onSuccess = arguments[2];
                }
            }

            if (typeof arguments[3] === 'function' && !onError) {
                onError = arguments[3];
            }

            if (busy) {
                throw ("Another conversion is in progress");
            }

            var initialSize = blob.size,
                fileReader = new FileReader(),
                startTime = Date.now();

            fileReader.onload = function (e) {
                //console.log(tag, "Passed to BG process");

                mp3Worker.postMessage({
                    cmd: 'init',
                    config: preferredConfig
                });
                mp3Worker.postMessage({cmd: 'encode', rawInput: e.target.result});
                mp3Worker.postMessage({cmd: 'finish'});
                mp3Worker.onmessage = function (e) {
                    if (e.data.cmd == 'end') {
                        //console.log(tag, "Done converting to Mp3");
                        var mp3Blob = new Blob(e.data.buf, {type: 'audio/mp3'});
                        //console.log(tag, "Conversion completed in: " + ((Date.now() - startTime) / 1000) + 's');
                        var finalSize = mp3Blob.size;
                        //console.log(tag + "Initial sizeasdf: = " + initialSize + ", " + "Final size = " + finalSize + ", Reduction: " + Number((100 * (initialSize - finalSize) / initialSize)).toPrecision(4) + "%");

                        busy = false;
                        if (onSuccess && typeof onSuccess === 'function') {
                            onSuccess(mp3Blob);
                        }
                    }
                };
            };

            busy = true;
            fileReader.readAsArrayBuffer(blob);
        }
    };

    win.MP3Converter = MP3Converter;
})(window);

(function (win) {
    'use strict';

    /*!SWFObject v2.1 <http://code.google.com/p/swfobject/>
     Copyright (c) 2007-2008 Geoff Stearns, Michael Williams, and Bobby van der Sluis
     This software is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
     */
    win.swfobject = function () {

        var UNDEF = "undefined",
            OBJECT = "object",
            SHOCKWAVE_FLASH = "Shockwave Flash",
            SHOCKWAVE_FLASH_AX = "ShockwaveFlash.ShockwaveFlash",
            FLASH_MIME_TYPE = "application/x-shockwave-flash",
            EXPRESS_INSTALL_ID = "SWFObjectExprInst",

            win = window,
            doc = document,
            nav = navigator,

            domLoadFnArr = [],
            regObjArr = [],
            objIdArr = [],
            listenersArr = [],
            script,
            timer = null,
            storedAltContent = null,
            storedAltContentId = null,
            isDomLoaded = false,
            isExpressInstallActive = false;

        /* Centralized function for browser feature detection
         - Proprietary feature detection (conditional compiling) is used to detect Internet Explorer's features
         - User agent string detection is only used when no alternative is possible
         - Is executed directly for optimal performance
         */
        var ua = function () {
            var w3cdom = typeof doc.getElementById != UNDEF && typeof doc.getElementsByTagName != UNDEF && typeof doc.createElement != UNDEF,
                playerVersion = [0, 0, 0],
                d = null;

            if (typeof nav.plugins != UNDEF && typeof nav.plugins[SHOCKWAVE_FLASH] == OBJECT) {
                d = nav.plugins[SHOCKWAVE_FLASH].description;
                if (d && !(typeof nav.mimeTypes != UNDEF && nav.mimeTypes[FLASH_MIME_TYPE] && !nav.mimeTypes[FLASH_MIME_TYPE].enabledPlugin)) { // navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin indicates whether plug-ins are enabled or disabled in Safari 3+
                    d = d.replace(/^.*\s+(\S+\s+\S+$)/, "$1");
                    playerVersion[0] = parseInt(d.replace(/^(.*)\..*$/, "$1"), 10);
                    playerVersion[1] = parseInt(d.replace(/^.*\.(.*)\s.*$/, "$1"), 10);
                    playerVersion[2] = /r/.test(d) ? parseInt(d.replace(/^.*r(.*)$/, "$1"), 10) : 0;
                }
            } else if (typeof win.ActiveXObject != UNDEF) {
                var a = null, fp6Crash = false;
                try {
                    a = new ActiveXObject(SHOCKWAVE_FLASH_AX + ".7");
                }
                catch (e) {
                    try {
                        a = new ActiveXObject(SHOCKWAVE_FLASH_AX + ".6");
                        playerVersion = [6, 0, 21];
                        a.AllowScriptAccess = "always";	 // Introduced in fp6.0.47
                    }
                    catch (e) {
                        if (playerVersion[0] == 6) {
                            fp6Crash = true;
                        }
                    }
                    if (!fp6Crash) {
                        try {
                            a = new ActiveXObject(SHOCKWAVE_FLASH_AX);
                        }
                        catch (e) {
                        }
                    }
                }

                if (!fp6Crash && a) { // a will return null when ActiveX is disabled
                    try {
                        d = a.GetVariable("$version");	// Will crash fp6.0.21/23/29
                        if (d) {
                            d = d.split(" ")[1].split(",");
                            playerVersion = [parseInt(d[0], 10), parseInt(d[1], 10), parseInt(d[2], 10)];
                        }
                    }
                    catch (e) {
                    }
                }
            }

            var u = nav.userAgent.toLowerCase(),
                p = nav.platform.toLowerCase(),
                webkit = /webkit/.test(u) ? parseFloat(u.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : false, // returns either the webkit version or false if not webkit
                ie = false,
                windows = p ? /win/.test(p) : /win/.test(u),
                mac = p ? /mac/.test(p) : /mac/.test(u);
            /*@cc_on
             ie = true;
             @if (@_win32)
             windows = true;
             @elif (@_mac)
             mac = true;
             @end
             @*/
            return {w3cdom: w3cdom, pv: playerVersion, webkit: webkit, ie: ie, win: windows, mac: mac};
        }();

        /* Cross-browser onDomLoad
         - Based on Dean Edwards' solution: http://dean.edwards.name/weblog/2006/06/again/
         - Will fire an event as soon as the DOM of a page is loaded (supported by Gecko based browsers - like Firefox -, IE, Opera9+, Safari)
         */
        var onDomLoad = function () {
            if (!ua.w3cdom) {
                return;
            }

            addDomLoadEvent(main);

            if (ua.ie && ua.win) {
                try {	 // Avoid a possible Operation Aborted error
                    doc.write("<scr" + "ipt id=__ie_ondomload defer=true src=//:></scr" + "ipt>"); // String is split into pieces to avoid Norton AV to add code that can cause errors
                    script = getElementById("__ie_ondomload");
                    if (script) {
                        addListener(script, "onreadystatechange", checkReadyState);
                    }
                }
                catch (e) {
                }
            }

            if (ua.webkit && typeof doc.readyState != UNDEF) {
                timer = setInterval(function () {
                    if (/loaded|complete/.test(doc.readyState)) {
                        callDomLoadFunctions();
                    }
                }, 10);
            }

            if (typeof doc.addEventListener != UNDEF) {
                doc.addEventListener("DOMContentLoaded", callDomLoadFunctions, null);
            }

            addLoadEvent(callDomLoadFunctions);
        }();

        function checkReadyState() {
            if (script.readyState == "complete") {
                script.parentNode.removeChild(script);
                callDomLoadFunctions();
            }
        }

        function callDomLoadFunctions() {
            if (isDomLoaded) {
                return;
            }

            if (ua.ie && ua.win) { // Test if we can really add elements to the DOM; we don't want to fire it too early
                var s = createElement("span");
                try { // Avoid a possible Operation Aborted error
                    var t = doc.getElementsByTagName("body")[0].appendChild(s);
                    t.parentNode.removeChild(t);
                }
                catch (e) {
                    return;
                }
            }

            isDomLoaded = true;

            if (timer) {
                clearInterval(timer);
                timer = null;
            }

            var dl = domLoadFnArr.length;
            for (var i = 0; i < dl; i++) {
                domLoadFnArr[i]();
            }
        }

        function addDomLoadEvent(fn) {
            if (isDomLoaded) {
                fn();
            } else {
                domLoadFnArr[domLoadFnArr.length] = fn; // Array.push() is only available in IE5.5+
            }
        }

        /* Cross-browser onload
         - Based on James Edwards' solution: http://brothercake.com/site/resources/scripts/onload/
         - Will fire an event as soon as a web page including all of its assets are loaded
         */
        function addLoadEvent(fn) {
            if (typeof win.addEventListener != UNDEF) {
                win.addEventListener("load", fn, false);
            } else if (typeof doc.addEventListener != UNDEF) {
                doc.addEventListener("load", fn, false);
            } else if (typeof win.attachEvent != UNDEF) {
                addListener(win, "onload", fn);
            } else if (typeof win.onload == "function") {
                var fnOld = win.onload;
                win.onload = function () {
                    fnOld();
                    fn();
                };
            }
            else {
                win.onload = fn;
            }
        }

        /* Main function
         - Will preferably execute onDomLoad, otherwise onload (as a fallback)
         */
        function main() { // Static publishing only
            var rl = regObjArr.length;
            for (var i = 0; i < rl; i++) { // For each registered object element
                var id = regObjArr[i].id;
                if (ua.pv[0] > 0) {
                    var obj = getElementById(id);

                    if (obj) {
                        regObjArr[i].width = obj.getAttribute("width") ? obj.getAttribute("width") : "0";
                        regObjArr[i].height = obj.getAttribute("height") ? obj.getAttribute("height") : "0";

                        if (hasPlayerVersion(regObjArr[i].swfVersion)) { // Flash plug-in version >= Flash content version: Houston, we have a match!
                            if (ua.webkit && ua.webkit < 312) { // Older webkit engines ignore the object element's nested param elements
                                fixParams(obj);
                            }
                            setVisibility(id, true);
                        } else if (regObjArr[i].expressInstall && !isExpressInstallActive && hasPlayerVersion("6.0.65") && (ua.win || ua.mac)) { // Show the Adobe Express Install dialog if set by the web page author and if supported (fp6.0.65+ on Win/Mac OS only)
                            showExpressInstall(regObjArr[i]);
                        } else { // Flash plug-in and Flash content version mismatch: display alternative content instead of Flash content
                            displayAltContent(obj);
                        }
                    }
                } else {	// If no fp is installed, we let the object element do its job (show alternative content)
                    setVisibility(id, true);
                }
            }
        }

        /* Fix nested param elements, which are ignored by older webkit engines
         - This includes Safari up to and including version 1.2.2 on Mac OS 10.3
         - Fall back to the proprietary embed element
         */
        function fixParams(obj) {
            var nestedObj = obj.getElementsByTagName(OBJECT)[0];
            if (nestedObj) {
                var e = createElement("embed"), a = nestedObj.attributes;

                if (a) {
                    var al = a.length;

                    for (var i = 0; i < al; i++) {
                        if (a[i].nodeName == "DATA") {
                            e.setAttribute("src", a[i].nodeValue);
                        }
                        else {
                            e.setAttribute(a[i].nodeName, a[i].nodeValue);
                        }
                    }
                }

                var c = nestedObj.childNodes;

                if (c) {
                    var cl = c.length;

                    for (var j = 0; j < cl; j++) {
                        if (c[j].nodeType == 1 && c[j].nodeName == "PARAM") {
                            e.setAttribute(c[j].getAttribute("name"), c[j].getAttribute("value"));
                        }
                    }
                }

                obj.parentNode.replaceChild(e, obj);
            }
        }

        /* Show the Adobe Express Install dialog
         - Reference: http://www.adobe.com/cfusion/knowledgebase/index.cfm?id=6a253b75
         */
        function showExpressInstall(regObj) {
            isExpressInstallActive = true;
            var obj = getElementById(regObj.id);

            if (obj) {
                if (regObj.altContentId) {
                    var ac = getElementById(regObj.altContentId);
                    if (ac) {
                        storedAltContent = ac;
                        storedAltContentId = regObj.altContentId;
                    }
                } else {
                    storedAltContent = abstractAltContent(obj);
                }

                if (!(/%$/.test(regObj.width)) && parseInt(regObj.width, 10) < 310) {
                    regObj.width = "310";
                }

                if (!(/%$/.test(regObj.height)) && parseInt(regObj.height, 10) < 137) {
                    regObj.height = "137";
                }

                doc.title = doc.title.slice(0, 47) + " - Flash Player Installation";

                var pt = ua.ie && ua.win ? "ActiveX" : "PlugIn",
                    dt = doc.title,
                    fv = "MMredirectURL=" + win.location + "&MMplayerType=" + pt + "&MMdoctitle=" + dt,
                    replaceId = regObj.id;

                // For IE when a SWF is loading (AND: not available in cache) wait for the onload event to fire to remove the original object element
                // In IE you cannot properly cancel a loading SWF file without breaking browser load references, also obj.onreadystatechange doesn't work
                if (ua.ie && ua.win && obj.readyState != 4) {
                    var newObj = createElement("div");
                    replaceId += "SWFObjectNew";
                    newObj.setAttribute("id", replaceId);
                    obj.parentNode.insertBefore(newObj, obj); // Insert placeholder div that will be replaced by the object element that loads expressinstall.swf
                    obj.style.display = "none";
                    var fn = function () {
                        obj.parentNode.removeChild(obj);
                    };
                    addListener(win, "onload", fn);
                }

                createSWF({
                    data: regObj.expressInstall,
                    id: EXPRESS_INSTALL_ID,
                    width: regObj.width,
                    height: regObj.height
                }, {flashvars: fv}, replaceId);
            }
        }

        /* Functions to abstract and display alternative content
         */
        function displayAltContent(obj) {
            if (ua.ie && ua.win && obj.readyState != 4) {
                // For IE when a SWF is loading (AND: not available in cache) wait for the onload event to fire to remove the original object element
                // In IE you cannot properly cancel a loading SWF file without breaking browser load references, also obj.onreadystatechange doesn't work
                var el = createElement("div");
                obj.parentNode.insertBefore(el, obj); // Insert placeholder div that will be replaced by the alternative content
                el.parentNode.replaceChild(abstractAltContent(obj), el);
                obj.style.display = "none";
                var fn = function () {
                    obj.parentNode.removeChild(obj);
                };
                addListener(win, "onload", fn);
            } else {
                obj.parentNode.replaceChild(abstractAltContent(obj), obj);
            }
        }

        function abstractAltContent(obj) {
            var ac = createElement("div");

            if (ua.win && ua.ie) {
                ac.innerHTML = obj.innerHTML;
            } else {
                var nestedObj = obj.getElementsByTagName(OBJECT)[0];
                if (nestedObj) {
                    var c = nestedObj.childNodes;
                    if (c) {
                        var cl = c.length;
                        for (var i = 0; i < cl; i++) {
                            if (!(c[i].nodeType == 1 && c[i].nodeName == "PARAM") && !(c[i].nodeType == 8)) {
                                ac.appendChild(c[i].cloneNode(true));
                            }
                        }
                    }
                }
            }

            return ac;
        }

        /* Cross-browser dynamic SWF creation
         */
        function createSWF(attObj, parObj, id) {
            var r, el = getElementById(id);

            if (el) {
                if (typeof attObj.id == UNDEF) { // if no 'id' is defined for the object element, it will inherit the 'id' from the alternative content
                    attObj.id = id;
                }

                if (ua.ie && ua.win) { // IE, the object element and W3C DOM methods do not combine: fall back to outerHTML
                    var att = "";

                    for (var i in attObj) {
                        if (attObj[i] != Object.prototype[i]) { // Filter out prototype additions from other potential libraries, like Object.prototype.toJSONString = function() {}
                            if (i.toLowerCase() == "data") {
                                parObj.movie = attObj[i];
                            }
                            else if (i.toLowerCase() == "styleclass") { // 'class' is an ECMA4 reserved keyword
                                att += ' class="' + attObj[i] + '"';
                            }
                            else if (i.toLowerCase() != "classid") {
                                att += ' ' + i + '="' + attObj[i] + '"';
                            }
                        }
                    }

                    var par = "";

                    for (var j in parObj) {
                        if (parObj[j] != Object.prototype[j]) { // Filter out prototype additions from other potential libraries
                            par += '<param name="' + j + '" value="' + parObj[j] + '" />';
                        }
                    }

                    el.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + att + '>' + par + '</object>';
                    objIdArr[objIdArr.length] = attObj.id; // Stored to fix object 'leaks' on unload (dynamic publishing only)
                    r = getElementById(attObj.id);
                } else if (ua.webkit && ua.webkit < 312) { // Older webkit engines ignore the object element's nested param elements: fall back to the proprietary embed element
                    var e = createElement("embed");

                    e.setAttribute("type", FLASH_MIME_TYPE);

                    for (var k in attObj) {
                        if (attObj[k] != Object.prototype[k]) { // Filter out prototype additions from other potential libraries
                            if (k.toLowerCase() == "data") {
                                e.setAttribute("src", attObj[k]);
                            } else if (k.toLowerCase() == "styleclass") { // 'class' is an ECMA4 reserved keyword
                                e.setAttribute("class", attObj[k]);
                            } else if (k.toLowerCase() != "classid") { // Filter out IE specific attribute
                                e.setAttribute(k, attObj[k]);
                            }
                        }
                    }

                    for (var l in parObj) {
                        if (parObj[l] != Object.prototype[l]) { // Filter out prototype additions from other potential libraries
                            if (l.toLowerCase() != "movie") { // Filter out IE specific param element
                                e.setAttribute(l, parObj[l]);
                            }
                        }
                    }

                    el.parentNode.replaceChild(e, el);
                    r = e;
                } else { // Well-behaving browsers
                    var o = createElement(OBJECT);
                    o.setAttribute("type", FLASH_MIME_TYPE);

                    for (var m in attObj) {
                        if (attObj[m] != Object.prototype[m]) { // Filter out prototype additions from other potential libraries
                            if (m.toLowerCase() == "styleclass") { // 'class' is an ECMA4 reserved keyword
                                o.setAttribute("class", attObj[m]);
                            } else if (m.toLowerCase() != "classid") { // Filter out IE specific attribute
                                o.setAttribute(m, attObj[m]);
                            }
                        }
                    }
                    for (var n in parObj) {
                        if (parObj[n] != Object.prototype[n] && n.toLowerCase() != "movie") { // Filter out prototype additions from other potential libraries and IE specific param element
                            createObjParam(o, n, parObj[n]);
                        }
                    }

                    el.parentNode.replaceChild(o, el);
                    r = o;
                }
            }

            return r;
        }

        function createObjParam(el, pName, pValue) {
            var p = createElement("param");
            p.setAttribute("name", pName);
            p.setAttribute("value", pValue);
            el.appendChild(p);
        }

        /* Cross-browser SWF removal
         - Especially needed to safely and completely remove a SWF in Internet Explorer
         */
        function removeSWF(id) {
            var obj = getElementById(id);

            if (obj && (obj.nodeName == "OBJECT" || obj.nodeName == "EMBED")) {
                if (ua.ie && ua.win) {
                    if (obj.readyState == 4) {
                        removeObjectInIE(id);
                    } else {
                        win.attachEvent("onload", function () {
                            removeObjectInIE(id);
                        });
                    }
                } else {
                    obj.parentNode.removeChild(obj);
                }
            }
        }

        function removeObjectInIE(id) {
            var obj = getElementById(id);

            if (obj) {
                for (var i in obj) {
                    if (typeof obj[i] == "function") {
                        obj[i] = null;
                    }
                }

                obj.parentNode.removeChild(obj);
            }
        }

        /* Functions to optimize JavaScript compression
         */
        function getElementById(id) {
            var el = null;

            try {
                el = doc.getElementById(id);
            }
            catch (e) {
            }

            return el;
        }

        function createElement(el) {
            return doc.createElement(el);
        }

        /* Updated attachEvent function for Internet Explorer
         - Stores attachEvent information in an Array, so on unload the detachEvent functions can be called to avoid memory leaks
         */
        function addListener(target, eventType, fn) {
            target.attachEvent(eventType, fn);
            listenersArr[listenersArr.length] = [target, eventType, fn];
        }

        /* Flash Player and SWF content version matching
         */
        function hasPlayerVersion(rv) {
            var pv = ua.pv, v = rv.split(".");
            v[0] = parseInt(v[0], 10);
            v[1] = parseInt(v[1], 10) || 0; // supports short notation, e.g. "9" instead of "9.0.0"
            v[2] = parseInt(v[2], 10) || 0;

            return (pv[0] > v[0] || (pv[0] == v[0] && pv[1] > v[1]) || (pv[0] == v[0] && pv[1] == v[1] && pv[2] >= v[2])) ? true : false;
        }

        /* Cross-browser dynamic CSS creation
         - Based on Bobby van der Sluis' solution: http://www.bobbyvandersluis.com/articles/dynamicCSS.php
         */
        function createCSS(sel, decl) {
            if (ua.ie && ua.mac) {
                return;
            }

            var h = doc.getElementsByTagName("head")[0],
                s = createElement("style");
            s.setAttribute("type", "text/css");
            s.setAttribute("media", "screen");

            if (!(ua.ie && ua.win) && typeof doc.createTextNode != UNDEF) {
                s.appendChild(doc.createTextNode(sel + " {" + decl + "}"));
            }

            h.appendChild(s);

            if (ua.ie && ua.win && typeof doc.styleSheets != UNDEF && doc.styleSheets.length > 0) {
                var ls = doc.styleSheets[doc.styleSheets.length - 1];

                if (typeof ls.addRule == OBJECT) {
                    ls.addRule(sel, decl);
                }
            }
        }

        function setVisibility(id, isVisible) {
            var v = isVisible ? "visible" : "hidden";

            if (isDomLoaded && getElementById(id)) {
                getElementById(id).style.visibility = v;
            } else {
                createCSS("#" + id, "visibility:" + v);
            }
        }

        /* Filter to avoid XSS attacks
         */
        function urlEncodeIfNecessary(s) {
            var regex = /[\\\"<>\.;]/;
            var hasBadChars = regex.exec(s) != null;

            return hasBadChars ? encodeURIComponent(s) : s;
        }

        /* Release memory to avoid memory leaks caused by closures, fix hanging audio/video threads and force open sockets/NetConnections to disconnect (Internet Explorer only)
         */
        var cleanup = function () {
            if (ua.ie && ua.win) {
                window.attachEvent("onunload", function () {
                    // remove listeners to avoid memory leaks
                    var ll = listenersArr.length;

                    for (var i = 0; i < ll; i++) {
                        listenersArr[i][0].detachEvent(listenersArr[i][1], listenersArr[i][2]);
                    }

                    // cleanup dynamically embedded objects to fix audio/video threads and force open sockets and NetConnections to disconnect
                    var il = objIdArr.length;

                    for (var j = 0; j < il; j++) {
                        removeSWF(objIdArr[j]);
                    }

                    // cleanup library's main closures to avoid memory leaks
                    for (var k in ua) {
                        ua[k] = null;
                    }

                    ua = null;

                    for (var l in swfobject) {
                        swfobject[l] = null;
                    }

                    swfobject = null;
                });
            }
        }();


        return {
            /* Public API
             - Reference: http://code.google.com/p/swfobject/wiki/SWFObject_2_0_documentation
             */
            registerObject: function (objectIdStr, swfVersionStr, xiSwfUrlStr) {
                if (!ua.w3cdom || !objectIdStr || !swfVersionStr) {
                    return;
                }

                var regObj = {};
                regObj.id = objectIdStr;
                regObj.swfVersion = swfVersionStr;
                regObj.expressInstall = xiSwfUrlStr ? xiSwfUrlStr : false;
                regObjArr[regObjArr.length] = regObj;

                setVisibility(objectIdStr, false);
            },

            getObjectById: function (objectIdStr) {
                var r = null;

                if (ua.w3cdom) {
                    var o = getElementById(objectIdStr);

                    if (o) {
                        var n = o.getElementsByTagName(OBJECT)[0];
                        if (!n || (n && typeof o.SetVariable != UNDEF)) {
                            r = o;
                        } else if (typeof n.SetVariable != UNDEF) {
                            r = n;
                        }
                    }
                }

                return r;
            },

            embedSWF: function (swfUrlStr, replaceElemIdStr, widthStr, heightStr, swfVersionStr, xiSwfUrlStr, flashvarsObj, parObj, attObj) {
                if (!ua.w3cdom || !swfUrlStr || !replaceElemIdStr || !widthStr || !heightStr || !swfVersionStr) {
                    return;
                }

                widthStr += ""; // Auto-convert to string
                heightStr += "";

                if (hasPlayerVersion(swfVersionStr)) {
                    setVisibility(replaceElemIdStr, false);
                    var att = {};

                    if (attObj && typeof attObj === OBJECT) {
                        for (var i in attObj) {
                            if (attObj[i] != Object.prototype[i]) { // Filter out prototype additions from other potential libraries
                                att[i] = attObj[i];
                            }
                        }
                    }
                    att.data = swfUrlStr;
                    att.width = widthStr;
                    att.height = heightStr;

                    var par = {};

                    if (parObj && typeof parObj === OBJECT) {
                        for (var j in parObj) {
                            if (parObj[j] != Object.prototype[j]) { // Filter out prototype additions from other potential libraries
                                par[j] = parObj[j];
                            }
                        }
                    }

                    if (flashvarsObj && typeof flashvarsObj === OBJECT) {
                        for (var k in flashvarsObj) {
                            if (flashvarsObj[k] != Object.prototype[k]) { // Filter out prototype additions from other potential libraries
                                if (typeof par.flashvars != UNDEF) {
                                    par.flashvars += "&" + k + "=" + flashvarsObj[k];
                                } else {
                                    par.flashvars = k + "=" + flashvarsObj[k];
                                }
                            }
                        }
                    }

                    addDomLoadEvent(function () {
                        createSWF(att, par, replaceElemIdStr);
                        if (att.id == replaceElemIdStr) {
                            setVisibility(replaceElemIdStr, true);
                        }
                    });
                } else if (xiSwfUrlStr && !isExpressInstallActive && hasPlayerVersion("6.0.65") && (ua.win || ua.mac)) {
                    isExpressInstallActive = true; // deferred execution
                    setVisibility(replaceElemIdStr, false);

                    addDomLoadEvent(function () {
                        var regObj = {};
                        regObj.id = regObj.altContentId = replaceElemIdStr;
                        regObj.width = widthStr;
                        regObj.height = heightStr;
                        regObj.expressInstall = xiSwfUrlStr;
                        showExpressInstall(regObj);
                    });
                }
            },

            getFlashPlayerVersion: function () {
                return {major: ua.pv[0], minor: ua.pv[1], release: ua.pv[2]};
            },

            hasFlashPlayerVersion: hasPlayerVersion,

            createSWF: function (attObj, parObj, replaceElemIdStr) {
                if (ua.w3cdom) {
                    return createSWF(attObj, parObj, replaceElemIdStr);
                } else {
                    return undefined;
                }
            },

            removeSWF: function (objElemIdStr) {
                if (ua.w3cdom) {
                    removeSWF(objElemIdStr);
                }
            },

            createCSS: function (sel, decl) {
                if (ua.w3cdom) {
                    createCSS(sel, decl);
                }
            },

            addDomLoadEvent: addDomLoadEvent,

            addLoadEvent: addLoadEvent,

            getQueryParamValue: function (param) {
                var q = doc.location.search || doc.location.hash;

                if (param == null) {
                    return urlEncodeIfNecessary(q);
                }

                if (q) {
                    var pairs = q.substring(1).split("&");
                    for (var i = 0; i < pairs.length; i++) {
                        if (pairs[i].substring(0, pairs[i].indexOf("=")) == param) {
                            return urlEncodeIfNecessary(pairs[i].substring((pairs[i].indexOf("=") + 1)));
                        }
                    }
                }

                return "";
            },

            // For internal usage only
            expressInstallCallback: function () {
                if (isExpressInstallActive && storedAltContent) {
                    var obj = getElementById(EXPRESS_INSTALL_ID);

                    if (obj) {
                        obj.parentNode.replaceChild(storedAltContent, obj);
                        if (storedAltContentId) {
                            setVisibility(storedAltContentId, true);
                            if (ua.ie && ua.win) {
                                storedAltContent.style.display = "block";
                            }
                        }
                        storedAltContent = null;
                        storedAltContentId = null;
                        isExpressInstallActive = false;
                    }
                }
            }
        };
    }();
})(window);