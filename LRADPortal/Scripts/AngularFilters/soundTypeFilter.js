﻿app.filter('soundTypeFilter', function () {
    return function (input) {
        switch (input) {
            case 0:
                return "General";
                break;
            case 1:
                return "Emergency";
                break;
            case 2:
                return "Custom General";
                break;
            case 3:
                return "Custom Emergency";
                break;
            default:
                break;
        }
    };
});