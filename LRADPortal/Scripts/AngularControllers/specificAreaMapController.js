app.controller("SpecificAreaMapController", function ($scope, $auth, $state, $stateParams, $http, $log, $timeout, uiGmapGoogleMapApi, mapImageOverlay, areaGroupDataFactory, mapDataFactory, speakerService, speakerGroupService, ModalService, speakerLimiter) {
    uiGmapGoogleMapApi.then(function (maps) {
        // uiGmapGoogleMapApi is a promise.
        // The "then" callback function provides the google.maps object.
        $scope.options = {
            mapTypeControl: true,
            scaleControl: true,
            streetViewControl: true,
            mapTypeId: google.maps.MapTypeId.HYBRID
        }

        areaGroupDataFactory.getAreaById({
                areaId: ($stateParams.areaId == undefined) ? "" : $stateParams.areaId,
                groupId: ($stateParams.groupId == undefined) ? "" : $stateParams.groupId
            })
            .then(function (data) {
                var keyPressed = false;

                    $scope.bindEventTypes = function (group) {
                        group.typeEvents = {
                            click: function(cluster, clusterModels) {
                                $scope.$apply(function() {
                                    $scope.closeMapWindow();
                                });
                            },
                            mouseout: function(cluster, clusterModels) {
                            },
                            mouseover: function(cluster, clusterModels) {
                                $scope.$apply(function () {
                                    $scope.closeMapWindow();
                                    var clusterCenter = cluster.getCenter();
                                    $scope.selectedClusterLocation = { latitude: clusterCenter.lat(), longitude: clusterCenter.lng() };
                                    $scope.clusterInfoWindowParameter.group = speakerGroupService.findGroupbyId($scope.selectedSpeakerGroups, clusterModels[0].SpeakerGroupUid);
                                    $scope.clusterWindowOptions.visible = true;
                                });
                            }
                        }
                    };
                    $scope.bindTypeEventsToAllGroups = function() {
                        // Because angualr google maps only supports one cluster event handler per cluster. I had to loop the groups and manually inject the event handeler function to each group
                        angular.forEach($scope.selectedSpeakerGroups, function(group) {
                            $scope.bindEventTypes(group);
                        });
                    };
                    $scope.bindTypeEventsToAllGroups();
                   
                    var getMeterPerPixel = function(zoomLevel, latitude) {
                        return 156543.03392 * Math.cos(latitude * Math.PI / 180) / Math.pow(2, zoomLevel);
                    };

                    $scope.map = {
                        center: {
                            latitude: data.CenterLatitude,
                            longitude: data.CenterLongitude
                        },
                        mapTypeControl: true,
                        zoom: data.Zoom,
                        bounds: {

                        },
                        control:{},
                        customImages: data.CustomImageOverlay,
                        events: {
                            click: function (maps, e, param) {
                                $scope.$apply(function() {
                                    $scope.closeMapWindow();
                                });
                            },
                            rightclick: function (maps, e, param) {
                                $scope.$apply(function() {
                                    $scope.closeMapWindow();
                                    $scope.rightClickLocation = {
                                        latitude: param[0].latLng.lat(),
                                        longitude: param[0].latLng.lng()
                                    };
                                    $scope.mapRightClickParameter.location = $scope.rightClickLocation;
                                    $scope.mapRightClickWindowOptions.visible = true;
                                });
                            },
                            drag: function(maps, e, param) {
                                $scope.$apply(function() {
                                    $scope.closeMapWindow();
                                });
                            },
                            tilesloaded: function (map) {
                                $scope.$apply(function () {
                                    _.each(data.CustomImageOverlay, function (image) {
                                        var swBound = new google.maps.LatLng(image.LowerLeftLatitude, image.LowerLeftLongitude);
                                        var neBound = new google.maps.LatLng(image.UpperRightLatitude, image.UpperRightLongitude);
                                        var bounds = new google.maps.LatLngBounds(swBound, neBound);
                                        //map.fitBounds(bounds);
                                        var overlay = mapImageOverlay.createImage(bounds, image.ImageFilename, map);
                                    });
                                });
                            }
                        }
                    };

                    $scope.windowOptions = {
                        visible: false,
                        pixelOffset: new google.maps.Size(0, -20, "px", "px"),
                        maxWidth: 150
                    };

                    $scope.clusterWindowOptions = {
                        visible: false,
                        pixelOffset: new google.maps.Size(0, -13, "px", "px")
                        };

                    $scope.mapRightClickWindowOptions = {
                        visible: false,
                        pixelOffset: new google.maps.Size(0, 0, "px", "px"),
                        maxWidth: 150
                    };

                    $scope.infoWindowTemplateUrl = "/Templates/MapInfoWindow.html";
                    $scope.clusterInfoWindowTemplateUrl = "/Templates/MapGroupInfoWindow.html";
                    $scope.mapRightClickWindowTemplateUrl = "/Templates/MapRightClickWindow.html";

                    $scope.closeMapWindow = function() {
                        $scope.windowOptions.visible = false;
                        $scope.clusterWindowOptions.visible = false;
                        $scope.mapRightClickWindowOptions.visible = false;
                    };

                    $scope.infoWindowParameter = {
                        paramScope: $scope,
                        speaker: {},
                        showEditSpeaker: function (speaker) {
                            if ($scope.isSpeakerPlaying) {
                                return false;
                            }

                            $timeout(function () {
                                $scope.closeMapWindow();
                            }, 100);

                            ModalService.showModal({
                                templateUrl: "Templates/SpeakerModal.html",
                                controller: "speakerModalController",
                                inputs: {
                                    speaker: speaker,
                                    allAreas: $scope.$parent.allAreas,
                                    speakerGroups: $scope.$parent.selectedSpeakerGroups,
                                    selectedArea: null,
                                    selectedGroup: null,
                                    latLng: undefined
                                }
                            }).then(function(modal) {
                                modal.element.modal();
                                modal.close.then(function(result) {
                                });
                            });
                        },
                        showDeleteSpeaker: function (speaker) {
                            if ($scope.isSpeakerPlaying) {
                                return false;
                            }

                            $timeout(function () {
                                $scope.closeMapWindow();
                            }, 100);
                            ModalService.showModal({
                                templateUrl: "Templates/DeleteSpeakerModal.html",
                                controller: "DeleteSpeakerModalController",
                                inputs: {
                                    speaker: speaker
                                }
                            }).then(function (modal) {
                                modal.element.modal();
                                modal.close.then(function (result) {
                                    if (result) {
                                        areaGroupDataFactory.deleteSpeaker($scope.speakerToModify.Uid).then(function () {
                                            var group = _.find($scope.selectedSpeakerGroups, function (group) {
                                                return group.Uid.toUpperCase() === $scope.speakerToModify.SpeakerGroupUid.toUpperCase();
                                            });
                                            group.Speakers.splice(group.Speakers.indexOf($scope.speakerToModify), 1);
                                        },
                                        function (data) {
                                            $log.error("Unable to delete speaker " + $scope.speakerToModify.Uid);
                                        });
                                    }
                                });
                            });
                        }
                    };

                    $scope.clusterInfoWindowParameter = {
                        group: {},
                        showEditGroup: function(group) {
                            $scope.closeMapWindow();
                            ModalService.showModal({
                                templateUrl: "Templates/SpeakerGroupModal.html",
                                controller: "speakerGroupModalController",
                                inputs: {
                                    speakerGroup: group,
                                    allAreas: $scope.$parent.allAreas,
                                    allGroups: $scope.$parent.allSpeakerGroups,
                                    allGeneralSounds: $scope.$parent.generalSounds,
                                    allEmergencySounds: $scope.$parent.emergencySounds,
                                    speakerGroups: $scope.$parent.selectedSpeakerGroups,
                                    selectedArea: null
                                }
                            }).then(function(modal) {
                                modal.element.modal();
                                modal.close.then(function(result) {
                                    $scope.bindEventTypes(result.speakerGroup);
                                });
                            });
                        },
                        showDeleteGroup: function(group) {
                            $scope.closeMapWindow();
                            $scope.$parent.alertContent = "<p>Are you sure you want to delete this group '<b>" + group.Name + "</b>'?</p>";
                            $scope.$parent.groupToModify = group;
                        }
                    };

                    $scope.mapRightClickParameter = {
                        location: {},
                        showAddGroup: function () {
                            $scope.closeMapWindow();
                            $scope.$parent.showAddSpeakerGroupModal();
                        },
                        showAddSpeaker: function (latLng) {
                            areaGroupDataFactory.getSpeakersCount().then(function (data) {
                                if (data <= speakerLimiter) {
                                    $scope.closeMapWindow();
                                    $scope.$parent.showAddSpeakerModal(latLng);
                                } else {
                                    ModalService.showModal({
                                        templateUrl: "Templates/SpeakerErrorModal.html",
                                        controller: "speakerErrorModalController"
                                    }).then(function (modal) {
                                        modal.element.modal();
                                        modal.close.then(function (result) { });
                                    });
                                }
                            });
                        }
                    };

                    $scope.getSpeakerIcon = function(speaker, zoom) {
                        var scaleToPixel = speakerService.calculateCoverage(speaker) * 2 / getMeterPerPixel(zoom, speaker.latitude);
                        var scaledSize = new google.maps.Size(scaleToPixel, scaleToPixel, "px", "px");
                        var offset = new google.maps.Point(scaleToPixel / 2, scaleToPixel / 2);
                        var isPlaying = _.has(speaker, "DeviceInfo.MnsState.StatusFlags.AudioOn")? speaker.DeviceInfo.MnsState.StatusFlags.AudioOn : false;

                        return {
                            url: mapDataFactory.speakerImage(speaker.Color, isPlaying, speaker.IsSelected == undefined ? false : speaker.IsSelected, speaker.AlertMessages),
                            anchor: offset,
                            scaledSize: scaledSize,
                            size: scaledSize
                        };
                    };

                    $scope.refreshSpeakersIcon = function() {
                        angular.forEach($scope.selectedSpeakerGroups, function(group) {
                            angular.forEach(group.Speakers, function(speaker) {
                                var icon = $scope.getSpeakerIcon(speaker, $scope.map.zoom);
                                speaker.options = {
                                    icon: icon,
                                    shape: {
                                        coords: [icon.anchor.x, icon.anchor.y, icon.scaledSize.height / 2],
                                        type: 'circle'
                                    }
                                };
                            });
                        });
                    };

                    $scope.$watch('map.zoom', function(newVal, oldVal) {
                        if (angular.isDefined(newVal)) {
                            if (angular.equals(newVal, oldVal)) return;
                            $scope.closeMapWindow();
                            $scope.refreshSpeakersIcon();
                        }
                    }, true);

                    $scope.getOptions = function(model) {
                        var icon = $scope.getSpeakerIcon(model, $scope.map.zoom);
                        model.options = {
                            icon: icon,
                            shape: {
                                coords: [icon.anchor.x, icon.anchor.y, icon.scaledSize.height / 2],
                                type: 'circle'
                            },
                            title: model.Name,
                            labelContent: model.Name.slice(0, 4), // show three chars of the speaker name
                            labelAnchor: "30 14", //x-values increase to the right and y-values increase to the top
                            labelClass: "markerLabels", // the CSS class for the label
                            labelInBackground: false
                        };
                        return model.options;
                    };

                    $scope.getClusterStyle = function(speakerGroup) {
                        var cluster = mapDataFactory.speakerClusterImg(speakerGroup.Color, speakerGroup.Name, $scope.getMinimumClusterSize(speakerGroup.Speakers), speakerGroup.IsSelected);
                        return [
                            {
                                url: cluster.svg,
                                width: cluster.width,
                                height: cluster.height,
                                textSize: 1
                            }
                        ];
                    };

                    $scope.myClick = function (event) {
                        if (event.ctrlKey || event.metaKey) {
                            keyPressed = true;
                        } else {
                            keyPressed = false;
                        }
                    }

                    $scope.markerClick = function (marker, event, model) {

                        $timeout(function () {
                            var selectedSpeakers = [],
                            selectedSpeakerGroup;

                            model.options.zIndex = google.maps.Marker.MAX_ZINDEX + 1;

                            if (keyPressed) { // ctrl key pressed, we select multple
                                model.IsSelected = !model.IsSelected;
                            } else { // not ctrl key, only select one speaker,unselect others
                                for (var i = 0; i < $scope.selectedSpeakerGroups.length; i++) {
                                    angular.forEach($scope.selectedSpeakerGroups[i].Speakers, function (speaker) {
                                        speaker.IsSelected = speaker.Uid === model.Uid ? !model.IsSelected : false;
                                    });
                                }
                            }

                            angular.forEach($scope.selectedSpeakerGroups, function (val, key) {
                                if (val.Speakers) {
                                    angular.forEach(val.Speakers, function (val2, key2) {
                                        if (val2.IsSelected) {
                                            selectedSpeakerGroup = val;
                                            if (selectedSpeakers.indexOf(val2) == -1) {
                                                selectedSpeakers.push(val2);
                                            }
                                        }
                                    });
                                }
                            });

                            $scope.$parent.selectedSpeakers = selectedSpeakers;

                            angular.forEach($scope.selectedSpeakerGroups, function (group) {
                                if (group.Speakers.length > 0) {
                                    group.IsSelected = group.Speakers.every(function (sp) { return sp.IsSelected; });
                                }
                            });

                            $scope.$parent.isCheckAll = $scope.selectedSpeakerGroups.every(function (gp) { return gp.IsSelected; });
                            
                            if (model.IsSelected) {
                                $scope.$parent.selectedSpeaker = model;
                            } else {
                                $scope.$parent.selectedSpeaker = null;
                            }
                        }, 100);
                      
                    };

                    $scope.markerEvents = {
                        mouseover: function (marker, eventName, model) {
                            $scope.isSpeakerPlaying = _.has(model, "DeviceInfo.MnsState.StatusFlags.AudioOn") ? model.DeviceInfo.MnsState.StatusFlags.AudioOn : false;
                            $scope.closeMapWindow();
                            $scope.selectedMarker = model;
                            $scope.infoWindowParameter.speaker = model;
                            $scope.windowOptions.visible = true;
                        }
                    };
                },
                function(data) { // error handle

                });

        $scope.getMinimumClusterSize = function(speakers) {
            var count = 0;
            angular.forEach(speakers, function(speaker) {
                if ((speaker.latitude !== null || speaker.latitude != undefined) && (speaker.longitude !== null || speaker.longitude != undefined))
                    count++;
            });
            return count === 1 ? 0 : count; // if there is only one speaker, don't need to have the cluster
        };
    });

    $scope.$on("filterSelectionChanged", function(event, args) {
        if ($state.is("specificarea.map")) {
            $state.go('specificarea.map', { areaId: args.AreaUid, groupId: args.GroupUid });
        }
    });

    $scope.$on("speakerGroupAdded", function (event, args) {
        $scope.bindEventTypes(args.group);
    });
});