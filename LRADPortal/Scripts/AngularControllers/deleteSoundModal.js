﻿app.controller('DeleteSoundModal', ['$scope', '$element', 'soundName', 'close', function ($scope, $element, soundName, close) {

    $scope.soundName = soundName;

    $scope.close = function (result) {
        close(result, 500);
    };

}]);