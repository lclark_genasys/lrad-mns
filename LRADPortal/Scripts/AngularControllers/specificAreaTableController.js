﻿app.controller("SpecificAreaTableController", function ($scope, $rootScope, $auth, $state, $stateParams, $timeout, ModalService) {
    $scope.showTable = false;
    var isPlaying = function(speaker) {
        return _.has(speaker, "DeviceInfo.MnsState.StatusFlags.AudioOn") ? speaker.DeviceInfo.MnsState.StatusFlags.AudioOn : false;
    }
    var reloadData = function () {
        var formattedList = [],
            selectedSpeakers = [];

        for (var i = 0; i < $scope.selectedSpeakerGroups.length; i++) {
            var _speakerGroup = $scope.selectedSpeakerGroups[i];
            if ($state.params.groupId && _speakerGroup.Uid !== $state.params.groupId)
                continue;
            angular.forEach(_speakerGroup.Speakers, function (value, key) {
                var speaker = {};
                speaker.AreaUid = _speakerGroup.AreaUid;
                speaker.GroupUid = _speakerGroup.Uid;
                speaker.Grouping = _speakerGroup.Name;
                speaker.Playing = _speakerGroup.Playing;
                speaker.IsSelected = value.IsSelected;
                speaker.Name = value.Name;
                speaker.AlertMessages = value.AlertMessages;
                speaker.Uid = value.Uid;
                speaker.StartTime = value.StartTime;
                speaker.PlayMode = value.PlayMode;
                speaker.SpeakerGroupUid = value.SpeakerGroupUid;
                speaker.SoundUid = value.SoundUid;
                speaker.RepeatTimes = value.RepeatTimes;
                speaker.Repeat = value.Repeat;
                speaker.NetworkName = value.NetworkName;
                speaker.DeviceInfo = value.DeviceInfo;
                formattedList.push(speaker);
            });
        }

        $scope.showTable = true;

        angular.forEach(formattedList, function (speaker, key) {
            if (speaker && typeof speaker.IsSelected != 'undefiled' && speaker.IsSelected) {
                selectedSpeakers.push(speaker);
            } else {
                selectedSpeakers.splice(key, 1);
            }
        });

        $scope.$parent.selectedSpeakers = selectedSpeakers;

        return formattedList;
    };

    var addAreaSearches = function (table) {
        // TODO: Need wrappers for each items.
        var search = $('#filterName').on('keyup click', function () {
            $("#specificAreaTable").DataTable().search(this.value).draw();
        });
    }
    var tableObj;
    var initializeTable = function () {
        var tableSpeakersOptions = {
            data: reloadData(),
            sAjaxDataProp: "",
            columns: [
                {
                    "data": "IsSelected",
                    "width": "21px",
                    "orderable": false,
                    "render": function (data, type, row) {
                        return '<div class="gray-checkbox-container"><label><input type="checkbox" ' + (data ? 'checked' : '') + '/><span></span></label></div>';
                    }
                },
                {
                    "data": "Grouping",
                    "width": "100px",
                    "render": function (data, type, row) {
                        return '<span style="text-transform: capitalize !important;">' + data + '</span>';
                    }
                },
                {
                    "data": "Name",
                    "width": "100px",
                    "render": function (data, type, row) {
                        return data;
                    }
                },
                {
                    "data": "Playing",
                    "width": "150px",
                    "render": function (data, type, row) {
                        if (isPlaying(row)) {
                            return "Playing";
                        } else {
                            return "Not Playing";
                        }
                    }
                },
                {
                    "data": "AlertMessages",
                    "render": function (data, type, row) {
                        var result = "";
                        var alerts = data? data.split("."):null;
                        angular.forEach(alerts, function (alert) {
                            if(alert)
                                result += '<div class="text-overflow" style="max-width:400px;"> <i class="ico-alert alertPop" data-container="body" data-toggle="popover" data-html="true"></i> ' + alert + "</div>";
                            return;
                        });
                        return result;
                    }
                },
                {
                    "data": "Uid",
                    "width": "80px",
                    "render": function (data, type, row) {
                        var html = "<div style='text-align: right;'> " +
                            "<a href=\"javascript:void(0)\" class='mapWindowModify " + (isPlaying(row) ? 'disabled' : 'editSpeaker') + "' start-time='" + (isPlaying(row) ? row.StartTime : '') + "' " + (isPlaying(row) ? 'disabled' : '') + " data-uid='" + row.Uid + "' data-groupid='" + row.GroupUid + "'>" +
                            "<img src='/Images/edit.png' title='Edit Speaker' style='width:20px; height:20px; cursor:pointer;margin-right:20px;' /></a>" + " " +
                            "<a data-toggle='modal' class=' " + (isPlaying(row) ? 'disabled' : 'deleteSpeaker') + "' start-time='" + (isPlaying(row) ? row.StartTime : '') + "' " + (isPlaying(row) ? 'disabled' : '') + " data-target='" + (row.StartTime == null ? '#deleteSpeakerModal' : '') + "' data-uid='" + row.Uid + "' data-groupid='" + row.GroupUid + "'>" +
                            "<img src='/Images/trash.png' title='Delete Speaker' style='width:20px; height:20px; cursor:pointer;margin-right:10px;' /></a>" + "</div>";

                        return html;
                    }
                }
            ],
            "bProcessing": true,
            "serverSide": false,
            "order": [1, 'asc'],
            "dom": "<'col-xs-12 dataTable-table't><'col-xs-4'l><'col-xs-4 text-center'i><'col-xs-4'p>",
            "pageLength": 25,
            "createdRow": function (row, data, index) {
                $(row).attr("data-checked", data.IsSelected ? "true" : "false");
                if (data.IsSelected)
                    $(row).addClass("checked");
                $(row).find("input[type=checkbox]").bind('change', function () {
                    var isSelected;
                    if ($(this).prop("checked")) {
                        $(row).addClass("checked");
                        $(row).attr("data-checked", "true");
                        isSelected = true;
                    } else {
                        $(row).removeClass("checked");
                        $(row).attr("data-checked", "false");
                        isSelected = false;
                    }
                    var rowData = tableObj.row(row).data(); // get the data from row
                    var totalGroups = $scope.selectedSpeakerGroups.length;
                    for (var i = 0; i < totalGroups; i++) { // loop all the selected speaker groups
                        var group = $scope.selectedSpeakerGroups[i];
                        if (group.Uid === rowData.GroupUid) { // find the group by id
                            for (var j = 0; j < group.Speakers.length; j++) { // loop all the speakers
                                if (group.Speakers[j].Uid === rowData.Uid) { // find the speaker by current row's id
                                    $scope.$apply(function () { // use $apply to notify anguarjs updates from non-angualrjs code.
                                        group.Speakers[j].IsSelected = isSelected; // set select, update the instance. This shold notify all the instances
                                    });
                                    break;
                                }
                            }
                            group.IsSelected = group.Speakers.every(function (sp) { return sp.IsSelected; });
                            break;
                        }
                    }
                    $scope.$parent.isCheckAll = $scope.selectedSpeakerGroups.every(function (gp) { return gp.IsSelected; });
                });
            }
        };
        var table = $("#specificAreaTable");
        $(table).attr("class", "table table-striped table-hover");
        tableObj = $(table).on('init.dt', function () {
            //// TODO: do something when initialized
            $scope.specificAreaTableInited = true;
            addAreaSearches(table);

            $(".alertPop").popover({
                trigger: 'manual',
                animate: false
            }).click(function (e) {
                e.preventDefault();
            }).mouseenter(function (e) {
                $(this).popover('show');
            }).mouseleave(function (e) {
                $(this).popover('hide');
            });

        }).on('click', 'td[targetId]', function () {
            // TODO: do something when click on row
        }).on('click', '.deleteSpeaker', function () {
            var uid = $(this).data("uid");
            var groupUid = $(this).data("groupid");
            var group = _.find($scope.selectedSpeakerGroups, function (group) {
                return group.Uid.toUpperCase() === groupUid.toUpperCase();
            });
            $scope.$apply(function () {
                $scope.$parent.speakerToModify = _.find(group.Speakers, function (sp) {
                    return sp.Uid.toUpperCase() === uid.toUpperCase();
                });
                $scope.$parent.alertContent = "<p>Are you sure you want to delete this speaker '<b>" + $scope.$parent.speakerToModify.Name + "</b>'?</p>";
            });
        }).on('click', '.editSpeaker', function () {
            var rowData = tableObj.row($(this).parents("tr")).data();
            var group = _.find($scope.selectedSpeakerGroups, function (group) {
                return group.Uid.toUpperCase() === rowData.GroupUid.toUpperCase();
            });
            var speaker = _.find(group.Speakers, function (speaker) {
                return speaker.Uid.toUpperCase() === rowData.Uid.toUpperCase();
            });

            ModalService.showModal({
                templateUrl: "Templates/SpeakerModal.html",
                controller: "speakerModalController",
                inputs: {
                    speaker: speaker,
                    allAreas: $scope.$parent.allAreas,
                    speakerGroups: $scope.selectedSpeakerGroups,
                    selectedArea: null,
                    selectedGroup: null,
                    latLng: undefined
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                });
            });
        }).DataTable(tableSpeakersOptions);
    };

    $scope.$on("filterSelectionChanged", function (event, args) {
        $state.go('specificarea.table', { areaId: args.AreaUid, groupId: args.GroupUid }, { notify: false, reload: false, location: 'replace' });
        $timeout(function () {
            if (tableObj)
                tableObj.clear().rows.add(reloadData()).draw();
        }, 0);
    });

    $scope.$watch('selectedSpeakerGroups', function (newVal, oldVal) {
        if (angular.isDefined(newVal)) {
            if (angular.equals(newVal, oldVal)) return;
            if ($scope.specificAreaTableInited)
                tableObj.clear().rows.add(reloadData()).draw();
        }
    }, true);

    $timeout(function () {
        initializeTable();
    }, 0);
});
