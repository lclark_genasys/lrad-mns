﻿app.controller("LoginController", function ($scope, $auth, $state, $http, $location, $timeout) {
    document.title = "Login";
    var storageMode = localStorage.getItem('storageMode');
    $scope.rememberMe = storageMode != null && storageMode == "localStorage";
    $scope.invalid_grant = "";
    $scope.login = function ($event) {
        $event.preventDefault();
        var data = "grant_type=password&username=" + $scope.email + "&password=" + $scope.password;
        $auth.login(data)
            .then(function () {
                $state.go("areaview");
            })
            .catch(function (response) {
                $scope.invalid_grant = response.data.error_description;
            });
    };

    function isLocalStorage() {
        return $scope.rememberMe ? 'localStorage' : 'sessionStorage';
    }

    $scope.rememberMeChanged = function() {
        localStorage.setItem('storageMode', isLocalStorage());
        $auth.setStorageType(isLocalStorage());
    }

    // For External Logins
    $scope.authenticate = function (provider) {
        $auth.authenticate(provider)
          .then(function () {
          })
          .catch(function (response) {
          });
    };

    $scope.resetPasswordEmail = "";

    $('#resetPasswordModal').on('shown.bs.modal', function (e) {
        $timeout(function () {
            $scope.resetPasswordEmail = "";
        }, 0);


    });

    $('#resetPasswordModal').on('hidden.bs.modal', function () {
        $scope.resetPasswordForm.$setPristine();
        $scope.resetPasswordEmail = "";
    });

    $scope.requestPasswordReset = function () {

        $scope.$broadcast('show-errors-check-validity');

        if (!$scope.resetPasswordForm.$invalid) {
            $http.post('api/account/forgetPassword', { email: $scope.resetPasswordEmail })
                .then(function(data) {
                    $('#resetPasswordModal').modal('hide');
                    alert("An email has been sent to you.");
                }, function (response) {
                    alert(response.data.Message);
                });
        }
    };

}).directive('showEmailErrors', function () {
    return {
        restrict: 'A',
        require: '^form',
        link: function (scope, el, attrs, formCtrl) {
            var inputElement = angular.element(el[0].querySelector("[name]"));
            var inputName = inputElement.attr('name');

            scope.$on('show-errors-check-validity', function () {
                if (formCtrl[inputName].$invalid) {
                    formCtrl[inputName].$setDirty(inputName, true);
                }
                el.toggleClass('has-error', formCtrl[inputName].$invalid);
            });
        }
    }
});