﻿app.controller("NavbarController", function ($scope, $http, $log, $auth, $state, $timeout, LRADUpdates, $rootScope, userInfoDataFactory) {
    var counter = 0;
    var hasLoadedUser = false;
    $scope.hasLogin = false;
   
    $scope.isAuthenticated = function () {
        return $auth.isAuthenticated();
    };
    $scope.username = function () {
        return $auth.getTokenObject().firstName + " " + $auth.getTokenObject().lastName.charAt(0);
    };
    $scope.isManagement = function () {
        return $auth.isAuthenticated() && ($auth.getTokenObject().accessLevel == 0);
    };

    $scope.checkUserLogin = function () {
         if (hasLoadedUser === false && $auth.isAuthenticated()) {
             $timeout(function () {
                 $scope.hasLogin = true;
             }, 0);
             hasLoadedUser = true;
         }
    };

    $scope.$on("userLogout", function (event, args) {
        hasLoadedUser = false;
        $scope.hasLogin = false;
        LRADUpdates.close();
        $state.go("login");
    });
    
});