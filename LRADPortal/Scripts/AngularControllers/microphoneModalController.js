﻿app.controller('microphoneModalController', function ($scope, $element, close, selectedSpeakers, recorderService) {
    $scope.selectedSpeakers = selectedSpeakers;

    $scope.close = function (result) {
        close(result, 500);
    };

});