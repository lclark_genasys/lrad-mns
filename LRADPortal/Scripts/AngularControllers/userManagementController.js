app.controller("UserManagementController", function ($scope, $auth, $state, $log, $timeout, $http, userInfoDataFactory, areaGroupDataFactory) {

    $scope.showTable = false;
    $scope.allUsers = [];
    $scope.allAreas = [];
    $scope.deleteUserUid = "";
    $scope.currentUserUid = "";
    $scope.hasGroups = true;
    $scope.validationMessage = "";
    $scope.isPasswordRequired = false;
    $scope.isConfirmPasswordRequired = false;
    $scope.emailAlreadyUsed = false;
    $scope.editUser = false;
    $scope.assignedGroupsStyle = {};
    $scope.editStatusButton = 'Disable';
    $scope.editStatusMessage = '';
    $scope.editStatusTitle = '';
    $scope.searchText = '';
   
    $scope.onSearchesChanged = function () {
        // TODO: Need wrappers for each items.
        var users = _.filter($scope.allUsers, function (user) { return (user.FirstName+' '+user.LastName).toUpperCase().match($scope.searchText.toUpperCase()); });
        tableObj.clear().rows.add(users).draw();
    };

    $scope.showAll = function () {
        $scope.searchText = '';
        var users = _.filter($scope.allUsers, function (user) { return (user.FirstName + ' ' + user.LastName).toUpperCase().match($scope.searchText.toUpperCase()); });
        tableObj.clear().rows.add(users).draw();
    }

    var getGroupNameList = function (groups) {
        var content = "";
        groups.forEach(function (group) {
            content += group["SpeakerGroupName"] + "<br/>";
        });

        return content;
    };

    var getGroupNumber = function (area, groups) {
        var result = "";
        var parentArea = _.find($scope.allAreas, function (item) { return item["Name"] === area; });
        var totalGroups = parentArea.Groups.length;

        if (groups.length === totalGroups) {
            result = "ALL)";
        } else {
            result = groups.length + ((groups.length === 1) ? " Group)" : " Groups)");
        }

        return result;
    };

    var processAreaGroup = function (data) {
        var result = "";
        var areas = _.uniq(_.map(data, "AreaName")).sort();

        areas.forEach(function (area) {
            var groups = _.filter(data, function (item) { return (item["AreaName"] === area); });

            result += "<span class='areaGroupPopup' style='cursor:pointer;text-transform: capitalize !important;' data-container='body' data-toggle='popover' data-title='" + area
                   + "' data-html='true' data-trigger='focus' data-placement='right' data-content='" + getGroupNameList(groups) + "'>"
                   +area + " (" + getGroupNumber(area, groups) + "</span>" + ", ";

        });

        return result.substr(0, result.length - 2);
    };

    var activatePopover = function () {
        $(".areaGroupPopup").popover({
            trigger: 'manual',
            animate: false
        }).click(function (e) {
            e.preventDefault();
        }).mouseenter(function (e) {
            $(this).popover('show');
        }).mouseleave(function (e) {
            $(this).popover('hide');
        });
    };
    var tableObj;

    $scope.initAll = initAll;
    $scope.loadAreas = loadAreas;
    $scope.loadUsers = loadUsers;
    $scope.initializeTable = initializeTable;

    function initializeTable() {
        var tableUsersOptions = {
            sAjaxDataProp: "",
            columns: [
                {
                    "data": function (row, type, val, meta) {
                        return "<span style='text-transform: capitalize !important;'>" + row.FirstName + " " + row.LastName + "</span>";
                    },
                    "width": "100px"
                },
                {
                    "data": "AreaSpeakerList",
                    "width": "200px",
                    "render": function (data, type, row) {
                        return processAreaGroup(data);
                    }
                },
                {
                    "data": "AccessLevel",
                    "width": "100px",
                    "render": function (data, type, row) {
                        return (data === 1) ? "LEVEL 1 USER" : ((data === 2) ? "LEVEL 2 USER" : "Administrator");
                    }
                },
                {
                    "data": "Status",
                    "width": "60px",
                    "render": function (data, type, row) {
                        return (data === 0) ? "ACTIVE" : "<span style='color: red;'>INACTIVE</span>";
                    }
                },
                {
                    "data": "Uid",
                    "width": "80px",
                    "render": function (data, type, row) {
                        return "<div style='text-align: right;'> " +
                            "<a data-toggle='modal' data-target='#changeStatusModal' data-uid='" + row.Uid + "'>" +
                            "<img src='/Images/power.png' title='Enable/Disable User' style='width:20px; height:20px; cursor:pointer;margin-right:20px;' /></a>" + " " +
                            "<a data-toggle='modal' data-target='#userModal' data-uid='" + row.Uid + "'>" +
                            "<img src='/Images/edit.png' title='Edit User' style='width:20px; height:20px; cursor:pointer;margin-right:20px;' /></a>" + " " +
                            "<a data-toggle='modal' "+ (row.IsSelf ? "style='display:none;'":"") +" data-target='#deleteUserModal' data-uid='" + row.Uid + "'>" +
                            "<img src='/Images/trash.png' title='Delete User' style='width:20px; height:20px; cursor:pointer;margin-right:10px;' /></a>" + "</div>";
                    }
                }
            ],
            "bProcessing": true,
            "serverSide": false, // TODO: Check how this option work
            "order": [0, 'asc'],
            "dom": "<'col-xs-12 dataTable-table't><'col-xs-4'l><'col-xs-4 text-center'i><'col-xs-4'p>",
            "pageLength": 25,
            "createdRow": function (row, data, index) {
                // TODO: Do something when row created, e.g. set attr by data.property
                $(row).attr("data-checked", 'false');
                $(row).find("input[type=checkbox]").bind('change', function () {
                    if ($(this).prop("checked")) {
                        $(row).addClass("checked");
                        $(row).attr("data-checked", "true");
                    } else {
                        $(row).removeClass("checked");
                        $(row).attr("data-checked", "false");
                    }
                });
            }
        };
        var table = $("#userInfoTable");
        $(table).attr("class", "table table-striped table-hover");
        tableObj = $(table).on('init.dt', function () {
            //// TODO: do something when initialized 
        }).on('click', 'td[targetId]', function () {
            // TODO: do something when click on row
        }).dataTable(tableUsersOptions).api();

    };

    function loadAreas() {
        areaGroupDataFactory.getAllAreas()
            .then(function (areas) {
                angular.copy(areas, $scope.allAreas);
                $scope.allAreas = _.sortBy($scope.allAreas, 'Name');
                $scope.allAreas.forEach(function (area) { area.Groups = _.sortBy(area.Groups, 'Name'); });
                $scope.loadUsers();
            },
            function (data) {
                $log.error("Unable to get all areas data");
            });
    };

    function loadUsers() {
        userInfoDataFactory.getAllUsers()
            .then(function (users) {
                $scope.allUsers = users;
                tableObj.clear().rows.add($scope.allUsers).draw();
                $scope.showTable = true;
                activatePopover();
            },
            function (data) {
                $log.error("Unable to get user inof data");
            });
    };

    function initAll() {
        $scope.initializeTable();
        $scope.loadAreas();
    }
    $scope.initAll();
    $scope.userFocus = false;
    $('#userModal').on('shown.bs.modal', function (e) {

        var element = $(e.relatedTarget);
        var uid = element.data('uid');
        var selectedUser = null;
        $scope.currentUserUid = uid;

        if (uid) {
            selectedUser = _.find($scope.allUsers, function (user) { return user.Uid === uid; });
            $scope.editUser = true;
        }
        else {
            $scope.editUser = false;
        }

        initializeTree(selectedUser);
        initializeUserViewModel(selectedUser);
        $scope.userFocus = true;
        $scope.$apply(function () {
            $scope.render = true;
        });
    });

    $('#userModal').on('hidden.bs.modal', function () {
        $scope.userForm.$setPristine();
        $scope.assignedGroupsStyle = {border:"2px solid #D5D8DB"};
        $scope.hasGroups = true;
        $scope.isPasswordRequired = false;
        $scope.isConfirmPasswordRequired = false;
        $scope.validationMessage = "";
    });

    $('#deleteUserModal').on('shown.bs.modal', function (e) {
        var element = $(e.relatedTarget);
        var uid = element.data('uid');
        $scope.deleteUserUid = (uid !== "") ? uid : "";
        if (uid !== "") {
            var selectedUser = _.find($scope.allUsers, function (user) { return user.Uid === uid; });
            $scope.editStatusTitle = 'Delete '+ selectedUser.FirstName + " " + selectedUser.LastName;
        }
        $scope.$apply(function () {
            $scope.render = true;
        });
    });

    $('#deleteUserModal').on('hidden.bs.modal', function () {
        $scope.deleteUserUid = "";
    });
   
    $('#changeStatusModal').on('shown.bs.modal', function (e) {
        var element = $(e.relatedTarget);
        var uid = element.data('uid');
        $scope.currentUserUid = (uid !== "") ? uid : "";

        if (uid !== "") {
            var selectedUser = _.find($scope.allUsers, function (user) { return user.Uid === uid; });
            
            if (selectedUser.Status == 0) {
                $scope.editStatusTitle = 'Disable '+ selectedUser.FirstName + " " + selectedUser.LastName ;
                $scope.editStatusButton = 'Disable';
                $scope.editStatusMessage = 'Are you sure you want to disable this user?';
                
            } else {
                $scope.editStatusTitle = 'Enable ' + selectedUser.FirstName + " " + selectedUser.LastName;;
                $scope.editStatusButton = 'Enable';
                $scope.editStatusMessage = 'Are you sure you want to enable this user?';
               
            }
        }
        $scope.$apply(function () {
            $scope.render = true;
        });
    });

    $('#changeStatusModal').on('hidden.bs.modal', function () {
        $scope.currentUserUid = "";
    });

    $scope.userViewModel = {
        Uid: "",
        UserInfoId: "",
        FirstName: "",
        LastName: "",
        Email: "",
        Phone: "",
        UserName: "",
        Password: "",
        ConfirmPassword: "",
        AccessLevel: "1",
        Status: "0",
        AreaSpeakerList: null
    }

    var initializeUserViewModel = function (selectedUser) {

        if (selectedUser == null) {
            $scope.userViewModel = {
                Uid: "",
                UserInfoId: "",
                FirstName: "",
                LastName: "",
                Email: "",
                Phone: "",
                UserName: "",
                Password: "",
                ConfirmPassword: "",
                AccessLevel: "1", //1 for Level 1 User
                Status: "0",
                AreaSpeakerList: null
            }
           
        } else {
            $scope.userViewModel = {
                Uid: selectedUser.Uid,
                UserInfoId: selectedUser.UserInfoId,
                FirstName: selectedUser.FirstName,
                LastName: selectedUser.LastName,
                Email: selectedUser.Email,
                Phone: selectedUser.Phone,
                UserName: selectedUser.UserName,
                Password: "",
                ConfirmPassword: "",
                AccessLevel: selectedUser.AccessLevel.toString(),
                Status: selectedUser.Status.toString(),
                AreaSpeakerList: selectedUser.AreaSpeakerList
            }
        }
    };

    $scope.manageUser = function () {

        $scope.$broadcast('show-errors-check-validity');
        toggleAssignedAreaGroups();
        validatePassword();
        confirmPassword();

        if ($scope.userForm.$invalid || $scope.assignedAreaGroups.length === 0) {
            return;
        } else {
            $scope.userViewModel.AreaSpeakerList = convertGroupsToTableStructure();
            userInfoDataFactory.manageUser($scope.userViewModel)
                .then(function (data) {
                    $('#userModal').modal('hide');
                    $timeout(function () {
                        $scope.loadUsers();
                    }, 0);
                },
                function (data) {
                    $log.error(data.Message);
                });
        }
    };

    $scope.enableDisableUser = function () {

        if ($scope.currentUserUid !== "") {
            var selectedUser = _.find($scope.allUsers, function (user) { return user.Uid === $scope.currentUserUid; });
            selectedUser.Status = (selectedUser.Status == 0) ? "1" : "0";

            userInfoDataFactory.manageUser(selectedUser)
                .then(function (data) {
                    $('#changeStatusModal').modal('hide');
                    $timeout(function () {
                        $scope.loadUsers();
                    }, 0);
                },
                function (data) {
                    $log.error(data.Message);
                });

        }
    };

    $scope.deleteUser = function () {
        if ($scope.deleteUserUid !== "") {
            userInfoDataFactory.deleteUser($scope.deleteUserUid)
                .then(function (data) {
                    $('#deleteUserModal').modal('hide');
                    $timeout(function () {
                        $scope.loadUsers();
                    }, 0);
                },
                function (data) {
                    $log.error(data.Message);
                });
        }
    };

    var toggleAssignedAreaGroups = function () {
        if ($scope.assignedAreaGroups.length === 0) {
            $scope.assignedGroupsStyle = { border: "2px solid #a94442" };
            $scope.hasGroups = false;
        } else {
            $scope.assignedGroupsStyle = { border: "2px solid #D5D8DB" };
            $scope.hasGroups = true;
        }
    };

    var updateValidatePasswordMessage = function () {

        $scope.validationMessage = ""; 
        if ($scope.userViewModel.Password.length < 6) {
            $scope.validationMessage="Password must be at least 6 characters.";
        }

        if ($scope.userViewModel.Password.search(/[a-z]/) < 0) {
            $scope.validationMessage="Password must contain at least one lowercase.";
        }

        if ($scope.userViewModel.Password.search(/[A-Z]/) < 0) {
            $scope.validationMessage="Password must contain at least one uppercase.";
        };

        if (/^[a-zA-Z0-9- ]*$/.test($scope.userViewModel.Password) === true) {
            $scope.validationMessage="Password must contain at least one non letter or digit character.";
        }
    };

    var validatePassword = function () {

        if ($scope.editUser) {
            $scope.isPasswordRequired = false;
        } else {
            $scope.isPasswordRequired = !$scope.userViewModel.Password;
            updateValidatePasswordMessage();
        }
        
    };

    var confirmPassword = function () {
        $scope.isConfirmPasswordRequired = false;
      
        if ($scope.userViewModel.Password !== $scope.userViewModel.ConfirmPassword) {
            $scope.isConfirmPasswordRequired = true;
        } else {
            if ($scope.userViewModel.ConfirmPassword === "" && $scope.currentUserUid === "") {
                $scope.isConfirmPasswordRequired= true;
            }
        }
    };

    $scope.passwordChanged=function () {
        validatePassword();
        confirmPassword();
    };

    $scope.confirmPasswordChanged = function () {
        confirmPassword();
    };
   
    $scope.verifyUniqueEmail = function () {
        if ($scope.userViewModel.Email) {
            $http.post('api/account/VerifyUniqueEmail', {
                userUid: $scope.userViewModel.Uid,
                userEmail: $scope.userViewModel.Email
            }).
                then(function (response) {
                    //if the email is unique response.data.result=true
                    $scope.emailAlreadyUsed = !response.data.result;
                });
        }
    };
    ////////////////////////////////////////////
    //Angular Tree control data and operations 
    ////////////////////////////////////////////

    $scope.availableAreaGroups = [];
    $scope.assignedAreaGroups = [];
    $scope.selectedSourceGroups = [];
    $scope.selectedDestinationGroups = [];
    $scope.selectedSourceTreeNodes = [];
    $scope.selectedDestinationTreeNodes = [];
    $scope.sourceExpandedNodes = [];
    $scope.destinationExpandedNodes = [];

    $scope.treeOptions = {
        nodeChildren: "Groups",
        dirSelectable: true,
        multiSelection: true
    }

    var initializeTree = function (selectedUser) {

        if (selectedUser != null) {
            var list = convertGroupsToTreeStructure(selectedUser.AreaSpeakerList);
            angular.copy(list, $scope.assignedAreaGroups);
        } else {
            var someGroups = [];

            angular.forEach($scope.allAreas, function (val) {
                if (val.Groups.length > 0) {
                    someGroups.push(val);
                }
            });

            angular.copy(someGroups, $scope.availableAreaGroups);
            angular.copy([], $scope.assignedAreaGroups);
            $scope.destinationExpandedNodes = [];
        }
        angular.copy([], $scope.selectedSourceTreeNodes);
        angular.copy([], $scope.selectedDestinationTreeNodes);
        $scope.sourceExpandedNodes = [];
    }

    var convertGroupsToTreeStructure = function (areaGroupList) {
        var treeList = [];
        var tempAllAreas = [];

        var areaIdList = _.uniq(_.map(areaGroupList, "AreaUid"));
        var groupIdList = _.uniq(_.map(areaGroupList, "SpeakerGroupUid"));

        angular.copy($scope.allAreas, tempAllAreas);

        areaIdList.forEach(function (id) {
            var area = _.find(tempAllAreas, function (areaItem) { return id === areaItem.Uid });
            if (area != undefined) {
                var newGroup = [];
                groupIdList.forEach(function (gid) {
                    var group = _.find(area.Groups, function (groupItem) { return gid === groupItem.Uid });
                    if (group != undefined) {
                        newGroup.push(group);
                    }
                });

                area.Groups = newGroup;
                treeList.push(area);
            }
        });

        $scope.destinationExpandedNodes = [];
        treeList = _.sortBy(treeList, 'Name');
        treeList.forEach(function (area) {
            area.Groups = _.sortBy(area.Groups, 'Name');
            $scope.destinationExpandedNodes.push(area);
        });

        //Set Available Areas and Groups
        tempAllAreas = [];
        angular.copy($scope.allAreas, tempAllAreas);

        groupIdList.forEach(function (gid) {
            for (var i = 0; i < tempAllAreas.length; i++) {
                var index = _.findIndex(tempAllAreas[i].Groups, function (item) {
                    return item.Uid === gid;
                });
                if (index !== -1) {
                    tempAllAreas[i].Groups.splice(index, 1);
                    break;
                }
            }
        });

        var finalAreas = _.filter(tempAllAreas, function (area) { return area.Groups.length !== 0; });

        angular.copy(finalAreas, $scope.availableAreaGroups);

        return treeList;
    };

    var convertGroupsToTableStructure = function () {
        var tableList = [];

        $scope.assignedAreaGroups.forEach(function (area) {
            area.Groups.forEach(function (group) {
                var newGroup = {
                    "AreaName": area.Name,
                    "AreaUid": area.Uid,
                    "SpeakerGroupName": group.Name,
                    "SpeakerGroupUid": group.Uid
                };
                tableList.push(newGroup);
            });
        });

        return tableList;
    };

    var unionGroups = function (incomingAreaGroups, existingAreaGroups) {

        if (existingAreaGroups.length === 0) {
            return incomingAreaGroups;
        }

        var flattenAreaGroups = [];

        existingAreaGroups.forEach(function (area) {
            area.Groups.forEach(function (group) {
                flattenAreaGroups.push(group);
            });
            flattenAreaGroups.push(area);
        });

        return _.union(incomingAreaGroups, flattenAreaGroups);
    };

    var updateTreeControls = function (selectedSourceTreeNodes, selectedSourceGroups, assignedAreaGroups, availableAreaGroups, allAreas) {

        if (selectedSourceTreeNodes.length === 0) {
            return;
        }

        //////////////////////////////////////////////////
        //Add the selected group to the destination tree
        //////////////////////////////////////////////////

        var tempAllAreas = [];
        var groupList = [];
        var areaList = [];
        var totalGroups = unionGroups(selectedSourceGroups, assignedAreaGroups);

        var areaIdList = _.filter(_.uniq(_.map(totalGroups, "AreaUid")), function (uid) { return uid != undefined });

        totalGroups.forEach(function (item) {
            if (item.AreaUid != undefined) {
                groupList.push(item);
            }
        });

        angular.copy(allAreas, tempAllAreas);

        areaIdList.forEach(function (id) {
            var area = _.find(tempAllAreas, function (areaItem) { return id === areaItem.Uid });
            if (area != undefined) {
                area.Groups = [];
                areaList.push(area);
            }
        });

        areaList.forEach(function (area) {
            groupList.forEach(function (group) {
                if (area.Uid === group.AreaUid) {
                    area.Groups.push(group);
                }
            });
        });

        $scope.destinationExpandedNodes = [];
        areaList = _.sortBy(areaList, 'Name');
        areaList.forEach(function (area) {
            area.Groups = _.sortBy(area.Groups, 'Name');
            $scope.destinationExpandedNodes.push(area);
        });

        angular.copy(areaList, assignedAreaGroups);

        //////////////////////////////////////////////////
        //Remove the selected group from the source tree
        //////////////////////////////////////////////////

        tempAllAreas = [];
        angular.copy(allAreas, tempAllAreas);

        groupList.forEach(function (group) {
            for (var i = 0; i < tempAllAreas.length; i++) {
                var index = _.findIndex(tempAllAreas[i].Groups, function (item) {
                    return item.Uid === group.Uid;
                });
                if (index !== -1) {
                    tempAllAreas[i].Groups.splice(index, 1);
                    break;
                }
            }
        });

        var finalAreas = _.filter(tempAllAreas, function (area) { return area.Groups.length !== 0; });

        angular.copy(finalAreas, availableAreaGroups);

        angular.copy([], selectedSourceTreeNodes); //angular.copy operates by reference

    };

    $scope.addSelection = function () {

        updateTreeControls($scope.selectedSourceTreeNodes, $scope.selectedSourceGroups, $scope.assignedAreaGroups,
            $scope.availableAreaGroups, $scope.allAreas);

        toggleAssignedAreaGroups();
    };

    $scope.removeSelection = function () {

        updateTreeControls($scope.selectedDestinationTreeNodes, $scope.selectedDestinationGroups, $scope.availableAreaGroups,
            $scope.assignedAreaGroups, $scope.allAreas);

        toggleAssignedAreaGroups();
    };

    $scope.processSelectedGroups = function (treeNode, isSelected, selectedNodes, areaGroups, selectedGroups) {
        var index = -1;

        if (isSelected) {
            if (treeNode.Groups != undefined) {
                treeNode.Groups.forEach(function (group) {
                    index = _.findIndex(selectedNodes, function (item) {
                        return group.Uid === item.Uid;
                    });
                    if (index === -1) {
                        selectedNodes.push(group);
                    }
                });
            } else {
                var number = 0;
                selectedNodes.forEach(function (node) {
                    if (node.AreaUid === treeNode.AreaUid) {
                        number++;
                    }
                });

                var parent = _.find(areaGroups, function (item) {
                    return item.Uid === treeNode.AreaUid;
                });

                if (number === parent.Groups.length) {
                    selectedNodes.push(parent);
                }
            }
        } else {
            if (treeNode.Groups != undefined) {

                treeNode.Groups.forEach(function (group) {
                    index = _.findIndex(selectedNodes, function (item) {
                        return treeNode.Uid === item.AreaUid;
                    });
                    if (index !== -1) {
                        selectedNodes.splice(index, 1);
                    }
                });

            } else {
                index = _.findIndex(selectedNodes, function (item) {
                    return item.Uid === treeNode.AreaUid;
                });
                if (index !== -1) {
                    selectedNodes.splice(index, 1);
                }
            }
        }

        angular.copy(selectedNodes, selectedGroups);
    };

}).controller("SourceTreeController", function ($scope) {

    $scope.selectSourceEvent = function (treeNode, isSelected) {
        $scope.processSelectedGroups(treeNode, isSelected, $scope.selectedSourceTreeNodes, $scope.availableAreaGroups, $scope.selectedSourceGroups);
    };

}).controller("DestinationTreeController", function ($scope) {

    $scope.selectDestinationEvent = function (treeNode, isSelected) {
        $scope.processSelectedGroups(treeNode, isSelected, $scope.selectedDestinationTreeNodes, $scope.assignedAreaGroups, $scope.selectedDestinationGroups);

    };
});

app.directive('showErrors', function () {
    return {
        restrict: 'A',
        require: '^form',
        link: function (scope, el, attrs, formCtrl) {
            var inputElement = angular.element(el[0].querySelector("[name]"));
            var inputName = inputElement.attr('name');

            scope.$on('show-errors-check-validity', function () {
                if (formCtrl[inputName].$invalid) {
                    formCtrl[inputName].$setDirty(inputName, true);
                }
                el.toggleClass('has-error', formCtrl[inputName].$invalid);
            });
        }
    }
});

app.directive('validateEmail', function () {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return {
        require: 'ngModel',
        restrict: '',
        link: function (scope, elm, attrs, ctrl) {
            if (ctrl && ctrl.$validators.email) {
                // this will overwrite the default Angular email validator
                ctrl.$validators.email = function (modelValue) {
                    return ctrl.$isEmpty(modelValue) || filter.test(modelValue);
                };
            }
        }
    };
});



