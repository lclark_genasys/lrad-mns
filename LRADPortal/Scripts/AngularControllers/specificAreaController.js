app.controller("SpecificAreaController", function ($scope, $auth, $state, $log, $stateParams, $timeout, $filter, $rootScope, areaGroupDataFactory, authorizationService, speakerService, ModalService, speakerLimiter, LRADUpdates, lradDeviceService) {
    var selectedStatus = ["0"];

    $scope.LRADUpdates = LRADUpdates;

    $scope.areaUid = $state.params.areaId;
    $scope.groupUid = $state.params.groupId;
    $scope.isCheckAll = false;
    $scope.allAreas = [];
    $scope.selectedArea = undefined;
    $scope.allSpeakerGroups = [];
    $scope.selectedSpeakerGroups = [];
    $scope.selectedGroup = undefined;
    $scope.isAuthorize = authorizationService.isAuthorize();
    $scope.persistSound = {};
    $scope.hideMeFromSpeakers = "show";
    $scope.selectedPlayButton = null;
    $scope.selectedSpeakers = false;
    $scope.speaker = {
        IsSelected: [false]
    };
    $scope.group = {
        IsSelected: false
    }

    // Map for easy speaker access.
    $scope.allSpeakerGroups = [];
    $scope.repeatTimeOptions = [1,2,3,4,5];
    $scope.sounds = [{ id: 1, name: "General" }, { id: 2, name: "General Custom" }, { id: 3, name: "Emergency" }, { id: 4, name: "Emergency Custom" }];
    

    //var loadGeneralSoundFiles = function () {
    //    areaGroupDataFactory.getGeneralSounds()
    //        .then(function (generalSounds) {
    //            $scope.generalSounds = generalSounds;
    //        },
    //        function (data) {
    //            $log.error("Unable to get general sounds (SAC).");
    //        });
    //};

    //var loadEmergencySoundFiles = function () {
    //    areaGroupDataFactory.getEmergencySounds()
    //        .then(function (emergencySounds) {
    //            $scope.emergencySounds = emergencySounds;
    //        },
    //        function (data) {
    //            $log.error("Unable to get emergency sounds.");
    //        });
    //};

    //var loadGeneralCustomSounds = function () {
    //    areaGroupDataFactory.customSounds(2)
    //        .then(function (generalCustomSounds) {
    //            $scope.generalCustomSounds = generalCustomSounds;
    //        },
    //        function (data) {
    //            $log.error("Unable to get custom sounds.");
    //        });
    //};

    //var loadEmergencyCustomSounds = function () {
    //    areaGroupDataFactory.customSounds(3)
    //        .then(function (emergencyCustomSounds) {
    //            $scope.emergencyCustomSounds = emergencyCustomSounds;
    //        },
    //        function (data) {
    //            $log.error("Unable to get custom sounds.");
    //        });
    //};

    //loadGeneralSoundFiles();
    //loadEmergencySoundFiles();
    //loadGeneralCustomSounds();
    //loadEmergencyCustomSounds();

    $scope.toggleSelectAll = function() {
        for (var i = 0; i < $scope.selectedSpeakerGroups.length; i++) {
            var group = $scope.selectedSpeakerGroups[i];
            var loopSpeakers = function() {
                var totalSp = group.Speakers.length;
                for (var j = 0; j < totalSp; j++) {
                    group.Speakers[j].IsSelected = $scope.isCheckAll;
                }
                group.IsSelected = $scope.isCheckAll;
            };
            if ($scope.selectedGroup == null || $scope.selectedGroup == undefined) { // for all group
                loopSpeakers();
            } else if ($scope.selectedGroup !== null && $scope.selectedGroup !== undefined && group.Uid === $scope.selectedGroup.Uid) { // for selected group
                loopSpeakers();
                break;
            }
        }
    };

    $scope.setChange = function(selection) {
        if (selection === "area") {
            $scope.selectedSpeakerGroups = $scope.selectedArea ? $scope.selectedArea.Groups : $scope.allSpeakerGroups;
            $scope.selectedGroup = undefined;
            $scope.areaUid = $scope.selectedArea ? $scope.selectedArea.Uid : "";
            $scope.groupUid = "";
            $scope.$broadcast("filterSelectionChanged", {
                AreaUid: $scope.areaUid,
                GroupUid: ""
            });
        } else {
            $scope.groupUid = $scope.selectedGroup ? $scope.selectedGroup.Uid : "";
            $scope.$broadcast("filterSelectionChanged", {
                AreaUid: $scope.selectedArea ? $scope.selectedArea.Uid : "",
                GroupUid: $scope.selectedGroup ? $scope.selectedGroup.Uid : ""
            });
        }

        $scope.isCheckAll = false;
        $scope.toggleSelectAll();
        $timeout(function() {
            $scope.$broadcast("filterChanged");
        }, 100);
    };

    $("#statusSelection").change(function() {
        var optionSelected = $(this).val();
        if (optionSelected != null) {
            for (var i = 0; i < optionSelected.length; i++) {
                if ($.inArray(optionSelected[i], selectedStatus) == -1) {
                    if (optionSelected[i] === "0") {
                        $(this).val(["0"]);
                        selectedStatus = ["0"];
                    } else {
                        selectedStatus = _.filter(optionSelected, function(num) { return num !== "0"; });
                        $(this).val(selectedStatus);
                    }
                    break;
                }
            }
        } else {
            $(this).val(["0"]);
            selectedStatus = ["0"];
        }

        $(this).selectpicker('render');

    });

    $scope.onDeleteSpeakerConfirm = function() {
        areaGroupDataFactory.deleteSpeaker($scope.speakerToModify.Uid).then(function() {
                var group = _.find($scope.selectedSpeakerGroups, function(group) {
                    return group.Uid.toUpperCase() === $scope.speakerToModify.SpeakerGroupUid.toUpperCase();
                });
                group.Speakers.splice(group.Speakers.indexOf($scope.speakerToModify), 1);
            },
            function(data) {
                $log.error("Unable to delete speaker " + $scope.speakerToModify.Uid);
            });
    };

    $scope.onDeleteSpeakerGroupConfirm = function() {
        var playingSpeakers = _.filter($scope.groupToModify.Speakers, ["DeviceInfo.MnsState.StatusFlags.AudioOn", true]);
        console.log(playingSpeakers);
        var deleteGroup = function() {
            areaGroupDataFactory.deleteSpeakerGroup($scope.groupToModify.Uid).then(function() {
                    var area = _.find($scope.allAreas, function(area) {
                        return area.Uid.toUpperCase() === $scope.groupToModify.AreaUid.toUpperCase();
                    });
                    if (area.Groups.indexOf($scope.groupToModify) > -1)
                        area.Groups.splice(area.Groups.indexOf($scope.groupToModify), 1); // remove the orignal group instance from its group
                    if ($scope.selectedSpeakerGroups.indexOf($scope.groupToModify) > -1)
                        $scope.selectedSpeakerGroups.splice($scope.selectedSpeakerGroups.indexOf($scope.groupToModify), 1);
                    if ($scope.allSpeakerGroups.indexOf($scope.groupToModify) > -1)
                        $scope.allSpeakerGroups.splice($scope.allSpeakerGroups.indexOf($scope.groupToModify), 1); // remove the orignal group instance from all group
                },
                function(data) {
                    $log.error("Unable to delete group " + $scope.groupToModify.Uid);
                });
        };
        if (playingSpeakers.length > 0) {
            var devices = [];
            _.each(playingSpeakers, function(speaker) {
                devices.push({ "Uid": speaker.Uid });
            });
            lradDeviceService.stop(devices).then(function(success) {
                $rootScope.$broadcast("GroupDeleted");
                deleteGroup();
            });
        } else {
            deleteGroup();
        }
    };

    var loadAllSpeakerGroups = function() {
        var allGroups = [];
        for (var i = 0; i < $scope.allAreas.length; i++) {
            $scope.allAreas[i].Groups.forEach(function addGroup(value) { allGroups.push(value); });
        }
        $scope.allSpeakerGroups = allGroups;
    };

    var loadAllAreas = function(callback) {
        areaGroupDataFactory.getAssignedAreas()
            .then(function(areas) {
                    $scope.allAreas = _.sortBy(areas, 'Name');
                    loadAllSpeakerGroups();

                    if ($scope.areaUid) {
                        $scope.selectedArea = _.find(areas, function(item) {
                            return item.Uid.toUpperCase() === $scope.areaUid.toUpperCase();
                        });
                    }

                    if ($scope.selectedArea) {
                        $scope.selectedSpeakerGroups = $scope.selectedArea.Groups;
                    } else {
                        $scope.selectedSpeakerGroups = $scope.allSpeakerGroups;
                    }

                    if ($scope.groupUid) {
                        $scope.selectedGroup = _.find($scope.selectedSpeakerGroups, function(item) {
                            return item.Uid.toUpperCase() === $scope.groupUid.toUpperCase();
                        });
                    }
                    if (callback)
                        callback();
                    $scope.$broadcast("initializePanel");
                },
                function(data) {
                    $log.error("Unable to reload area and speaker group data");
                });
    };

    $scope.showAddSpeakerGroupModal = function () {
        ModalService.showModal({
            templateUrl: "Templates/SpeakerGroupModal.html",
            controller: "speakerGroupModalController",
            inputs: {
                speakerGroup: null,
                allAreas: $scope.allAreas,
                allGroups: $scope.allSpeakerGroups,
                allSounds: $scope.allSounds,
                allGeneralSounds: $scope.generalSounds,
                allEmergencySounds: $scope.emergencySounds,
                speakerGroups: $scope.selectedSpeakerGroups,
                selectedArea: $scope.selectedArea
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                $scope.$broadcast("speakerGroupAdded", {
                    group: result.speakerGroup
                });
            });
        });
    };

    $scope.showAddSpeakerModal = function (latLng) {
        areaGroupDataFactory.getSpeakersCount().then(function (data) {
            if (data <= speakerLimiter) {
                ModalService.showModal({
                    templateUrl: "Templates/SpeakerModal.html",
                    controller: "speakerModalController",
                    inputs: {
                        speaker: null,
                        allAreas: $scope.allAreas,
                        speakerGroups: $scope.selectedSpeakerGroups,
                        selectedArea: $scope.selectedArea,
                        selectedGroup: $scope.selectedGroup,
                        latLng: latLng
                    }
                }).then(function (modal) {
                    modal.element.modal();
                    modal.close.then(function (result) {
                    });
                });
            } else {
                ModalService.showModal({
                    templateUrl: "Templates/SpeakerErrorModal.html",
                    controller: "speakerErrorModalController"
                }).then(function (modal) {
                    modal.element.modal();
                    modal.close.then(function (result) { });
                });
            }
        });
    };

    $scope.getAlertForTooltip = function (deviceInfo) {
        if (deviceInfo && deviceInfo.Status && deviceInfo.Status.toUpperCase()!=="OK")
            return deviceInfo.Status;
        return "";
    };

    $rootScope.$on("LRAD.DevicePropertyChanged", function (evt, data) {
        console.log('LRAD.DevicePropertyChanged');
        var speakerStates = data["DevicePropertyChanged"];
        angular.forEach(speakerStates, function (state, index) {
            angular.forEach(state, function (props, key) {
                var speaker = _.find(_.flatMap($scope.selectedSpeakerGroups, "Speakers"), { 'Uid': key.toLowerCase() });
                if (speaker) {
                    speaker.DeviceInfo = _.merge(speaker.DeviceInfo, props);
                    if (_.has(props, "MnsState.StatusFlags.AudioOn")) // play/stop status changed
                        $rootScope.$broadcast("audioOn");
                    if (_.has(props, "Status")) // AlertRaised
                        speaker.AlertMessages = $scope.getAlertForTooltip(speaker.DeviceInfo);
                }
            });
        });
    });

    $rootScope.$on("speakerAdded", function (evt, data) {
        data.AlertMessages = $scope.getAlertForTooltip(data.DeviceInfo);
    });

    $scope.$on('$destroy', function () {
        $scope.LRADUpdates.close();
    });

    function activate() {
        $scope.LRADUpdates.connect();

        if ($state.is("specificarea.table") || $state.is("specificarea.map")) {
            if ($state.params != undefined) {
                $scope.areaUid = $state.params.areaId;
                $scope.groupUid = $state.params.groupId;
                loadAllAreas(function() {
                    lradDeviceService.getAllDevices().then(function (devices) {
                        _.each(devices, function(device) {
                            var speaker = _.find(_.flatMap($scope.selectedSpeakerGroups, "Speakers"), { Uid: device.Uid });
                            if (speaker) {
                                speaker.DeviceInfo = device;
                                speaker.AlertMessages = $scope.getAlertForTooltip(device);
                            }
                        });
                    });
                });

                $timeout(function () {
                    $('.selectpicker').selectpicker();
                }, 500);
            }
        }
    }

    activate();
});