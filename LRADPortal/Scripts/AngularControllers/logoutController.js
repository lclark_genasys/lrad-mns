﻿app.controller("LogoutController", function ($scope, $state, $http, $location, $auth, $timeout, $rootScope) {
    if (!$auth.isAuthenticated()) {
        return;
    }
    $auth.logout()
      .then(function () {
          $rootScope.$broadcast("userLogout");
        });
});