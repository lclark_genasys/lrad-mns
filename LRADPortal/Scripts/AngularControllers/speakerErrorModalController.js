app.controller("speakerErrorModalController", function ($scope) {
    $scope.externalTitle = "Reached max limit of speakers."
    $scope.externalErrorMessage = 'You\'ll need to remove some speakers before adding more.';
});