app.controller("soundModalController", function ($scope, $log, $element, close, areaGroupDataFactory, $timeout) {

    $scope.soundTypes = [];
    $scope.newSound = $scope.newSound || {};

    areaGroupDataFactory.soundTypes()
        .then(function (data) {
            $scope.soundTypes = data.data;
            $scope.newSound.soundType = data.data[0].Id.toString();
        },
        function (data) {
            $log.error("Unable to get list of sound types.");
        });

    $scope.saveSound = function (result) {
        if (result && !isEmpty(result)) {
            $scope.newSound = angular.copy(result);

            var newData = [{
                Name: $scope.newSound.soundName,
                Filename: $scope.newSound.soundFile,
                Type: $scope.newSound.soundType,
                Description: $scope.newSound.soundDesc
            }];

            areaGroupDataFactory.uploadSoundFiles(newData)
            .then(function () {
            },
            function (data) {
                console.log('Unable to save sound.');
            });
        } else {
            console.log('Result was empty.');
        }
        close(result, 500);
    };

    $scope.close = function (result) {
        close(result, 500);
    };

    function isEmpty(obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }

});