﻿app.controller("SpecificAreaPanelAnnouncingController", function ($scope, $log, $auth, $state, $stateParams, $http, $rootScope, $filter, $interval, $window, $timeout, areaGroupDataFactory, lradDeviceService, timerFactory, clickEventFactory, ModalService) {

    Array.prototype.allValuesSame = function () {
        for (var i = 1; i < this.length; i++) {
            if (this[i] !== this[0])
                return false;
        }
        return true;
    }

    function checkIfSelectedSpeakersArePlaying() {
        return _.filter(_.flatMap(_.map($scope.$parent.selectedSpeakerGroups, "Speakers")), function (speaker) { return speaker.IsSelected && _.has(speaker, "DeviceInfo.MnsState.StatusFlags.AudioOn") && speaker.DeviceInfo.MnsState.StatusFlags.AudioOn }).length > 0;
    }

    function getSelectedSpeakers() {
        return _.filter(_.flatMap(_.map($scope.$parent.selectedSpeakerGroups, "Speakers")), function (speaker) { return speaker.IsSelected });
    }

    function getSelectedSound(speakers) {
        var sounds = _.map(speakers, "PredefinedSound");
        return sounds.length > 0 && sounds.allValuesSame() && sounds[0] ? sounds[0] : 1;
    }

    function getSelectedRepeat(speakers) {
        var repeat = _.map(speakers, "Repeat");
        return repeat.length > 0 && repeat.allValuesSame() && repeat[0] && repeat[0] !== 0 && repeat[0] !== null ? repeat[0] : false;
    }

    function updateAnnouncePanel() {
        var selectedSpeakers = getSelectedSpeakers();
        $scope.selectedSound = _.find($scope.$parent.sounds, { "id": getSelectedSound(selectedSpeakers) });
        $scope.repeat = getSelectedRepeat(selectedSpeakers);
        if (selectedSpeakers.length > 0) {
            $scope.isPlaying = checkIfSelectedSpeakersArePlaying();
            $scope.isDisabled = $scope.isPlaying;
            $scope.isPlayDisabled = false;
        } else {
            $scope.isDisabled = true;
            $scope.isPlayDisabled = true;
        }
    }

    $scope.$watch('selectedSpeakers', function (newValue, oldValue) {
        if (newValue !== oldValue)
            updateAnnouncePanel();
    });

    $scope.$watch('speaker.IsSelected', function (newValue, oldValue) {
        if (newValue !== oldValue)
            updateAnnouncePanel();
    });

    function activate() {
        $scope.selectedSound = $scope.$parent.sounds[0];
        $scope.repeat = false;
        $scope.isDisabled = true;
        $scope.isPlayDisabled = true;
    }

    activate();

    $scope.toggleSoundPlay = function(event) {
        if (!$scope.$parent.selectedSpeakers && event.currentTarget.className.indexOf("disabled") > -1) {
            return;
        }
        var selectedSpeakers = getSelectedSpeakers();
        var devices = [];
        angular.forEach(selectedSpeakers, function(speaker) {
            if (!$scope.isPlaying) {
                speaker.StartTime = new Date();
                speaker.PredefinedSound = $scope.selectedSound.id;
                speaker.Repeat = $scope.repeat;
                devices.push({ "Uid": speaker.Uid, "Key": $scope.selectedSound.name, "Attenuation": speaker.GeneralVolume, "Repeat": speaker.Repeat });
            } else {
                speaker.StartTime = null;
                devices.push({ "Uid": speaker.Uid });
            }
            areaGroupDataFactory.updateSpeaker(speaker);
        });
        if ($scope.isPlaying)
            lradDeviceService.stop(devices).then(function (success) {
            });
        else
            lradDeviceService.play(devices).then(function (success) {
            });
        updateAnnouncePanel();
    };

    $rootScope.$on("audioOn", function (evt, data) {
        updateAnnouncePanel();
    });

    $rootScope.$on("GroupDeleted", function (evt, data) {
        updateAnnouncePanel();
    });

    $scope.$on("filterChanged", function (event, args) {
        $scope.isPlaying = false;
        $scope.isDisabled = true;
        $scope.isPlayDisabled = true;
    });
});