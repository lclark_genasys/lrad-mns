app.controller("speakerModalController", function ($scope, $rootScope, $log, $element, speaker, allAreas, speakerGroups, selectedArea, selectedGroup, lradDeviceService, latLng, close, speakerService, areaGroupDataFactory, $timeout, ModalService) {
    $scope.speakerFocus = true;
    $scope.isAddMode = speaker == null;
    
    $scope.SpeakeTypes = speakerService.speakeTypes;
    $scope.allAreas = allAreas;

    function stringifyData(resp) {
        var p1 = JSON.stringify(eval("(" + resp + ")"));
        return JSON.parse(p1);
    }

    var initializeSpeakerViewModel = function () {
        var updateSelectedNewGroupArea = function () {
            if (selectedArea) {
                $scope.speakerVM.Area = selectedArea;
                if (selectedGroup) {
                    $scope.speakerVM.Group = selectedGroup;
                } else {
                    $scope.speakerVM.Group = selectedArea.Groups[0];
                }
            } else if (!selectedArea && selectedGroup) { // if all area, but it has selected group
                $scope.speakerVM.Area = _.find(allAreas, function (a) {
                    return a.Uid === selectedGroup.AreaUid;
                });
                $scope.speakerVM.Group = selectedGroup;
            } else {
                $scope.speakerVM.Area = allAreas.length > 0 ? allAreas[0] : {};
                $scope.speakerVM.Group = allAreas.length > 0 ? allAreas[0].Groups[0] : {};
            }
        };

        $scope.speakerVM = {
            Area: allAreas.length > 0 ? allAreas[0] : {},
            Group: allAreas.length > 0 ? allAreas[0].Groups[0] : {},
            UnassignedSpeaker: undefined,
            Type: $scope.SpeakeTypes[0],
            Name: "",
            IPAddress: "",
            GeneralVolume: -6,
            EmergencyVolume: -6,
            latitude: latLng ? latLng.latitude : "",
            longitude: latLng ? latLng.longitude : "",
            SatelliteID: "",
            MacAddress: "",
            NetworkName: ""
        };
        updateSelectedNewGroupArea();
    };

    $scope.discoverSpeakers = function () {
        getUnassignedSpeakers();
        getDiscoveredSpeakers();
    }

    function getDiscoveredSpeakers() {
        $scope.isDiscovering = true;

        areaGroupDataFactory.GetDiscoveredSpeakers().then(function(resp) {
                if (typeof resp === 'undefined' || resp === '' || resp === '[]' || resp === 'An error occurred while sending the request.') {
                    $scope.isDiscovering = false;
                    return;
                }

                var p1 = stringifyData(resp);

                areaGroupDataFactory.SpeakerExists(p1)
                    .then(function(speakers) {
                        $scope.UnassignedSpeakers = $scope.UnassignedSpeakers.concat(speakers);
                        $scope.isDiscovering = false;
                    }, function(error) {
                        $scope.isDiscovering = false;
                        console.log('failed to get speaker MacAddress: ', error);
                    });
            },
            function(error) {
                $scope.isDiscovering = false;
                console.log('we did not get GetUnassignedLRADs: ', error);
            });
    }

    function getUnassignedSpeakers() {
        $scope.UnassignedSpeakers = [];

        areaGroupDataFactory.getUnassignedSpeakers()
            .then(function(unassignedSpeakers) {
                    $scope.UnassignedSpeakers = unassignedSpeakers;
                },
                function(data) {
                    $log.error(data.Message);
                });
    }

    // Load the unassigned speakers first
    if ($scope.isAddMode) {
        getUnassignedSpeakers();
        getDiscoveredSpeakers();
    }

    // Note:
    // Moved this block of code out of "getUnassignedSpeakers" method since. getUnassignedSpeakers should only build up the unassigned speakers dropdown.
    // Not be responsible for add/edit speaker.
    if (speaker) { // for editing
        $scope.speakerVM = angular.copy(speaker);
        $scope.deviceInfo = speaker.DeviceInfo;
        $scope.speakerVM.Group = _.find(speakerGroups, function (sg) {
            return sg.Uid === speaker.SpeakerGroupUid;
        });
        $scope.speakerVM.Area = _.find(allAreas, function (a) {
            return a.Uid === $scope.speakerVM.Group.AreaUid;
        });
        $scope.speakerVM.Type = $scope.SpeakeTypes[$scope.speakerVM.Type];
    } else { // for adding
        initializeSpeakerViewModel();
    }

    // watch for unassigned speaker, if user choose one from the list, it will fill the blanks for the speaker modal based on the selected unassigned speaker.
    $scope.$watch('speakerVM.UnassignedSpeaker', function(newVal, oldVal) {
        if (angular.isDefined(newVal) && newVal !== null) {
            if (angular.equals(newVal, oldVal)) return;
            $scope.speakerVM.Uid = newVal.Uid;
            $scope.speakerVM.Type = $scope.SpeakeTypes[newVal.Type];
            $scope.speakerVM.IPAddress = newVal.IPAddress;
            $scope.speakerVM.GeneralVolume = (newVal.GeneralVolume) ? newVal.GeneralVolume : -6;
            $scope.speakerVM.EmergencyVolume = (newVal.EmergencyVolume) ? newVal.EmergencyVolume : -6;
            if (!latLng || latLng.latitude === "")
                $scope.speakerVM.latitude = newVal.Latitude;
            if (!latLng || latLng.longitude === "")
                $scope.speakerVM.longitude = newVal.Longitude;
            $scope.speakerVM.SatelliteID = newVal.SatelliteID;
            $scope.speakerVM.MacAddress = (newVal.MacAddress) ? newVal.MacAddress : "";
            $scope.speakerVM.NetworkName = (newVal.Name) ? newVal.Name : "";
        } else if ($scope.isAddMode && $scope.speakerVM) {
            var name = $scope.speakerVM.Name;
            initializeSpeakerViewModel();
            $scope.speakerVM.Name = name;
        }
    }, true);

    $scope.sliderOptions = {
        floor: -6,
        ceil: 0,
        step: 3,
        precision: 0,
        showTicks: true,
        showTicksValues: true,
        translate: function (val) {
            return val + (val === 0 ? "dB (Max)" : "dB");
        }
    };

    $scope.newSpeakerAreaChanged = function () {
        $scope.speakerVM.Group = $scope.speakerVM.Area ? $scope.speakerVM.Area.Groups[0] : {};
    };

    $scope.manageSpeaker = function() {
        if (!$scope.newSpeakerForm.$invalid) {
            var speakerType = (typeof $scope.speakerVM.Type == 'object') ? $scope.speakerVM.Type.id : $scope.SpeakeTypes[0].id;
            var generalVolume = ($scope.speakerVM.GeneralVolume != NaN) ? $scope.speakerVM.GeneralVolume : -6;
            var emergencyVolume = ($scope.speakerVM.EmergencyVolume != NaN) ? $scope.speakerVM.EmergencyVolume : -6;

            var newSpeakerData = {
                Uid: $scope.speakerVM.Uid,
                SpeakerGroupUid: $scope.speakerVM.Group.Uid,
                Type: speakerType,
                Name: $scope.speakerVM.Name,
                IPAddress: $scope.speakerVM.IPAddress,
                GeneralVolume: generalVolume,
                EmergencyVolume: emergencyVolume,
                latitude: parseFloat($scope.speakerVM.latitude),
                longitude: parseFloat($scope.speakerVM.longitude),
                SatelliteID: $scope.speakerVM.SatelliteID,
                SoundUid: $scope.speakerVM.SoundUid,
                RepeatTimes: $scope.speakerVM.RepeatTimes,
                Repeat: $scope.speakerVM.Repeat,
                StartTime: $scope.speakerVM.StartTime,
                MacAddress: $scope.speakerVM.MacAddress,
                NetworkName: $scope.speakerVM.NetworkName,
                PredefinedSound: $scope.speakerVM.PredefinedSound,
                IsSelected: $scope.speakerVM.IsSelected,
                DeviceInfo: $scope.isAddMode ? null : speaker.DeviceInfo,
                AlertMessages: $scope.isAddMode ? "" : speaker.AlertMessages
            };

            if ($scope.isAddMode && (angular.isUndefined($scope.speakerVM.UnassignedSpeaker) || $scope.speakerVM.UnassignedSpeaker === null || newSpeakerData.MacAddress)) { // add speaker,not for unassigned speaker case
                areaGroupDataFactory.manageSpeaker(newSpeakerData)
                    .then(function(speaker) {
                            $timeout(function() {
                                lradDeviceService.getDeviceById(speaker.Uid).then(function (deviceInfo) {
                                    for (var i = 0; i < allAreas.length; i++) {
                                        if (allAreas[i].Uid === $scope.speakerVM.Area.Uid) {
                                            for (var j = 0; j < allAreas[i].Groups.length; j++) {
                                                if (allAreas[i].Groups[j].Uid === $scope.speakerVM.Group.Uid) {
                                                    newSpeakerData.Uid = speaker.Uid;
                                                    newSpeakerData.Color = $scope.speakerVM.Group.Color;
                                                    newSpeakerData.IsSelected = $scope.speakerVM.Group.IsSelected;
                                                    newSpeakerData.DeviceInfo = deviceInfo[0];

                                                    allAreas[i].Groups[j].Speakers.push(newSpeakerData);
                                                    $rootScope.$broadcast("speakerAdded", newSpeakerData);
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    $scope.close();
                                });
                            }, 0);
                        },
                        function(data) {
                            if (data.data.Message) {
                                $log.error(data.data.Message);
                            } else {
                                $log.error(data.Message);
                            }
                        });
            } else { // udpate speaker, treat unassigned speaker as update speaker
                newSpeakerData.Uid = $scope.speakerVM.Uid;
                areaGroupDataFactory.updateSpeaker(newSpeakerData)
                    .then(function() {
                            // for unassigned speakers, there is no existing speaker added to the group instance, skip it.
                            // for update assigned existing speakers, it should remove itself in the instance and add back.
                            if (angular.isUndefined($scope.speakerVM.UnassignedSpeaker) || $scope.speakerVM.UnassignedSpeaker === null) {
                                var group = _.find(speakerGroups, function(group) {
                                    return group.Uid.toUpperCase() === speaker.SpeakerGroupUid.toUpperCase();
                                });
                                group.Speakers.splice(group.Speakers.indexOf(speaker), 1); // remove the orignal speaker instance from its group
                            }
                            for (var i = 0; i < allAreas.length; i++) {
                                if (allAreas[i].Uid === $scope.speakerVM.Area.Uid) {
                                    for (var j = 0; j < allAreas[i].Groups.length; j++) {
                                        if (allAreas[i].Groups[j].Uid === $scope.speakerVM.Group.Uid) {
                                            newSpeakerData.Color = $scope.speakerVM.Group.Color;

                                            allAreas[i].Groups[j].Speakers.push(newSpeakerData);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            $scope.close();
                        },
                        function(data) {
                            $log.error(data.data.Message);
                        });
            }
        }
    };

    //  This close function doesn't need to use jQuery or bootstrap, because
    //  the button has the 'data-dismiss' attribute.
    $scope.close = function () {
        //  Manually hide the modal.
        $element.modal('hide');
        close({
            speaker: $scope.speakerVM
        }, 500); // close, but give 500ms for bootstrap to animate
    };

    ////  This cancel function must use the bootstrap, 'modal' function because
    ////  the doesn't have the 'data-dismiss' attribute.
    //$scope.cancel = function () {
    //    //  Manually hide the modal.
    //    $element.modal('hide');

    //    //  Now call close, returning control to the caller.
    //    close({
    //        name: $scope.name,
    //        age: $scope.age
    //    }, 500); // close, but give 500ms for bootstrap to animate
    //};
});