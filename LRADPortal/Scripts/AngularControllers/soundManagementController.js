app.controller("SoundManagementController", function ($scope, $auth, $state, $log, $timeout, $http, $filter, areaGroupDataFactory, authorizationService, ModalService) {

    $scope.soundList = [];
    $scope.speakerGroups = [];
    $scope.initAll = initAll();
    $scope.isAuthorize = authorizationService.isAuthorize();
    $scope.currentPage = 1;
    $scope.pageSize = "10";
    $scope.startIndex = 1;
    $scope.lastIndex = $scope.pageSize;
    $scope.reverse = false;
    $scope.columnOrder = 'Name';

    $scope.order = function (columnOrder) {
        $scope.reverse = ($scope.columnOrder === columnOrder) ? !$scope.reverse : false;
        $scope.columnOrder = columnOrder;
    };

    $scope.pageChangeHandler = function (num) {
        updatePaginationEntries();
    };

    $scope.showAll = function () {
        $scope.q = '';
    }

    $scope.$watch('q', function (newVal, oldVal) {
        if (typeof $scope.q != 'undefined') {
            $scope.filtered = $filter('filter')($scope.soundList, $scope.q);
            var filtered = $scope.filtered.length;

            updatePaginationEntries(filtered);
        }
    });

    $scope.pageSizeOptions = [
         {
             name: '5',
             value: 5
         },
        {
            name: '10',
            value: 10
        },
        {
            name: '15',
            value: 15
        },
        {
            name: '20',
            value: 20
        }
    ];

    $scope.sliderOptions = {
        value: -6,
        options: {
            floor: -6,
            ceil: 0,
            step: 3,
            precision: 0,
            showTicks: true,
            showTicksValues: true,
            translate: function (val) {
                return val + (val === 0 ? "dB (Max)" : "dB");
            }
        }
    };

    $scope.speakerVM = {
        GeneralVolume: -6,
        EmergencyVolume: -6,
    };

    $scope.showAddSoundModal = function () {
        ModalService.showModal({
            templateUrl: "Templates/SoundModal.html",
            controller: "soundModalController"
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result) {
                    $scope.loadSounds = loadSounds();
                }
            });
        });
    };

    $scope.deleteSound = function (sound) {
        ModalService.showModal({
            templateUrl: "Templates/delete-sound-modal.html",
            controller: "DeleteSoundModal",
            inputs: {
                soundName: sound.Name
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result) {
                    areaGroupDataFactory.deleteSound(sound.Uid)
                        .then(function (data) {
                            $scope.loadSounds = loadSounds();
                        }, function (a, b, c) {
                            console.log('wrong', a, b, c);
                        });
                }

                $scope.loadSounds = loadSounds();
            });
        });
    };

    function initAll() {
        $scope.loadSounds = loadSounds();
        $scope.loadSpeakerGroups = getSpeakerGroups();
    }

    function loadSounds() {
        areaGroupDataFactory.soundManagement()
            .then(function (sounds) {
                $scope.soundList = sounds.data;
                $scope.totalItems = sounds.data.length;
            },
            function (data) {
                $log.error("Unable to get sound data.");
            });
    };

    function getSpeakerGroups() {
        areaGroupDataFactory.getAllData().then(function (data) {
            $scope.speakerGroups = data;
        }, function (a, b, c) {

        });
    }

    function updatePaginationEntries(filtered) {
        var displayCount = angular.element('.sdmgmt-item').length;
        var pageSize = parseInt($scope.pageSize);

        if (filtered && filtered > 0) {
            displayCount = filtered;
            $scope.totalItems = filtered;
        }

        if (displayCount > pageSize) {
            displayCount = pageSize;
        }

        $scope.startIndex = $scope.totalItems !== 0 ? ($scope.currentPage - 1) * parseInt($scope.pageSize) + 1 : 0;
        $scope.lastIndex = $scope.totalItems !== 0 ? $scope.startIndex + displayCount - 1 : 0;
    }

});

app.controller("SourceTreeController", function ($scope) {
    $scope.selectSourceEvent = function (treeNode, isSelected) {
        $scope.processSelectedGroups(treeNode, isSelected, $scope.selectedSourceTreeNodes, $scope.availableAreaGroups, $scope.selectedSourceGroups);
    };
});

app.controller("DestinationTreeController", function ($scope) {
    $scope.selectDestinationEvent = function (treeNode, isSelected) {
        $scope.processSelectedGroups(treeNode, isSelected, $scope.selectedDestinationTreeNodes, $scope.assignedAreaGroups, $scope.selectedDestinationGroups);

    };
});