app.controller("AreaViewController", function($scope, $auth, $state, $http, $location, $timeout, $log, uiGmapGoogleMapApi, uiGmapIsReady, mapImageOverlay, Upload, areaGroupDataFactory) {
    document.title = "Areas";

    var setupGoogleMaps = function(data) {
        $scope.areaList = [];

        data.forEach(function(item) {
            var area = {
                UserModified: item.UserModified,
                TimeModified: item.TimeModified,
                Uid: item.Uid,
                Name: item.Name,
                Alerts: item.Alerts,
                HasCustomImage: item.HasCustomImage,
                CustomImageOverlay: item.CustomImageOverlay,
                areaAddress: "",
                mapOption: "0",
                map: {
                    "center": {
                        "latitude": item.CenterLatitude,
                        "longitude": item.CenterLongitude
                    },
                    "zoom": item.Zoom,
                    "options": {
                        draggable: false,
                        zoomControl: false,
                        scrollwheel: false,
                        streetViewControl: false,
                        mapTypeControl: false,
                        mapTypeId: google.maps.MapTypeId.HYBRID,
                        disableDefaultUI: true,
                        areaId: item.Uid
                    },
                    "control": {},
                    "events": {
                        tilesloaded: function(map) {
                            $scope.$apply(function() {
                                var areaId = map.getOptions().options.areaId;
                                var areaInfo = _.find($scope.areaList, { 'Uid': areaId });
                                if (areaInfo.HasCustomImage) {
                                    var swBound = new google.maps.LatLng(areaInfo.CustomImageOverlay.LowerLeftLatitude, areaInfo.CustomImageOverlay.LowerLeftLongitude);
                                    var neBound = new google.maps.LatLng(areaInfo.CustomImageOverlay.UpperRightLatitude, areaInfo.CustomImageOverlay.UpperRightLongitude);
                                    var bounds = new google.maps.LatLngBounds(swBound, neBound);
                                    map.fitBounds(bounds);
                                    map.setZoom(map.getZoom() + 1);
                                    var overlay = mapImageOverlay.createImage(bounds, areaInfo.CustomImageOverlay.ImageFilename, map);
                                }
                            });
                        }
                    }
                }, //TODO:  set location based on users current gps location 
                marker: {
                    id: item.Uid,
                    coords: {
                        latitude: item.CenterLatitude,
                        longitude: item.CenterLongitude
                    },
                    options: { draggable: true },
                    events: {

                    }
                }
            };

            $scope.areaList.push(area);
        });
    };

    $scope.areaViewModel = {
        areaName: "",
        areaAddress: "",
        mapOption: "0",
        map: {
            "center": {
                "latitude": 52.47491894326404,
                "longitude": -1.8684210293371217
            },
            "zoom": 15,
            "control": {}
        }, //TODO:  set location based on users current gps location 
        marker: {
            id: 0,
            coords: {
                latitude: 52.47491894326404,
                longitude: -1.8684210293371217
            },
            options: { draggable: true },
            events: {
                dragend: function(marker, eventName, args) {

                    $scope.areaViewModel.marker.options = {
                        draggable: true,
                        labelContent: "lat: " + $scope.areaViewModel.marker.coords.latitude + ' ' + 'lon: ' + $scope.areaViewModel.marker.coords.longitude,
                        labelAnchor: "100 0",
                        labelClass: "marker-labels"
                    };
                }
            }
        },
        searchbox: {
            template: 'searchbox.tpl.html',
            events: {
                places_changed: function(searchBox) {
                    var place = searchBox.getPlaces();
                    if (!place || place === 'undefined' || place.length === 0) {
                        console.log('no place data :(');
                        return;
                    }

                    $scope.areaViewModel.map.center = {
                        "latitude": place[0].geometry.location.lat(),
                        "longitude": place[0].geometry.location.lng()
                    };
                    var map = $scope.areaViewModel.map.control.getGMap();
                    var sw = place[0].geometry.viewport.getSouthWest();
                    var ne = place[0].geometry.viewport.getNorthEast();
                    var newBounds = new google.maps.LatLngBounds(sw, ne);
                    map.fitBounds(newBounds);
                    $scope.areaViewModel.marker = {
                        id: 0,
                        coords: {
                            latitude: place[0].geometry.location.lat(),
                            longitude: place[0].geometry.location.lng()
                        }
                    };
                }
            }
        },
        picFile: {

        },
        areaImage: {
            imageBase64Url: '',
            overlay: null,
            sw: {
            },
            ne: {
            },
            markerOptions: { draggable: true },
            markerEvents: {
                drag: function(marker, eventName, args) {
                    var bounds = $scope.areaViewModel.areaImage.overlay.bounds_;
                    var sw = bounds.getSouthWest();
                    var ne = bounds.getNorthEast();
                    if (marker.key === 1) { //southwest
                        sw = marker.position;
                    } else { //northeast
                        ne = marker.position;
                    }
                    var newBounds = new google.maps.LatLngBounds(sw, ne);
                    $scope.areaViewModel.areaImage.overlay.updateBounds(newBounds);
                }
            }
        }
    };

    var reloadArea = function() {
        uiGmapGoogleMapApi.then(function(maps) {
            $http({
                method: 'GET',
                url: '/api/Areas/GetAreasForOverview'
            }).then(function(response) {
                setupGoogleMaps(response.data);
            });
        });
    };

    reloadArea();
    $scope.$watch('areaViewModel.picFile', function(newVal, oldVal) {
        if (angular.isDefined(newVal)) {
            if (angular.equals(newVal, oldVal)) return;
           // Upload.imageDimensions(newVal).then(function(size) {
            Upload.base64DataUrl(newVal).then(
                function(url) {
                    $scope.areaViewModel.areaImage.imageBase64Url = url;
                    var map = $scope.areaViewModel.map.control.getGMap();
                    var bounds = map.getBounds();
                    var sw = bounds.getSouthWest();
                    var ne = bounds.getNorthEast();
                    $scope.areaViewModel.areaImage.sw = {
                        latitude: sw.lat(),
                        longitude: sw.lng()
                    };
                    $scope.areaViewModel.areaImage.ne = {
                        latitude: ne.lat(),
                        longitude: ne.lng()
                    };
                    if ($scope.areaViewModel.areaImage.overlay) {
                        $scope.areaViewModel.areaImage.overlay.update(bounds, url);
                    } else
                        $scope.areaViewModel.areaImage.overlay = mapImageOverlay.createImage(bounds, url, map);
                });
            // });
        }
        });

    $scope.$watch('areaViewModel.mapOption', function(newVal, oldVal) {
        if (angular.isDefined(newVal)) {
            if (angular.equals(newVal, oldVal)) return;
            if ($scope.areaViewModel.areaImage.overlay) {
                $scope.areaViewModel.areaImage.overlay.toggle();
            }
        }
    });

    $scope.addNewArea = function() {
        $log.log($scope.areaViewModel);

        var newAreadata = {
            Name: $scope.areaViewModel.areaName,
            Latitude: $scope.areaViewModel.marker.coords.latitude,
            Longitude: $scope.areaViewModel.marker.coords.longitude,
            Zoom: $scope.areaViewModel.map.zoom
        }

        if ($scope.areaViewModel.mapOption === '1') {
            newAreadata.AreaImage = {
                LowerLeftLatitude: $scope.areaViewModel.areaImage.sw.latitude,
                LowerLeftLongitude: $scope.areaViewModel.areaImage.sw.longitude,
                UpperRightLatitude: $scope.areaViewModel.areaImage.ne.latitude,
                UpperRightLongitude: $scope.areaViewModel.areaImage.ne.longitude,
                ImageFilename: $scope.areaViewModel.areaImage.imageBase64Url
            }
        }

        areaGroupDataFactory.manageArea(newAreadata)
            .then(function(data) {
                    $('#newAreaModal').modal('hide');
                    $timeout(function() {
                        reloadArea();
                    }, 0);
                },
                function(response) {
                    alert(response.data.Message);
                });
    };

    $scope.setEditingArea = function(area) {
        $scope.editingArea = angular.copy(area);
    };

    $scope.updateArea = function() {
        areaGroupDataFactory.updateArea($scope.editingArea).then(function() {
                for (var i = 0; i < $scope.areaList.length; i++) {
                    if ($scope.areaList[i].Uid === $scope.editingArea.Uid) {
                        $scope.areaList[i].Name = $scope.editingArea.Name; // update the instance
                        break;
                    }
                }
            },
            function(data) {

            });
    };

    $scope.deleteArea = function() {
        areaGroupDataFactory.deleteArea($scope.editingArea.Uid).then(function () {
            _.remove($scope.areaList, $scope.editingArea);
            },
            function(data) {

            });
    }

    $scope.$watch('editingArea.Name', function(newVal, oldVal) {
        if (angular.isDefined(newVal)) {
            if (angular.equals(newVal, oldVal)) return;
            areaGroupDataFactory.isAreaNameExisting({ id: $scope.editingArea.Uid, name: $scope.editingArea.Name }).then(function(isExisting) {
                    $scope.editAreaError = isExisting ? "This name is in use, please choose a different name." : "";
                },
                function(data) {

                });
        }
    }, true);


    $('#newAreaModal').on('shown.bs.modal', function(e) {
        $timeout(function() {
            $('.selectpicker').selectpicker();
        }, 0);
        $scope.$apply(function() {
            $scope.render = true;
        });
    });

    $('#newAreaModal').on('hidden.bs.modal', function(e) {
        $scope.areaViewModel.areaName = "";
        $scope.areaViewModel.areaAddress = "";
    });

});