﻿app.controller("speakerGroupModalController", function ($scope, $log, $element, speakerGroup, allAreas, allGroups, allGeneralSounds, allEmergencySounds, speakerGroups, selectedArea, close, speakerService, areaGroupDataFactory, $timeout) {
    $scope.groupFocus = true;
    $scope.isError = false;

    $scope.allAreas = allAreas;
    $scope.allGroups = allGroups;

    $scope.availableGeneralFiles = [];
    $scope.assignedGeneralFiles = [];
    $scope.availableEmergencyFiles = [];
    $scope.assignedEmergencyFiles = [];
    $scope.selectedSourceGeneralFiles = [];
    $scope.selectedDestinationGeneralFiles = [];
    $scope.selectedSourceEmergencyFiles = [];
    $scope.selectedDestinationEmergencyFiles = [];
    $scope.treeOptions = {
        multiSelection: true
    };

    var initializeGroupViewModel = function () {
        $scope.newGroupViewModel = {
            Area: {},
            AreaUid: "",
            Name: "",
            MultiCastGroup: "",
            Color: "#ffffff",
            GeneralFilesList: [],
            EmergencyFilesList: []
        };
        if (selectedArea) {
            $scope.newGroupViewModel.Area = _.find(allAreas, function (a) {
                return a.Uid === selectedArea.Uid;
            });
        }
        else {
            $scope.newGroupViewModel.Area = allAreas[0];
        }
    };
    $scope.isAddMode = speakerGroup == null;

    if ($scope.isAddMode) { // for adding
       // initializeTree();
        initializeGroupViewModel();
        var allGeneralFiles = _.sortBy(_.filter(allGeneralSounds, function (sound) { return sound.Type === 0; }), 'Name');
        var allEmergencyFiles = _.sortBy(_.filter(allEmergencySounds, function (sound) { return sound.Type === 1; }), 'Name');
        angular.copy(allGeneralFiles, $scope.availableGeneralFiles);
        angular.copy(allEmergencyFiles, $scope.availableEmergencyFiles);
        angular.copy([], $scope.assignedGeneralFiles);
        angular.copy([], $scope.assignedEmergencyFiles);
    } else { // for editing
        $scope.newGroupViewModel = angular.copy(speakerGroup);
        $scope.newGroupViewModel.Area = _.find(allAreas, function (a) {
            return a.Uid === speakerGroup.AreaUid;
        });
        var assignedGeneralSounds = _.sortBy(_.filter(speakerGroup.Sounds, function (sound) { return sound.Type === 0; }), 'Name');
        var assignedEmergencySounds = _.sortBy(_.filter(speakerGroup.Sounds, function (sound) { return sound.Type === 1; }), 'Name');

        var availableGeneralSounds = [],
            availableEmergencySounds = [];

        angular.forEach(allGeneralSounds, function (sound) {
            var contains = 0;
 
            angular.forEach(assignedGeneralSounds, function (assigned) {
                if (assigned.Uid === sound.Uid)
                    contains++;
            });
            if (contains === 0)
                availableGeneralSounds.push(sound);
        });

        angular.forEach(allEmergencySounds, function (sound) {
            var contains = 0;

            angular.forEach(assignedEmergencySounds, function (assigned) {
                if (assigned.Uid === sound.Uid)
                    contains++;
            });
            if (contains === 0)
                availableEmergencySounds.push(sound);
        });

        angular.copy(assignedGeneralSounds, $scope.assignedGeneralFiles);
        angular.copy(assignedEmergencySounds, $scope.assignedEmergencyFiles);
        angular.copy(availableGeneralSounds, $scope.availableGeneralFiles);
        angular.copy(availableEmergencySounds, $scope.availableEmergencyFiles);
    }

    var updateSelecteions = function (selectedSourceFiles, assignedFiles, availableFiles) {

        if (selectedSourceFiles.length === 0) {
            return;
        }

        var addedFiles = _.sortBy(_.union(assignedFiles, selectedSourceFiles), 'Name');

        angular.copy(addedFiles, assignedFiles);

        var removedFiles = _.difference(availableFiles, selectedSourceFiles);

        angular.copy(removedFiles, availableFiles);

        angular.copy([], selectedSourceFiles); //angular.copy operates by reference
    };

    $scope.addGeneralFiles = function () {
        updateSelecteions($scope.selectedSourceGeneralFiles, $scope.assignedGeneralFiles, $scope.availableGeneralFiles);
    };

    $scope.removeGeneralFiles = function () {
        updateSelecteions($scope.selectedDestinationGeneralFiles, $scope.availableGeneralFiles, $scope.assignedGeneralFiles);
    };

    $scope.addEmergencyFiles = function () {
        updateSelecteions($scope.selectedSourceEmergencyFiles, $scope.assignedEmergencyFiles, $scope.availableEmergencyFiles);
    };

    $scope.removeEmergencyFiles = function () {
        updateSelecteions($scope.selectedDestinationEmergencyFiles, $scope.availableEmergencyFiles, $scope.assignedEmergencyFiles);
    };

    $scope.manageGroup = function () {
        //var assignedGenFiles = $scope.assignedGeneralFiles;

        //if (assignedGenFiles.length < 1) {
        //    $scope.isError = true;
        //    return;
        //}

        if (!$scope.newGroupForm.$invalid) {
            $scope.areaUid = $scope.newGroupViewModel.Area.Uid;
            var newGroupdata = {
                Playing: "",
                AreaUid: $scope.newGroupViewModel.Area.Uid,
                Name: $scope.newGroupViewModel.Name,
                MultiCastGroup: $scope.newGroupViewModel.MultiCastGroup,
                Color: $scope.newGroupViewModel.Color,
                GeneralFilesList: $scope.assignedGeneralFiles,
                EmergencyFilesList: $scope.assignedEmergencyFiles,
                Speakers: $scope.isAddMode ? [] : speakerGroup.Speakers,
                Sounds: $scope.isAddMode ? [] : speakerGroup.Sounds
            };
            if ($scope.isAddMode) { // for adding group
                areaGroupDataFactory.manageGroup(newGroupdata)
                    .then(function(newGroupUid) {
                            newGroupdata.Uid = newGroupUid;
                            newGroupdata.Sounds = _.union($scope.assignedGeneralFiles, $scope.assignedEmergencyFiles);

                            var area = _.find(allAreas, function(area) {
                                return area.Uid.toUpperCase() === newGroupdata.AreaUid.toUpperCase();
                            });
                            area.Groups.push(newGroupdata);
                            allGroups.push(newGroupdata);

                            $scope.close(newGroupdata);
                        },
                        function(data) {
                            $log.error(data.Message);
                        });
            } else { // for editing group
                newGroupdata.Uid = speakerGroup.Uid;

                angular.forEach(speakerGroup.Speakers, function (val, key) {
                    val.Color = $scope.newGroupViewModel.Color;
                });

                newGroupdata.Speakers = speakerGroup.Speakers;
                newGroupdata.Sounds = _.union($scope.assignedGeneralFiles, $scope.assignedEmergencyFiles);
                areaGroupDataFactory.updateSpeakerGroup(newGroupdata)
                    .then(function() {
                            var area = _.find(allAreas, function(area) {
                                return area.Uid.toUpperCase() === newGroupdata.AreaUid.toUpperCase();
                            });
                            if (area.Groups.indexOf(speakerGroup) > -1)
                                area.Groups.splice(area.Groups.indexOf(speakerGroup), 1); // remove the orignal group instance from its group
                            if (speakerGroups.indexOf(speakerGroup) > -1)
                                speakerGroups.splice(speakerGroups.indexOf(speakerGroup), 1);
                            if (allGroups.indexOf(speakerGroup) > -1)
                                allGroups.splice(allGroups.indexOf(speakerGroup), 1); // remove the orignal group instance from all group
                            area = _.find(allAreas, function(area) {
                                return area.Uid.toUpperCase() === newGroupdata.AreaUid.toUpperCase();
                            });
                            area.Groups.push(newGroupdata);
                            allGroups.push(newGroupdata);
                            $scope.close(newGroupdata);
                        },
                        function(data) {
                            $log.error(data.Message);
                        });
            }
        }
    };

    //  This close function doesn't need to use jQuery or bootstrap, because
    //  the button has the 'data-dismiss' attribute.
    $scope.close = function (data) {
        //  Manually hide the modal.
        $element.modal('hide');
        close({
            speakerGroup: data
        }, 500); // close, but give 500ms for bootstrap to animate
    };
});