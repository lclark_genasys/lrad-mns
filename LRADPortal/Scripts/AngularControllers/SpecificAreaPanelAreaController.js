﻿app.controller("SpecificAreaPanelAreaController", function ($scope, $auth, $state, $stateParams, $http, $rootScope, $filter, $log, areaGroupDataFactory) {
    $scope.onCheckAllGroups = function () {
        var selectedSpeakers = [];

        $scope.$parent.selectedSpeaker = null;

        angular.forEach($scope.selectedSpeakerGroups, function (group) {
            group.IsSelected = $scope.$parent.isCheckAll;
            angular.forEach(group.Speakers, function(speaker) {
                speaker.IsSelected = $scope.$parent.isCheckAll;

                if (speaker.IsSelected) {
                    selectedSpeakers.push(speaker.IsSelected);
                }
            });
        });
        
        $scope.$parent.speaker.IsSelected = selectedSpeakers;
    };

    $scope.onGroupSelected = function (group) {
        var selectedSpeakers = [];

        $scope.$parent.selectedSpeaker = null;

        angular.forEach(group.Speakers, function (speaker) {
            speaker.IsSelected = group.IsSelected; // loop all children speakers set speakers to groups selected

            if (speaker.IsSelected) {
                selectedSpeakers.push(speaker.IsSelected);
            }
        });

        $scope.$parent.speaker.IsSelected = selectedSpeakers;

        if ($scope.selectedGroup !== undefined && $scope.selectedGroup !== null && $scope.selectedGroup.Uid === group.Uid) // if there is a group selected
            $scope.$parent.isCheckAll = group.IsSelected;
        else if ($scope.selectedGroup == null) { // if all groups selected
            if (!group.IsSelected) // if uncheck a group
                $scope.$parent.isCheckAll = false; // set check all to false of cause
            else { // if checked a group
                $scope.$parent.isCheckAll = $scope.selectedSpeakerGroups.every(function(gp) { return gp.IsSelected; }); // loop all groups see if all groups are checked or not.
            }
        }
    };

    $scope.onSpeakerToggled = function (group) {
        if (this.speaker.IsSelected) {
            $scope.$parent.selectedSpeaker = this.speaker;
        } else {
            $scope.$parent.selectedSpeaker = null;
        }

        $scope.$parent.speaker.IsSelected = [this.speaker.IsSelected];
        group.IsSelected = group.Speakers.every(function (sp) { return sp.IsSelected; });

        if (group.IsSelected)
            $scope.$parent.isCheckAll = $scope.selectedSpeakerGroups.every(function (gp) { return gp.IsSelected; }); // loop all groups see if all groups are checked or not.
        else
            $scope.$parent.isCheckAll = false;
    };

});