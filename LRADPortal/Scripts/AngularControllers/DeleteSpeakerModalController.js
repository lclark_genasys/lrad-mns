﻿app.controller('DeleteSpeakerModalController', function ($scope, $element, speaker, close) {

    $scope.$parent.speakerToModify = speaker;

    $scope.close = function (result) {
        close(result, 500);
    };

});