﻿app.factory('tabFactory', function ($rootScope) {
    return {
        broadcast: function (activateTab) {
            $rootScope.$broadcast('handleUpdateToTab', activateTab);
        }
    };
});