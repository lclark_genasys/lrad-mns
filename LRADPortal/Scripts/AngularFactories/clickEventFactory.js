﻿app.factory('clickEventFactory', function ($rootScope) {
    return {
        myEvent: function (event, stringToTest) {
            if (event && stringToTest) {
                var string = event.currentTarget.className;
                var reg = new RegExp(stringToTest);
                var testString = reg.test(string);

                return testString;
            }

            return false;
        }
    };
});