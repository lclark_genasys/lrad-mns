﻿var app = angular.module('LRADPortal', [
    'ui.router', 'satellizer', 'ngSanitize', 'angular-loading-bar', 'uiGmapgoogle-maps', 'nemLogging', 'treeControl', 'mgcrea.ngStrap', 'angular-bootstrap-select',
    'rzModule', 'angularModalService', 'alert-modal', 'angularUtils.directives.dirPagination', 'ngWebSocket', 'ngFileUpload'
]);
app.config(function ($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider, $locationProvider, $authProvider, $httpProvider, uiGmapGoogleMapApiProvider, recorderServiceProvider) {
    $locationProvider.html5Mode(true);
    $urlMatcherFactoryProvider.caseInsensitive(true);
    var authenticated = function ($q, $location, $auth) {
        var deferred = $q.defer();
        if (!$auth.isAuthenticated()) {
            $location.path('/login');
        } else {
            deferred.resolve();
        }
        return deferred.promise;
    };

    $urlRouterProvider.otherwise('/login');
    $stateProvider
        .state('areaview', {
            url: '/areaview',
            templateUrl: 'Templates/AreaView.html',
            controller: 'AreaViewController',
            resolve: {
                authenticated: authenticated
            }
        })
        .state('specificarea', {
            url: '/specificarea',
            abstract: true,
            templateUrl: 'Templates/SpecificArea.html',
            controller: 'SpecificAreaController',
            resolve: {
                authenticated: authenticated
            }
        })
        .state('specificarea.map', {
            url: '/map/?areaId&groupId',
            parent:'specificarea',
            templateUrl: 'Templates/SpecificAreaMap.html',
            controller: 'SpecificAreaMapController'
        })
        .state('specificarea.table', {
            url: '/table/?areaId&groupId',
            parent: 'specificarea',
            templateUrl: 'Templates/SpecificAreaTable.html',
            controller: 'SpecificAreaTableController'
        })
        .state('userManagement', {
            url: '/userManagement',
            templateUrl: 'Templates/UserManagement.html',
            controller: 'UserManagementController'
        })
        .state('soundManagement', {
            url: '/soundManagement',
            templateUrl: 'Templates/SoundManagement.html',
            controller: 'SoundManagementController'
        })
        .state('login', {
            url: '/login',
            templateUrl: 'Templates/Login.html',
            controller: 'LoginController',
            resolve: {
                authenticated: function ($q, $auth, $state, $timeout) {
                    if (!$auth.isAuthenticated()) {
                        return $q.when();
                    } else {
                        $timeout(function () {
                            $state.go("areaview");
                        });
                        return $q.reject();
                    }
                }
            }
        })
        .state('logout', {
           // url: '/logout',
            template: null,
            controller: 'LogoutController'
        });

    //Http Intercpetor to check auth failures for xhr requests
    $httpProvider.interceptors.push('authHttpResponseInterceptor');
    // Authorization configuration setup
    $authProvider.loginOnSignup = true;
    $authProvider.loginRedirect = undefined;
    $authProvider.logoutRedirect = '/login';
    //$authProvider.signupRedirect = '/Enrollment/Score';
    $authProvider.loginUrl = '/Token';
    $authProvider.signupUrl = '/api/Account/Register';
    $authProvider.loginRoute = '/login';
    $authProvider.signupRoute = '/register';
    $authProvider.tokenRoot = false; // set the token parent element if the token is not the JSON root
    $authProvider.tokenName = 'token';
    $authProvider.tokenPrefix = 'satellizer'; // Local Storage name prefix
    $authProvider.platform = 'browser'; // or 'mobile'
    var storageMode = localStorage.getItem('storageMode');
    $authProvider.storageType = storageMode != null ? storageMode : 'sessionStorage'; // or 'localStorage'

    // google map configuration
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyDNCwYbOr6sYM9_nr5TimWhs-sbkpm2WCg',
        v: '3.28',
        libraries: 'places' // Required for SearchBox.
    });

    recorderServiceProvider
        .forceSwf(false)
        .withMp3Conversion(true);
}).run([
    '$rootScope', '$location', '$state', '$stateParams', function ($rootScope, $location, $state, $stateParams) {
        // It's very handy to add references to $state and $stateParams to the $rootScope
        // so that you can access them from any scope within your applications.For example,
        // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
        // to active whenever 'contacts.list' or one of its decendents is active.
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        var path = function () { return $location.path(); };
        $rootScope.$watch(path, function (newVal, oldVal) {
            $rootScope.activetab = newVal;
        });
        //app util functions
        $rootScope.UTIL = {
            getformattedDate: function (date) {
                if (date == null)
                    return date;
                var fdate = new Date(date);
                fdate = fdate.getUTCMonth() + 1 + "/" + fdate.getUTCDate() + "/" + fdate.getUTCFullYear();
                return fdate;
            },
            getformattedDateTime: function (date) {
                if (date == null)
                    return date;
                var fdate = new Date(date);
                fdate = fdate.getUTCMonth() + 1 + "/" + fdate.getUTCDate() + "/" + fdate.getUTCFullYear() + " " + fdate.getUTCHours() + ":" + fdate.getUTCMinutes() + ":" + fdate.getUTCSeconds();
                return fdate;
            },
            addComma: function (nStr) {
                nStr += '';
                var x = nStr.split('.');
                var x1 = x[0];
                var x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
            },
            currency: function (nStr) {
                return "$" + $rootScope.UTIL.decimal(nStr);
            },
            decimal: function (nStr) {
                return $rootScope.UTIL.addComma(parseFloat(nStr).toFixed(2));
            },
            percentage: function (nStr) {
                return $rootScope.UTIL.decimal(parseFloat(nStr) * 100) + '%';
            }
        };

        // TODO: Since we don't need db, so don't need to do anything here.
        //$rootScope.$on("LRAD.DevicePropertyChanged", function (evt, data) {
        //    console.log('DevicePropertyChanged: ', data);

        //    // todo: AudioOn = true means the audio is playing
        //});

        //$rootScope.$on("LRAD.AlertRaised", function (evt, data) {
        //    console.log('AlertRaised: ', data);
        //});
    }
]).directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                scope.$evalAsync(attr.onFinishRender);
            }
        }
    }
}).directive('currency', function ($rootScope) {
    return {
        require: 'ngModel',
        link: function (elem, $scope, attrs, ngModel) {
            ngModel.$formatters.push(function (val) {
                return '$' + $rootScope.UTIL.decimal(val);
            });
            ngModel.$parsers.push(function (val) {
                return val.replace(/^\$/, '');
            });
        }
    }
}).directive('datetime', function ($rootScope) {
    return {
        require: 'ngModel',
        link: function (elem, $scope, attrs, ngModel) {
            ngModel.$formatters.push(function (val) {
                return $rootScope.UTIL.getformattedDate(val);
            });
        }
    }
}).directive('visible', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            scope.$watch(attributes.visible, function (value) {
                element.css('visibility', value ? 'visible' : 'hidden');
            });
        }
    };
}).factory('authHttpResponseInterceptor', [
    '$q', '$timeout', '$injector', function ($q, $timeout, $injector) {
        var $state;
        var $auth;
        //// this trick must be done so that we don't receive
        //// `Uncaught Error: [$injector:cdep] Circular dependency found`
        $timeout(function () {
            $state = $injector.get('$state');
            $auth = $injector.get('$auth');
            $rootScope = $injector.get('$rootScope');
        });
        return {
            response: function (response) {
                if (response.status === 401) {
                    console.log("Response 401");
                }
                return response || $q.when(response);
            },
            responseError: function (rejection) {
                if (rejection.status === 401) {
                    console.log("Response Error 401", rejection);
                    $auth.logout()
                        .then(function() {
                            $rootScope.$broadcast("userLogout");
                        });
                }
                return $q.reject(rejection);
            }
        }
    }
]);

app.constant('recorderScriptUrl', (function() {
        var scripts = document.getElementsByTagName('script');
        var myUrl = scripts[scripts.length - 1].getAttribute('src');
        var path = myUrl.substr(0, myUrl.lastIndexOf('/') + 1);
        var a = document.createElement('a');
        a.href = path;
        return a.href;
    }))
    .constant('recorderPlaybackStatus', {
        STOPPED: 0,
        PLAYING: 1,
        PAUSED: 2
    })
    .constant('speakerLimiter', 500)
    //.constant('LRADUpdateEndPoint', 'ws://192.168.100.222:7998/')
    //.constant('LRADUpdateEndPoint', 'ws://localhost:8066/')
    .constant('LRADUpdateEndPoint', 'ws://8.25.38.141:7998/')
    .constant('lRADDeviceBaseUrl', 'http://8.25.38.141:7999/lrad/v1/');


