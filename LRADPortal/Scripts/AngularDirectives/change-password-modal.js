﻿angular
    .module('LRADPortal')
    .directive('changePassword', changePassword);

function changePassword() {
    var directive = {
        restrict: 'EA',
        templateUrl: 'Templates/ChangePassword.html',
        controller: changePasswordController,
    };
    return directive;
}

changePasswordController.$inject = ['$scope', '$log','$http'];

function changePasswordController($scope, $log, $http) {
    // Injecting $scope just for comparison
    $scope.changePasswordViewModel = [];
    $scope.isNewPasswordRequired = false;
    $scope.isConfirmNewPasswordRequired = false;
    $scope.validationMessage = "";
    var initializePasswordViewModel = function () {
        $scope.changePasswordViewModel = {
            OldPassword: "",
            NewPassword: "",
            ConfirmPassword: ""
        };
    };
    $scope.pwdFocus = false;
    $('#changePasswordModal').on('shown.bs.modal', function () {
        $scope.changePasswordForm.$setPristine();
        $scope.isNewPasswordRequired = false;
        $scope.isConfirmNewPasswordRequired = false;
        initializePasswordViewModel();
        $scope.pwdFocus = true;
        $scope.validationMessage = "";
        $scope.$apply(function () {
            $scope.render = true;
        });

    });

    $scope.changePassword = function () {
        validateNewPassword();
        confirmNewPassword();

        if ($scope.changePasswordForm.$invalid || $scope.isNewPasswordRequired || $scope.validationMessage || $scope.isConfirmNewPasswordRequired) {
            return;
        } else {

            $http.post("api/account/ChangeUserPassword", $scope.changePasswordViewModel)
                .then(function (data) {
                    $('#changePasswordModal').modal('hide');
                },
                function (data) {
                    $log.error(data.Message);
                });
        }

    };

    var updateValidatePasswordMessage = function () {
        $scope.validationMessage = "";
        if ($scope.changePasswordViewModel.NewPassword.length < 6) {
            $scope.validationMessage = "Password must be at least 6 characters.";
        }

        if ($scope.changePasswordViewModel.NewPassword.search(/[a-z]/) < 0) {
            $scope.validationMessage = "Password must contain at least one lowercase.";
        }

        if ($scope.changePasswordViewModel.NewPassword.search(/[A-Z]/) < 0) {
            $scope.validationMessage = "Password must contain at least one uppercase.";
        };

        if (/^[a-zA-Z0-9- ]*$/.test($scope.changePasswordViewModel.NewPassword) === true) {
            $scope.validationMessage = "Password must contain at least one non letter or digit character.";
        }
    };

    var validateNewPassword = function () {
        $scope.isNewPasswordRequired = !$scope.changePasswordViewModel.NewPassword;
        updateValidatePasswordMessage();
    };

    var confirmNewPassword = function () {
        $scope.isConfirmNewPasswordRequired = $scope.changePasswordViewModel.NewPassword !== $scope.changePasswordViewModel.ConfirmPassword;
    };

    $scope.newPasswordChanged = function () {
        validateNewPassword();
        confirmNewPassword();
    };

    $scope.confirmPasswordChanged = function () {
        confirmNewPassword();
    };
}