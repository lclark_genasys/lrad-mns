﻿app.directive('fileUploadDirective', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        scope: {},
        require: '^ngModel',
        link: function (scope, element, attrs) {

            element.bind('change', function () {
                scope.$apply(function () {
                    var file = element[0].files[0];

                    scope.$parent.newSound.soundFile = file.name;
                    scope.$parent.newSound.soundName = file.name;
                    scope.$parent.newSound.soundDesc = "This is a description about, " + file.name;
                });
            });

        }
    };
}]);