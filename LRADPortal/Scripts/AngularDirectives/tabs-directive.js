﻿app.directive('customTabs', function () {
    return {
        restrict: 'E',
        templateUrl: 'Templates/tabs-tmpl.html',
        transclude: true,
        link: function (scope, element, attrs) {
        }
    };
});

app.directive('customPanes', function () {
    return {
        require: ['?^customTabs'],
        restrict: 'E',
        templateUrl: 'Templates/tabs-pane-tmpl.html',
        transclude: true,
        link: function (scope, element, attrs, ctrl) {
        }
    };
});

app.directive('tabSelection', function () {
    return {
        controller: function ($scope) {
            $scope.tab = 1;

            $scope.selectTab = function (setTab) {
                $scope.tab = setTab;
            };

            $scope.isSelected = function (checkTab) {
                return $scope.tab === checkTab;
            };

            $scope.$on('handleUpdateToTab', function (event, message) {
                $scope.tab = message;
            });
        }
    };
});

//app.directive('populatePanes', function (areaGroupDataFactory) {
//    return {
//        restrict: 'A',
//        controller: function ($scope) {
//        },
//        link: function (scope) {
//        }
//    };
//});