﻿'use strict';

var directiveModule = angular.module('alert-modal', ['ngSanitize']);

directiveModule.directive('alertModal', function ($http, $timeout) {
    return {
        restrict: 'AE',
        replace: true,
        scope: {
            events: '=',
            displayTexts: '=',
            bindModel: '=ngModel',
            settings: '='
        },
        template: function (element, attrs) {
            var template = '<div id="{{_settings.id}}" class="modal" tabindex="-1" role="dialog" aria-labelledby="{{_settings.id}}" aria-hidden="true" data-keyboard="{{_settings.keyboard}}" data-backdrop="{{_settings.backdrop}}">';
            template += '<div class="modal-dialog">';
            template += '<div class="modal-content">';
            template += '<form class="form-horizontal container-fluid" novalidate>';
            template += '<div class="modal-header">';
            template += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="ico-close" data-ng-click="closeClick()"><\/i><\/button>';
            template += '<h1 class="modal-title">{{texts.title}}<\/h1>';
            template += '<\/div>';
            template += '<div class="modal-body" ng-bind-html="bindModel">';
            template += '<\/div>';
            template += '<div class="modal-footer">';
            template += '<button type="button" class="btn btn-default" data-dismiss="modal" data-ng-click="cancelClick()">{{texts.cancelBtnText}}<\/button>';
            template += '<button type="button" class="btn btn-primary" data-dismiss="modal" data-ng-click="okayClick()">{{texts.okBtnText}}<\/button>';
            template += '<\/div>';
            template += '<\/form>';
            template += '<\/div>';
            template += '<\/div>';
            template += '<\/div>';
            return template;
        },
        link: function ($scope, element, attrs) {
            $scope.externalEvents = {
                onShowing: angular.noop,
                onShown: angular.noop,
                onHidden: angular.noop,
                onClosed: angular.noop,
                onOkayClick: angular.noop,
                onCancelClick: angular.noop
            };
            $scope.texts = {
                title: 'Alert',
                okBtnText: 'Ok',
                cancelBtnText: 'Cancel'
            };

            $scope._settings = {
                isCancelHidden: false,
                backdrop: true, // specify 'static' for a backdrop which doesn't close the modal on click
                keyboard: true, // Closes the modal when escape key is pressed
                isHTML: false,
                id: "alertModal"
            };

            angular.extend($scope._settings, $scope.settings || []);
            angular.extend($scope.externalEvents, $scope.events || []);
            angular.extend($scope.texts, $scope.displayTexts);

            $scope.okayClick = function () {
                $scope.externalEvents.onOkayClick();
            };
            $scope.cancelClick = function () {
                $scope.externalEvents.onCancelClick();
            };
            $scope.closeClick = function () {
                $scope.externalEvents.onClosed();
            };

            $timeout(function() {
                element.on('shown.bs.modal', function (e) {
                    $scope.externalEvents.onShown(e);
                });
                element.on('show.bs.modal', function (e) {
                    $scope.externalEvents.onShowing(e);
                });
                element.on('hidden.bs.modal', function (e) {
                    $scope.externalEvents.onHidden(e);
                });
            });
        }
    };
});