//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LRADPortal
{
    using System;
    using System.Collections.Generic;
    
    public partial class SpeakerType
    {
        public SpeakerType()
        {
            this.Speakers = new HashSet<Speaker>();
        }
    
        public byte Id { get; set; }
        public string Name { get; set; }
        public int MaxCoverageRadius { get; set; }
    
        public virtual ICollection<Speaker> Speakers { get; set; }
    }
}
