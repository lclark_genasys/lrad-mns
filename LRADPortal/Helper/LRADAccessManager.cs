﻿using LRADPortal.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace LRADPortal.Helper
{
    public class LradAccessManager
    {
        private static LRADMNSEntities _db = new LRADMNSEntities();
        private static readonly string Host = WebConfigurationManager.AppSettings["API_IP"];
        private static readonly string Port = WebConfigurationManager.AppSettings["API_PORT"];
        private static readonly string Version = WebConfigurationManager.AppSettings["API_Version"];
        private static readonly Uri BaseAddress = new Uri(string.Format("http://{0}:{1}", Host, Port));

        public static async Task<JArray> GetAllSpeakers()
        {
            using (var httpClient = new HttpClient { BaseAddress = BaseAddress })
            {
                using (var response = await httpClient.GetAsync(Version + "/devices"))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string responseData = await response.Content.ReadAsStringAsync();
                        return JArray.Parse(responseData);
                    }
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<JArray> GetSpeakersById(Guid id)
        {
            using (var httpClient = new HttpClient { BaseAddress = BaseAddress })
            {
                using (var response = await httpClient.GetAsync(Version + "/devices?Uid=" + id))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string responseData = await response.Content.ReadAsStringAsync();
                        return JArray.Parse(responseData);
                    }
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<string> GetDiscoveredSpeakers()
        {
            using (var httpClient = new HttpClient {BaseAddress = BaseAddress})
            {
                using (var content = new StringContent(""))
                {
                    using (var response = await httpClient.PostAsync(Version + "/discovery", content))
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            string responseData = await response.Content.ReadAsStringAsync();
                            return responseData;
                        }
                        throw new Exception(response.ReasonPhrase);
                    }
                }
            }
        }

        public static List<ApiSpeaker> SpeakerExists(List<ApiSpeaker> speakers)
        {
            return (speakers.Select(
                speaker => new {speaker, isIt = _db.Speakers.Count(e => e.Uid.ToString() == speaker.Uid) > 0})
                .Where(w => !w.isIt)
                .Select(s => s.speaker)).ToList();
        }

        public static JArray ConvertDataToJArray(string data)
        {
            var jsonData = JsonConvert.DeserializeObject(data);
            JArray resources = (JArray)jsonData;

            return resources;
        }

        public static async Task<string> CreateLrad(Speaker speaker)
        {
                using (var httpClient = new HttpClient { BaseAddress = BaseAddress })
                {
                    using (var response = await httpClient.PostAsJsonAsync(Version + "/devices", new ApiSpeaker(speaker)))
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            string responseData = await response.Content.ReadAsStringAsync();
                            return responseData;
                        }
                        throw new Exception(response.ReasonPhrase);
                    }
                }
        }

        public static async Task<string> UpdatedLrad(Speaker speaker)
        {
            using (var httpClient = new HttpClient {BaseAddress = BaseAddress})
            {
                using (var response = await httpClient.PutAsJsonAsync(Version + "/devices/", new ApiSpeaker(speaker)))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string responseData = await response.Content.ReadAsStringAsync();
                        return responseData;
                    }
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<string> DeleteLrad(Guid id)
        {
            using (var httpClient = new HttpClient {BaseAddress = BaseAddress})
            {
                using (var response = await httpClient.DeleteAsync(Version + "/devices/" + id))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string responseData = await response.Content.ReadAsStringAsync();
                        return responseData;
                    }
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<string> Play(List<PlayDevice> data)
        {
            using (var httpClient = new HttpClient {BaseAddress = BaseAddress})
            {
                using (var response = await httpClient.PostAsJsonAsync(Version + "/devices/audio/remote/play", data))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string responseData = await response.Content.ReadAsStringAsync();
                        return responseData;
                    }
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<string> Stop(List<PlayDevice> data)
        {
            using (var httpClient = new HttpClient {BaseAddress = BaseAddress})
            {

                using (var response = await httpClient.PostAsJsonAsync(Version + "/devices/audio/stop", data))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string responseData = await response.Content.ReadAsStringAsync();
                        return responseData;
                    }
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }
}