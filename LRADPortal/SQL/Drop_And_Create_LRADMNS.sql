USE [master]
GO

IF EXISTS (SELECT name FROM sys.databases WHERE name = N'LRADMNS')
BEGIN
ALTER DATABASE [LRADMNS] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
DROP DATABASE [LRADMNS] ;
END
GO

/****** Object:  Database [LRADMNS]    Script Date: 4/21/2015 7:13:43 PM ******/
CREATE DATABASE [LRADMNS]
GO

ALTER DATABASE [LRADMNS] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LRADMNS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LRADMNS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LRADMNS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LRADMNS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LRADMNS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LRADMNS] SET ARITHABORT OFF 
GO
ALTER DATABASE [LRADMNS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LRADMNS] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [LRADMNS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LRADMNS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LRADMNS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LRADMNS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LRADMNS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LRADMNS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LRADMNS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LRADMNS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LRADMNS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LRADMNS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LRADMNS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LRADMNS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LRADMNS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LRADMNS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LRADMNS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LRADMNS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LRADMNS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LRADMNS] SET  MULTI_USER 
GO
ALTER DATABASE [LRADMNS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LRADMNS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LRADMNS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LRADMNS] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

USE [LRADMNS]
GO
/****** Object:  Table [dbo].[Area]    Script Date: 4/21/2015 7:13:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Area](
	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[ImageFilename] [nchar](256) NULL,
	[UpperLeftLatitude] [float] NULL,
	[UpperLeftLongitude] [float] NULL,
	[LowerRightLatitude] [float] NULL,
	[LowerRightLongitude] [float] NULL,
	[TimeCreated] [datetime] NOT NULL,
	[UserCreated] [nchar](50) NULL,
	[TimeModified] [datetime] NOT NULL,
	[UserModified] [nchar](50) NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SpeakerState]    Script Date: 4/21/2015 7:13:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpeakerState](
	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[SpeakerUid] [uniqueidentifier] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[AmpTemp] [float] NULL,
	[BatteryLevel] [float] NULL,
	[Door] [smallint] NULL,
	[Connection] [smallint] NULL,
	[Activity] [smallint] NULL,
	[Status] [smallint] NULL,
	[SatelliteStatus] [smallint] NULL,
 CONSTRAINT [PK_SpeakerState] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sound]    Script Date: 4/21/2015 7:13:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sound](
	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[Name] [nchar](50) NOT NULL,
	[Type] [smallint] NULL,
	[Description] [nchar](256) NOT NULL,
	[Filename] [nchar](256) NULL,
	[Text] [ntext] NULL,
	[TimeCreated] [datetime] NOT NULL,
	[UserCreated] [nchar](50) NULL,
	[TimeModified] [datetime] NOT NULL,
	[UserModified] [nchar](50) NULL,
 CONSTRAINT [PK_Sound] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SoundSpeakerGroup]    Script Date: 4/21/2015 7:13:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoundSpeakerGroup](
	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[SoundUid] [uniqueidentifier] NULL,
	[SpeakerUid] [uniqueidentifier] NULL,
	[SpeakerGroupUid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_SoundSpeakerGroup] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Speaker]    Script Date: 4/21/2015 7:13:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Speaker](
	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[SpeakerGroupUid] [uniqueidentifier] NULL,
	[IPAddress] [nchar](50) NULL,
	[SatelliteID] [nchar](50) NULL,
	[Type] [nchar](50) NULL,
	[GeneralVolume] [float] NULL,
	[EmergencyVolume] [float] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[TimeCreated] [datetime] NOT NULL,
	[UserCreated] [nchar](50) NULL,
	[TimeModified] [datetime] NOT NULL,
	[UserModified] [nchar](50) NULL,
 CONSTRAINT [PK_Speaker] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SpeakerGroup]    Script Date: 4/21/2015 7:13:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpeakerGroup](
	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[AreaUid] [uniqueidentifier] NULL,
	[MultiCastGroup] [nchar](50) NULL,
	[Color] [int] NULL,
	[TimeCreated] [datetime] NOT NULL,
	[UserCreated] [nchar](50) NULL,
	[TimeModified] [datetime] NOT NULL,
	[UserModified] [nchar](50) NULL,
 CONSTRAINT [PK_SpeakerGroup] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 4/21/2015 7:13:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[FirstName] [nchar](50) NOT NULL,
	[LastName] [nchar](50) NOT NULL,
	[Email] [nchar](256) NULL,
	[UserName] [nchar](50) NOT NULL,
	[Password] [nchar](50) NOT NULL,
	[AcessLevel] [smallint] NOT NULL,
	[Status] [smallint] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[UserCreated] [nchar](50) NULL,
	[TimeModified] [datetime] NOT NULL,
	[UserModified] [nchar](50) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserSpeakerGroup]    Script Date: 4/21/2015 7:13:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSpeakerGroup](
	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[UserUid] [uniqueidentifier] NULL,
	[AreaUid] [uniqueidentifier] NULL,
	[SpeakerGroupUid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_UserSpeakerGroup] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
--/****** Object:  Table [dbo].[SpeakerState]    Script Date: 4/21/2015 7:13:43 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[SpeakerState](
--	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
--	[SpeakerUid] [uniqueidentifier] NULL,
--	[ControlledByUserUid] [uniqueidentifier] NULL,
-- CONSTRAINT [PK_SpeakerState] PRIMARY KEY CLUSTERED 
--(
--	[Uid] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO
--/****** Object:  Table [dbo].[UserState]    Script Date: 4/21/2015 7:13:43 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[UserState](
--	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
--	[UserUid] [uniqueidentifier] NULL,
--	[Online] [smallint] NULL,
--	[Disconnected] [smallint] NULL,
--	[DisconnectedByUserUid] [uniqueidentifier] NULL,
-- CONSTRAINT [PK_UserState] PRIMARY KEY CLUSTERED 
--(
--	[Uid] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--/****** Object:  Table [dbo].[UserState]    Script Date: 4/21/2015 7:13:43 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[UserSpeakerState](
--	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
--	[UserUid] [uniqueidentifier] NULL,
--	[SpeakerUid] [uniqueidentifier] NULL,
--	[Online] [smallint] NULL,
--	[Disconnected] [smallint] NULL,
--	[DisconnectedByUserUid] [uniqueidentifier] NULL,
--	[ControlledByUserUid] [uniqueidentifier] NULL,
-- CONSTRAINT [PK_UserSpeakerState] PRIMARY KEY CLUSTERED 
--(
--	[Uid] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO


ALTER TABLE [dbo].[Area] ADD  DEFAULT (newid()) FOR [Uid]
GO
ALTER TABLE [dbo].[Area] ADD  DEFAULT (getdate()) FOR [TimeCreated]
GO
ALTER TABLE [dbo].[Area] ADD  DEFAULT (getdate()) FOR [TimeModified]
GO
ALTER TABLE [dbo].[SpeakerState] ADD  DEFAULT (newid()) FOR [Uid]
GO
ALTER TABLE [dbo].[Sound] ADD  DEFAULT (newid()) FOR [Uid]
GO
ALTER TABLE [dbo].[Sound] ADD  DEFAULT (getdate()) FOR [TimeCreated]
GO
ALTER TABLE [dbo].[Sound] ADD  DEFAULT (getdate()) FOR [TimeModified]
GO
ALTER TABLE [dbo].[SoundSpeakerGroup] ADD  DEFAULT (newid()) FOR [Uid]
GO
ALTER TABLE [dbo].[Speaker] ADD  DEFAULT (newid()) FOR [Uid]
GO
ALTER TABLE [dbo].[Speaker] ADD  DEFAULT (getdate()) FOR [TimeCreated]
GO
ALTER TABLE [dbo].[Speaker] ADD  DEFAULT (getdate()) FOR [TimeModified]
GO
ALTER TABLE [dbo].[SpeakerGroup] ADD  DEFAULT (newid()) FOR [Uid]
GO
ALTER TABLE [dbo].[SpeakerGroup] ADD  DEFAULT (getdate()) FOR [TimeCreated]
GO
ALTER TABLE [dbo].[SpeakerGroup] ADD  DEFAULT (getdate()) FOR [TimeModified]
GO
ALTER TABLE [dbo].[User] ADD  DEFAULT (newid()) FOR [Uid]
GO
ALTER TABLE [dbo].[User] ADD  DEFAULT (getdate()) FOR [TimeCreated]
GO
ALTER TABLE [dbo].[User] ADD  DEFAULT (getdate()) FOR [TimeModified]
GO
ALTER TABLE [dbo].[UserSpeakerGroup] ADD  DEFAULT (newid()) FOR [Uid]
GO
--ALTER TABLE [dbo].[SpeakerState] ADD  DEFAULT (newid()) FOR [Uid]
--GO
--ALTER TABLE [dbo].[UserState] ADD  DEFAULT (newid()) FOR [Uid]
--GO
--ALTER TABLE [dbo].[UserSpeakerState] ADD  DEFAULT (newid()) FOR [Uid]
--GO

ALTER TABLE [dbo].[SoundSpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_SoundSpeakerGroup_Sound] FOREIGN KEY([SoundUid])
REFERENCES [dbo].[Sound] ([Uid])
GO
ALTER TABLE [dbo].[SoundSpeakerGroup] CHECK CONSTRAINT [FK_SoundSpeakerGroup_Sound]
GO

ALTER TABLE [dbo].[SoundSpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_SoundSpeakerGroup_SpeakerGroup] FOREIGN KEY([SpeakerGroupUid])
REFERENCES [dbo].[SpeakerGroup] ([Uid])
GO
ALTER TABLE [dbo].[SoundSpeakerGroup] CHECK CONSTRAINT [FK_SoundSpeakerGroup_SpeakerGroup]
GO

ALTER TABLE [dbo].[SoundSpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_SoundSpeakerGroup_Speaker] FOREIGN KEY([SpeakerUid])
REFERENCES [dbo].[Speaker] ([Uid])
GO
ALTER TABLE [dbo].[SoundSpeakerGroup] CHECK CONSTRAINT [FK_SoundSpeakerGroup_Speaker]
GO

ALTER TABLE [dbo].[Speaker]  WITH CHECK ADD  CONSTRAINT [FK_Speaker_SpeakerGroup] FOREIGN KEY([SpeakerGroupUid])
REFERENCES [dbo].[SpeakerGroup] ([Uid])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Speaker] CHECK CONSTRAINT [FK_Speaker_SpeakerGroup]
GO
ALTER TABLE [dbo].[SpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_SpeakerGroup_Area] FOREIGN KEY([AreaUid])
REFERENCES [dbo].[Area] ([Uid])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[SpeakerGroup] CHECK CONSTRAINT [FK_SpeakerGroup_Area]
GO
ALTER TABLE [dbo].[UserSpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserSpeakerGroup_Area] FOREIGN KEY([AreaUid])
REFERENCES [dbo].[Area] ([Uid])
GO
ALTER TABLE [dbo].[UserSpeakerGroup] CHECK CONSTRAINT [FK_UserSpeakerGroup_Area]
GO
ALTER TABLE [dbo].[UserSpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserSpeakerGroup_SpeakerGroup] FOREIGN KEY([SpeakerGroupUid])
REFERENCES [dbo].[SpeakerGroup] ([Uid])
GO
ALTER TABLE [dbo].[UserSpeakerGroup] CHECK CONSTRAINT [FK_UserSpeakerGroup_SpeakerGroup]
GO
ALTER TABLE [dbo].[UserSpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserSpeakerGroup_User] FOREIGN KEY([UserUid])
REFERENCES [dbo].[User] ([Uid])
GO
ALTER TABLE [dbo].[UserSpeakerGroup] CHECK CONSTRAINT [FK_UserSpeakerGroup_User]
GO

USE [master]
GO
ALTER DATABASE [LRADMNS] SET  READ_WRITE 
GO


USE [LRADMNS]
GO
/****** Object:  ForeignKey [FK_SpeakerGroup_Area]    Script Date: 06/09/2015 11:08:10 ******/
ALTER TABLE [dbo].[SpeakerGroup] DROP CONSTRAINT [FK_SpeakerGroup_Area]
GO
/****** Object:  Table [dbo].[SpeakerGroup]    Script Date: 06/09/2015 11:08:10 ******/
ALTER TABLE [dbo].[SpeakerGroup] DROP CONSTRAINT [FK_SpeakerGroup_Area]
GO
ALTER TABLE [dbo].[SpeakerGroup] DROP CONSTRAINT [DF__SpeakerGrou__Uid__173876EA]
GO
ALTER TABLE [dbo].[SpeakerGroup] DROP CONSTRAINT [DF__SpeakerGr__TimeC__182C9B23]
GO
ALTER TABLE [dbo].[SpeakerGroup] DROP CONSTRAINT [DF__SpeakerGr__TimeM__1920BF5C]
GO
DROP TABLE [dbo].[SpeakerGroup]
GO
/****** Object:  Table [dbo].[SpeakerGroup]    Script Date: 06/09/2015 11:08:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpeakerGroup](
	[Uid] [uniqueidentifier] ROWGUIDCOL  NOT NULL DEFAULT (newid()),
	[AreaUid] [uniqueidentifier] NULL,
	[MultiCastGroup] [nchar](50) NULL,
	[Color] [int] NULL,
	[TimeCreated] [datetime] NOT NULL DEFAULT (getdate()),
	[UserCreated] [nchar](50) NULL,
	[TimeModified] [datetime] NOT NULL DEFAULT (getdate()),
	[UserModified] [nchar](50) NULL,
	[LabelLatitude] [float] NULL,
	[LabelLongitude] [float] NULL,
 CONSTRAINT [PK_SpeakerGroup] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_SpeakerGroup_Area]    Script Date: 06/09/2015 11:08:10 ******/
ALTER TABLE [dbo].[SpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_SpeakerGroup_Area] FOREIGN KEY([AreaUid])
REFERENCES [dbo].[Area] ([Uid])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[SpeakerGroup] CHECK CONSTRAINT [FK_SpeakerGroup_Area]
GO
