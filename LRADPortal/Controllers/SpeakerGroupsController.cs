using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LRADPortal;
using LRADPortal.Models;
using Microsoft.AspNet.Identity;

namespace LRADPortal.Controllers
{
    [Authorize]
    [RoutePrefix("api/speakergroups")]
    public class SpeakerGroupsController : ApiController
    {
        private LRADMNSEntities db = new LRADMNSEntities();

        /// <summary>
        /// Get the speaker groups within the specific Area 
        /// </summary>
        /// <param name="guid">Area ID</param>
        /// <returns></returns>
        [Route("GetSpeakerGroupByAreaId")]
        public IHttpActionResult GetSpeakerGroupByAreaId(string guid)
        {
            try
            {
                var groups = db.SpeakerGroups.Where(sg => sg.AreaUid.ToString() == guid).ToList();

                var result = groups.SelectMany(g => g.Speakers).Select(s => new
                {
                    IsSelected = false,
                    Grouping = s.SpeakerGroup.Name,
                    s.Name,
                    Playing = "",
                    Alert = s.SpeakerGroup.Speakers.Where(s1 => s1.SpeakerGroupUid == s.SpeakerGroupUid && s1.SpeakerAlerts.Count > 0).FirstOrDefault().SpeakerAlerts.FirstOrDefault().Text,
                }).ToList();

                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("GetAllSpeakerGroup")]
        public IHttpActionResult GetAllSpeakerGroup()
        {
            try
            {
                var groups = db.SpeakerGroups.ToList();

                var result = groups.SelectMany(g => g.Speakers).Select(s => new
                {
                    IsSelected = false,
                    Grouping = s.SpeakerGroup.Name,
                    SpeakerName = s.Name,
                    Playing = "",
                    Alert = s.SpeakerGroup.Speakers.Where(s1 => s1.SpeakerGroupUid == s.SpeakerGroupUid && s.SpeakerAlerts.Count > 0).FirstOrDefault().SpeakerAlerts.FirstOrDefault().Text,
                    AreaId = s.SpeakerGroup.AreaUid, 
                    SpeakerUid = s.Uid
                }).ToList();

                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // POST api/speakergroups/AddNewGroup
        [Route("AddNewGroup")]
        [HttpPost]
        public IHttpActionResult AddNewGroup(NewSpeakerGroup data)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var groupUid = Guid.NewGuid();

                var newGroup = new SpeakerGroup()
                {
                    Uid = groupUid,
                    Name = data.Name,
                    AreaUid = Guid.Parse(data.AreaUid),
                    MultiCastGroup = data.MultiCastGroup,
                    Color = data.Color,
                    UserCreated = userId,
                    TimeCreated = DateTime.Now,
                    UserModified = userId,
                    TimeModified = DateTime.Now
                };
                db.SpeakerGroups.Add(newGroup);
                var newUserGroup = new UserSpeakerGroup()
                {
                    Uid =  Guid.NewGuid(),
                    UserUid = userId,
                    AreaUid = Guid.Parse(data.AreaUid),
                    SpeakerGroupUid=groupUid,
                };
                
                db.UserSpeakerGroups.Add(newUserGroup);

                var area = db.Areas.Single(s => s.Uid.ToString() == data.AreaUid);
                area.TimeModified = DateTime.Now;
                area.UserModified = userId;

                data.GeneralFilesList.ForEach(f =>
                {
                    var soundGroup = new SoundSpeakerGroup()
                    {
                        Uid = Guid.NewGuid(),
                        SoundUid = Guid.Parse(f.Uid),
                        SpeakerGroupUid = groupUid
                    };

                    db.SoundSpeakerGroups.Add(soundGroup);
                });

                data.EmergencyFilesList.ForEach(f =>
                {
                    var soundGroup = new SoundSpeakerGroup()
                    {
                        Uid = Guid.NewGuid(),
                        SoundUid = Guid.Parse(f.Uid),
                        SpeakerGroupUid = groupUid
                    };

                    db.SoundSpeakerGroups.Add(soundGroup);
                });

                db.SaveChanges();

                return Ok(groupUid);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET: api/SpeakerGroups
        public IQueryable<SpeakerGroup> GetSpeakerGroups()
        {
            return db.SpeakerGroups;
        }

        // GET: api/SpeakerGroups/5
        [ResponseType(typeof(SpeakerGroup))]
        public async Task<IHttpActionResult> GetSpeakerGroup(Guid id)
        {
            SpeakerGroup speakerGroup = await db.SpeakerGroups.FindAsync(id);
            if (speakerGroup == null)
            {
                return NotFound();
            }

            return Ok(speakerGroup);
        }

        // PUT: api/SpeakerGroups/5
        [Route("UpdateSpeakerGroup")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateSpeakerGroup(NewSpeakerGroup speakerGroup)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var id = Guid.Parse(speakerGroup.Uid);

            var group = db.SpeakerGroups.SingleOrDefault(s => s.Uid == id);
            if (group == null)
                return NotFound();
            group.Name = speakerGroup.Name;
            group.Color = speakerGroup.Color;
            group.AreaUid = Guid.Parse(speakerGroup.AreaUid);
            group.TimeModified = DateTime.Now;
            group.UserModified = User.Identity.GetUserId();
            group.Area.TimeModified = DateTime.Now;
            group.Area.UserModified = User.Identity.GetUserId();
            // remove all assigned sounds first
            db.SoundSpeakerGroups.RemoveRange(db.SoundSpeakerGroups.Where(w => w.SpeakerGroupUid == id));

            // addd the newly assigned sounds
            speakerGroup.GeneralFilesList.ForEach(f =>
            {
                var soundGroup = new SoundSpeakerGroup()
                {
                    Uid = Guid.NewGuid(),
                    SoundUid = Guid.Parse(f.Uid),
                    SpeakerGroupUid = id
                };

                db.SoundSpeakerGroups.Add(soundGroup);
            });

            speakerGroup.EmergencyFilesList.ForEach(f =>
            {
                var soundGroup = new SoundSpeakerGroup()
                {
                    Uid = Guid.NewGuid(),
                    SoundUid = Guid.Parse(f.Uid),
                    SpeakerGroupUid = id
                };

                db.SoundSpeakerGroups.Add(soundGroup);
            });

            db.Entry(group).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SpeakerGroupExists(id))
                {
                    return NotFound();
                }
                    throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SpeakerGroups
        [ResponseType(typeof(SpeakerGroup))]
        public async Task<IHttpActionResult> PostSpeakerGroup(SpeakerGroup speakerGroup)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SpeakerGroups.Add(speakerGroup);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SpeakerGroupExists(speakerGroup.Uid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = speakerGroup.Uid }, speakerGroup);
        }

        // DELETE: api/SpeakerGroups/5
        [ResponseType(typeof(SpeakerGroup))]
        public async Task<IHttpActionResult> DeleteSpeakerGroup(Guid id)
        {
            SpeakerGroup speakerGroup = db.SpeakerGroups.SingleOrDefault(s=>s.Uid == id);
            if (speakerGroup == null)
            {
                return NotFound();
            }
            speakerGroup.Area.UserModified = User.Identity.GetUserId();
            speakerGroup.Area.TimeModified = DateTime.Now;
            db.SpeakerGroups.Remove(speakerGroup);
            await db.SaveChangesAsync();

            return Ok(speakerGroup);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SpeakerGroupExists(Guid id)
        {
            return db.SpeakerGroups.Count(e => e.Uid == id) > 0;
        }
    }
}