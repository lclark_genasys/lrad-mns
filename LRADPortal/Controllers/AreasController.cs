using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LRADPortal.Models;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;

namespace LRADPortal.Controllers
{
    [Authorize]
    public class AreasController : ApiController
    {
        private LRADMNSEntities db = new LRADMNSEntities();

        public IHttpActionResult GetAreasForOverview()
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var filteredAreas =
                    db.Areas.GroupJoin(db.AspNetUsers, x => x.UserModified, y => y.Id, (x, y) => new {x, y})
                        .SelectMany(s => s.y.DefaultIfEmpty(),
                            (a, y) =>
                                new
                                {
                                    a.x.Uid,
                                    a.x.Name,
                                    a.x.CenterLatitude,
                                    a.x.CenterLongitude,
                                    a.x.Zoom,
                                    UserModified = y.UserInfo.FirstName + " " + y.UserInfo.LastName,
                                    UserId = y.Id,
                                    a.x.TimeModified,
                                    SpeakerGroups =
                                        db.UserSpeakerGroups.Where(
                                            u => u.UserUid == userId)
                                            .Join(a.x.SpeakerGroups, usersp => usersp.SpeakerGroupUid, sgg => sgg.Uid,
                                                (usersp, spg) => new {spg.Name}),
                                    Alerts = db.UserSpeakerGroups.Where(
                                        u => u.UserUid == userId)
                                        .Join(a.x.SpeakerGroups, usersp => usersp.SpeakerGroupUid, sgg => sgg.Uid,
                                            (usersp, spg) => new {spg.Speakers})
                                        .SelectMany(
                                            sg =>
                                                sg.Speakers.SelectMany(s => s.SpeakerAlerts)
                                                    .Select(s => new {s.Id, s.SpeakerUid, s.Text, s.Datetime})),
                                    CustomImageOverlay =
                                        a.x.AreaImages.Select(
                                            s =>
                                                new
                                                {
                                                    s.Id,
                                                    s.ImageFilename,
                                                    s.LowerLeftLatitude,
                                                    s.LowerLeftLongitude,
                                                    s.UpperRightLatitude,
                                                    s.UpperRightLongitude
                                                }).FirstOrDefault(),
                                    HasCustomImage = a.x.AreaImages.Count > 0
                                }).Where(s => s.SpeakerGroups.Any() || s.UserId == userId);

                return Ok(filteredAreas);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult GetAreaById(string areaId, string groupId)
        {
            try
            {
                bool isAllArea = areaId.IsNullOrWhiteSpace();
                bool isAllGroup = groupId.IsNullOrWhiteSpace();
                Guid areaUid = isAllArea? Guid.Empty : Guid.Parse(areaId);
                Guid groupUid = isAllGroup ? Guid.Empty : Guid.Parse(groupId);
                string userId = User.Identity.GetUserId();
                var filteredArea = db.Areas.Select(
                    a =>
                        new
                        {
                            a.Uid,
                            CustomImageOverlay = a.AreaImages.Select(
                                            s =>
                                                new
                                                {
                                                    s.Id,
                                                    s.AreaUid,
                                                    s.ImageFilename,
                                                    s.LowerLeftLatitude,
                                                    s.LowerLeftLongitude,
                                                    s.UpperRightLatitude,
                                                    s.UpperRightLongitude
                                                }).FirstOrDefault(w => w.AreaUid == areaUid || isAllArea),
                            HasCustomImage = a.AreaImages.Count(c => c.AreaUid == areaUid || isAllArea) > 0,
                            a.CenterLatitude,
                            a.CenterLongitude,
                            a.UserCreated,
                            a.Zoom,
                            SpeakerGroups =
                                db.UserSpeakerGroups.Where(u => u.UserUid == userId)
                                    .Join(a.SpeakerGroups, usg => usg.SpeakerGroupUid, sg => sg.Uid,
                                        (usg, sg) =>
                                            new
                                            {
                                                sg.Uid,
                                                sg.Color,
                                                sg.MultiCastGroup,
                                                sg.LabelLatitude,
                                                sg.LabelLongitude,
                                                sg.Name,
                                                sg.AreaUid,
                                                Speakers =
                                                    sg.Speakers.Select(
                                                        s =>
                                                            new
                                                            {
                                                                s.Uid,
                                                                s.Name,
                                                                sg.Color,
                                                                s.SpeakerGroupUid,
                                                                s.SatelliteID,
                                                                s.Latitude,
                                                                s.Longitude,
                                                                s.GeneralVolume,
                                                                s.EmergencyVolume,
                                                                IPAddress = s.IPAddress.Trim(),
                                                                s.Type,
                                                                s.SoundUid,
                                                                s.StartTime,
                                                                s.RepeatTimes,
                                                                s.Repeat,
                                                                s.PredefinedSound,
                                                                s.PlayMode,
                                                                s.TTSUid,
                                                                Alerts = s.SpeakerAlerts.Select(al => new { al.Id, al.SpeakerUid, al.Text, al.Datetime })
                                                            })
                                            }).Where(w => w.Uid == groupUid || isAllGroup)
                        }).Where(w => (w.Uid == areaUid || isAllArea) && (w.SpeakerGroups.Any()|| w.UserCreated == userId));

                #region GetCenterPoint
                var speakersLocations = filteredArea.
                        SelectMany(a => a.SpeakerGroups.SelectMany(sg => sg.Speakers))
                        .Where(a => a.Latitude != null && a.Longitude != null)
                        .Select(a => new MapPoint { Lat = a.Latitude.Value, Lng = a.Longitude.Value }).ToList();

                MapPoint center = new MapPoint();
                if (speakersLocations.Count > 0)
                    center = GetCentralGeoCoordinate(speakersLocations);
                else
                {
                    center.Lat = filteredArea.Single().CenterLatitude.GetValueOrDefault();
                    center.Lng = filteredArea.Single().CenterLongitude.GetValueOrDefault();
                }
                #endregion

                var wholeArea =
                    new AreaGroupMap
                    {
                        Uid = "",
                        CenterLatitude = center.Lat,
                        CenterLongitude = center.Lng,
                        Zoom = filteredArea.Min(r => r.Zoom) - 2 ?? 13, //find the smallest zoom value
                        SpeakerGroups = filteredArea.SelectMany(r => r.SpeakerGroups),
                        CustomImageOverlay = filteredArea.Select(s=>s.CustomImageOverlay).Where(w=>w!=null),
                        HasCustomImage = filteredArea.Select(s => s.HasCustomImage).Any(a=>a.Equals(true))
                    };
                    
                    return Ok(wholeArea);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET: api/GetAllAreas
        public IQueryable<object> GetAllAreas()
        {
            return
                db.Areas.Select(
                    x =>
                        new
                        {
                            x.Uid,
                            x.Name,
                            x.Zoom,
                            Groups =
                                x.SpeakerGroups.Select(
                                    g =>
                                        new
                                        {
                                            g.Name,
                                            g.Uid,
                                            g.AreaUid,
                                            g.Color,
                                            g.MultiCastGroup,
                                            Alert = g.Speakers.FirstOrDefault(s => s.SpeakerGroupUid == g.Uid && s.SpeakerAlerts.Count > 0).SpeakerAlerts.FirstOrDefault().Text,
                                            Playing ="",
                                            Speakers =
                                                g.Speakers.Select(
                                                    s =>
                                                        new
                                                        {
                                                            s.Name,
                                                            s.Uid,
                                                            g.Color,
                                                            s.SpeakerGroupUid,
                                                            s.Longitude,
                                                            s.Latitude,
                                                            s.EmergencyVolume,
                                                            s.GeneralVolume,
                                                            IPAddress = s.IPAddress.Trim(),
                                                            s.SatelliteID,
                                                            s.Type,
                                                            s.SoundUid,
                                                            s.StartTime,
                                                            s.PlayMode,
                                                            s.PredefinedSound,
                                                            s.TTSUid
                                                        })
                                        })
                        });
        }

        // GET: api/GetAreasForDropdown
        public IQueryable<object> GetAreasForDropdown()
        {
            return
                db.Areas.Select(
                    x =>
                        new
                        {
                            x.Uid,
                            x.Name,
                            x.Zoom,
                            Groups = ""
                        });
        }
       
        // GET: api/GetAssignedAreas
        public IQueryable<object> GetAssignedAreas()
        {
            string userId = User.Identity.GetUserId();
            var userSpeakerGroups=db.UserSpeakerGroups.Where(u => u.UserUid == userId);
            var allAreas = db.Areas.Select(
                area =>
                    new
                    {
                        area.Uid,
                        area.Name,
                        area.Zoom,
                        area.UserCreated,
                        Groups =
                            userSpeakerGroups.Join(area.SpeakerGroups, usersp => usersp.SpeakerGroupUid, sgg => sgg.Uid,
                                (usersp, spg) =>
                                    new
                                    {
                                        spg.Name,
                                        spg.Uid,
                                        longitude = spg.LabelLatitude,
                                        latitude = spg.LabelLongitude,
                                        spg.Color,
                                        spg.AreaUid,
                                        spg.MultiCastGroup,
                                        Alert =
                                            spg.Speakers.Where(
                                                s => s.SpeakerGroupUid == spg.Uid && s.SpeakerAlerts.Count > 0)
                                                .FirstOrDefault()
                                                .SpeakerAlerts.FirstOrDefault()
                                                .Text,
                                        Playing = "",
                                        Speakers =
                                            spg.Speakers.Select(
                                                s =>
                                                    new
                                                    {
                                                        s.Uid,
                                                        s.Name,
                                                        s.SpeakerGroupUid,
                                                        s.Repeat,
                                                        s.RepeatTimes,
                                                        s.PredefinedSound,
                                                        spg.Color,
                                                        longitude = s.Longitude,
                                                        latitude = s.Latitude,
                                                        s.EmergencyVolume,
                                                        s.GeneralVolume,
                                                        IPAddress = s.IPAddress.Trim(),
                                                        s.SatelliteID,
                                                        s.Type,
                                                        s.SoundUid,
                                                        s.StartTime,
                                                        s.PlayMode,
                                                        s.TTSUid,
                                                        s.NetworkName,
                                                        Alerts =
                                                            s.SpeakerAlerts.Select(
                                                                a => new {a.Id, a.SpeakerUid, a.Text, a.Datetime})
                                                    }),
                                        //Sounds = spg.SoundSpeakerGroups.Select(ssg => ssg.Sound).Select(
                                        //    s => new
                                        //    {
                                        //        s.Uid,
                                        //        s.Name,
                                        //        s.Type,
                                        //        s.Description,
                                        //        s.Filename,
                                        //        s.Text,
                                        //        s.TimeCreated,
                                        //        s.UserCreated,
                                        //        s.TimeModified,
                                        //        s.UserModified
                                        //    })
                                    })
                    });

            IQueryable<object> assigned = allAreas.Where(a => a.Groups.Any() || a.UserCreated == userId);
            return assigned;
        }

        // POST api/Speakers/AddNewSpeaker
        [Route("api/Areas/AddNewArea")]
        [HttpPost]
        public IHttpActionResult AddNewArea(NewArea data)
        {
            try
            {
                string userId = User.Identity.GetUserId();

                var areaUid = Guid.NewGuid();

                var newArea = new Area()
                {
                    Uid = areaUid,
                    Name = data.Name,
                    CenterLatitude = data.Latitude,
                    CenterLongitude = data.Longitude,
                    Zoom = data.Zoom,
                    UserCreated = userId,
                    TimeCreated = DateTime.Now,
                    UserModified = userId,
                    TimeModified = DateTime.Now
                };

                db.Areas.Add(newArea);

                if (data.AreaImage != null)
                {
                    data.AreaImage.AreaUid = areaUid;
                    db.AreaImages.Add(data.AreaImage);
                }

                db.SaveChanges();

                return Ok(areaUid);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [Route("api/Areas/UpdateArea")]
        [HttpPut]
        public IHttpActionResult UpdateArea(NewArea data)
        {
            try
            {
                var area = db.Areas.SingleOrDefault(s => s.Uid.ToString() == data.Uid);
                if (area == null)
                    return NotFound();
                if (db.Areas.Count(e => e.Uid.ToString() != data.Uid && e.Name == data.Name.Trim()) > 0)
                    return BadRequest("Area name existing");
                area.Name = data.Name;
                return Ok(db.SaveChanges() > 0);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/Areas/IsExisting")]
        [HttpGet]
        public IHttpActionResult IsAreaNameExisting(string id, string name)
        {
            return Ok(db.Areas.Count(e => e.Uid.ToString() != id && e.Name == name.Trim()) > 0);
        }

        // GET: api/Areas/5
        [ResponseType(typeof(Area))]
        public async Task<IHttpActionResult> GetArea(Guid id)
        {
            Area area = await db.Areas.FindAsync(id);
            if (area == null)
            {
                return NotFound();
            }

            return Ok(area);
        }

        // PUT: api/Areas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutArea(Guid id, Area area)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != area.Uid)
            {
                return BadRequest();
            }

            db.Entry(area).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AreaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Areas
        [ResponseType(typeof(Area))]
        public async Task<IHttpActionResult> PostArea(Area area)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Areas.Add(area);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AreaExists(area.Uid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = area.Uid }, area);
        }

        // DELETE: api/Areas/5
        [Route("api/Areas/DeleteArea")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteArea(Guid id)
        {
            Area area = await db.Areas.FindAsync(id);
            if (area == null)
            {
                return NotFound();
            }

            var image = area.AreaImages;
            db.AreaImages.RemoveRange(image);
            var userSpeakerGroup = area.UserSpeakerGroups;
            db.UserSpeakerGroups.RemoveRange(userSpeakerGroup);
            var speakerGroups = area.SpeakerGroups;
            db.SpeakerGroups.RemoveRange(speakerGroups); // remove all speaker groups under this area

            db.Areas.Remove(area);

            await db.SaveChangesAsync();

            return Ok(area);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AreaExists(Guid id)
        {
            return db.Areas.Count(e => e.Uid == id) > 0;
        }

        public static MapPoint GetCentralGeoCoordinate(
       IList<MapPoint> geoCoordinates)
        {
            if (geoCoordinates.Count == 1)
            {
                return geoCoordinates.Single();
            }

            double x = 0;
            double y = 0;
            double z = 0;

            foreach (var geoCoordinate in geoCoordinates)
            {
                var latitude = geoCoordinate.Lat * Math.PI / 180;
                var longitude = geoCoordinate.Lng * Math.PI / 180;

                x += Math.Cos(latitude) * Math.Cos(longitude);
                y += Math.Cos(latitude) * Math.Sin(longitude);
                z += Math.Sin(latitude);
            }

            var total = geoCoordinates.Count;

            x = x / total;
            y = y / total;
            z = z / total;

            var centralLongitude = Math.Atan2(y, x);
            var centralSquareRoot = Math.Sqrt(x * x + y * y);
            var centralLatitude = Math.Atan2(z, centralSquareRoot);

            return new MapPoint{Lat = centralLatitude * 180 / Math.PI, Lng = centralLongitude * 180 / Math.PI};
        }

    }
}