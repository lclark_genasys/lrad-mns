﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace LRADPortal.Controllers
{
    public class SpeechesController : ApiController
    {
        private LRADMNSEntities db = new LRADMNSEntities();

        [HttpGet]
        [ResponseType(typeof(SpeechToText))]
        public async Task<IHttpActionResult> GetSpeech(Guid id)
        {
            SpeechToText speech = await db.SpeechToTexts.FindAsync(id);
            if (speech == null)
            {
                return NotFound();
            }

            return Ok(speech);
        }

        [HttpPost]
        public IHttpActionResult AddNewSpeech(string body)
        {
            try
            {
                string userId = User.Identity.GetUserId();

                var newSpeechText = new SpeechToText()
                {
                    Uid = Guid.NewGuid(),
                    Body = body,
                    UserCreated = userId,
                    TimeCreated = DateTime.Now
                };

                db.SpeechToTexts.Add(newSpeechText);
                db.SaveChanges();

                return Ok(newSpeechText);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
