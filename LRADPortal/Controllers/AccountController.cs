using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using LRADPortal.Models;
using LRADPortal.Providers;
using LRADPortal.Results;
using WebGrease.Css.Extensions;
using Newtonsoft.Json.Linq;

namespace LRADPortal.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;
        private LRADMNSEntities _db = new LRADMNSEntities();

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        [AllowAnonymous]
        [HttpPost]
        [Route("ForgetPassword")]
        public IHttpActionResult ForgetPassword(ForgetPasswordModel model)
        {
            var email = model.Email;
            
            return Ok();
        }

        [Route("UserManagementInfo")]
        public IHttpActionResult GetUserManagementInfo()
        {
            try
            {
                List<UserManagementInfo> userManagementInfoList = new List<UserManagementInfo>();

                using (var db = new LRADMNSEntities())
                {
                    var currentUserId = User.Identity.GetUserId();
                    var users = db.AspNetUsers.Join(db.UserInfoes, anu => anu.UserInfo_Uid, ui => ui.Uid,
                        (anu, ui) => new UserManagementInfo()
                        {
                            FirstName = ui.FirstName,
                            LastName = ui.LastName,
                            AccessLevel = (UserAccessLevel)ui.AcessLevel,
                            Status = (UserStatus)ui.Status,
                            UserInfoId = ui.Uid.ToString(),
                            Phone = anu.PhoneNumber,
                            Email = anu.Email,
                            UserName = anu.UserName,
                            Uid = anu.Id,
                            IsSelf = anu.Id == currentUserId,
                            AreaSpeakerList = db.UserSpeakerGroups.Where(usg => usg.UserUid == anu.Id).Select(r => new AreaSpeakerGroup()
                            {
                                AreaUid = r.AreaUid.ToString(),
                                AreaName = r.Area.Name,
                                SpeakerGroupUid = r.SpeakerGroupUid.ToString(),
                                SpeakerGroupName = r.SpeakerGroup.Name
                            })
                        });

                    userManagementInfoList.AddRange(users);
                }

                return Ok(userManagementInfoList);

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
           
        }

        [Route("GetLoginUserInfo")]
        public IHttpActionResult GetLoginUserInfo(string id)
        {
            string userName = id;
            
            try
            {
                var aspUser = _db.AspNetUsers.Where(i => i.UserName == userName).Include("UserSpeakerGroups").FirstOrDefault();

                if (aspUser != null)
                {
                    var loginUser = new UserManagementInfo()
                    {
                        FirstName = aspUser.UserInfo.FirstName,
                        LastName = aspUser.UserInfo.LastName,
                        AccessLevel = (UserAccessLevel)aspUser.UserInfo.AcessLevel,
                        Status = (UserStatus)aspUser.UserInfo.Status,
                        UserInfoId = aspUser.UserInfo.Uid.ToString(),
                        Phone = aspUser.PhoneNumber,
                        Email = aspUser.Email,
                        UserName = aspUser.UserName,
                        Uid = aspUser.Id,
                        AreaSpeakerList = aspUser.UserSpeakerGroups.Select(g => new AreaSpeakerGroup()
                        {
                            AreaUid = g.AreaUid.ToString(),
                            AreaName = g.Area.Name,
                            SpeakerGroupUid = g.SpeakerGroupUid.ToString(),
                            SpeakerGroupName = g.SpeakerGroup.Name
                        })
                    };

                    return Ok(loginUser);
                }
                else
                {
                    return BadRequest("Unable to get login user information.");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpPost]
        public IHttpActionResult VerifyUniqueEmail([FromBody] JObject obj)
        {
            using (var db = new LRADMNSEntities())
            {
                var Uid = obj["userUid"].ToString();
                var Email = obj["userEmail"].ToString();
                var NbUser = db.AspNetUsers.Where(u => u.Id != Uid && u.Email == Email).Count();
                if (NbUser > 0)
                {
                    return Ok(new { result = false });
                }
                return Ok(new { result = true });
            }
        }

        // POST api/Account/AddNewUser
        [Route("AddNewUser")]
        [HttpPost]
        public async Task<IHttpActionResult> AddNewUser(UserManagementInfo userViewModel)
        {
            var newUser = new ApplicationUser()
            {
                UserName = userViewModel.Email,
                Email = userViewModel.Email,
                PhoneNumber = userViewModel.Phone,
                UserInfo = new Models.UserInfo()
                {
                    FirstName = userViewModel.FirstName,
                    LastName = userViewModel.LastName,
                    AcessLevel = (byte)userViewModel.AccessLevel,
                    Status = (byte)userViewModel.Status,
                    Uid = Guid.NewGuid(),
                    UserCreated = DateTime.Now,
                    TimeCreated = DateTime.Now,
                    UserModified = DateTime.Now,
                    TimeModified = DateTime.Now
                }
            };

            IdentityResult result = await UserManager.CreateAsync(newUser, userViewModel.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            else
            {
                var userId = newUser.Id;

                try
                {
                    using (var db = new LRADMNSEntities())
                    {
                        userViewModel.AreaSpeakerList.ForEach(i =>
                        {
                            var userGroup = new UserSpeakerGroup()
                            {
                                Uid = Guid.NewGuid(),
                                UserUid = userId,
                                AreaUid = Guid.Parse(i.AreaUid),
                                SpeakerGroupUid = Guid.Parse(i.SpeakerGroupUid)
                            };

                            db.UserSpeakerGroups.Add(userGroup);
                        });

                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }

            return Ok();
        }

        // POST api/Account/UpdateUser
        [Route("UpdateUser")]
        [HttpPut]
        public IHttpActionResult UpdateUser(UserManagementInfo userViewModel)
        {
            try
            {
                using (var db = new LRADMNSEntities())
                {
                    var aspUser = db.AspNetUsers.FirstOrDefault(au => au.Id == userViewModel.Uid);
                    if (aspUser != null)
                    {
                        aspUser.UserName = userViewModel.Email;
                        aspUser.Email = userViewModel.Email;
                        aspUser.PhoneNumber = userViewModel.Phone;
                        aspUser.UserInfo.FirstName = userViewModel.FirstName;
                        aspUser.UserInfo.LastName = userViewModel.LastName;
                        aspUser.UserInfo.AcessLevel = (byte)userViewModel.AccessLevel;
                        aspUser.UserInfo.Status = (byte)userViewModel.Status;
                        aspUser.UserInfo.UserModified = DateTime.Now.ToString(CultureInfo.CurrentUICulture);
                        aspUser.UserInfo.TimeModified = DateTime.Now;

                        var userGroup = db.UserSpeakerGroups.Where(g => g.UserUid == userViewModel.Uid).ToList();
                        db.UserSpeakerGroups.RemoveRange(userGroup);

                        userViewModel.AreaSpeakerList.ForEach(i =>
                        {
                            var newUserGroup = new UserSpeakerGroup()
                            {
                                Uid = Guid.NewGuid(),
                                UserUid = userViewModel.Uid,
                                AreaUid = Guid.Parse(i.AreaUid),
                                SpeakerGroupUid = Guid.Parse(i.SpeakerGroupUid)
                            };

                            db.UserSpeakerGroups.Add(newUserGroup);
                        });

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        // DELETE api/Account/DeleteUser
        [Route("DeleteUser")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteUser(string id)
        {
            try
            {
                using (var db = new LRADMNSEntities())
                {
                    var aspUser = db.AspNetUsers.FirstOrDefault(au => au.Id == id);

                    if (aspUser != null)
                    {
                        var userInfo = db.UserInfoes.FirstOrDefault(u => u.Uid == aspUser.UserInfo_Uid);
                        if (userInfo != null)
                        {
                            db.UserInfoes.Remove(userInfo);
                        }

                        var userGroup = db.UserSpeakerGroups.Where(g => g.UserUid == id).ToList();
                        db.UserSpeakerGroups.RemoveRange(userGroup);
                        db.AspNetUsers.Remove(aspUser);
                        db.SaveChanges();
                    }
                }

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        // POST api/Account/ChangePassword
        [Route("ChangeUserPassword")]
        public async Task<IHttpActionResult> ChangeUserPassword(ChangePasswordBindingModel model)
        {
            try
            {
                string userId = User.Identity.GetUserId();

                var user = _db.AspNetUsers.Single(u => u.Id == userId);

                if (UserManager.PasswordHasher.VerifyHashedPassword(user.PasswordHash, model.OldPassword) == PasswordVerificationResult.Failed)
                {
                    return BadRequest("Your old password is wrong.");
                };

                user.PasswordHash = UserManager.PasswordHasher.HashPassword(model.NewPassword);

                var result = await _db.SaveChangesAsync();
               
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                
                 ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await Authentication.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return InternalServerError();
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result); 
            }
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
                _db.Dispose();
                _db = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
