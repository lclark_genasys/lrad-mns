﻿using LRADPortal.Helper;
using LRADPortal.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Description;
using System.Windows.Forms;

namespace LRADPortal.Controllers
{
    [Authorize]
    [RoutePrefix("api/Speakers")]
    public class SpeakersController : ApiController
    {
        private LRADMNSEntities db = new LRADMNSEntities();
        private string host = WebConfigurationManager.AppSettings["API_IP"];
        private string port = WebConfigurationManager.AppSettings["API_PORT"];

        // GET: api/Speakers
        public IQueryable<Speaker> GetSpeakers()
        {
            return db.Speakers;
        }

        // GET: api/Speakers/Count
        [Route("Count")]
        [HttpGet]
        public int GetSpeakersCount()
        {
            var speakersCount = this.GetSpeakers().Count();

            return speakersCount;
        }

        // GET: api/Speakers/5
        [ResponseType(typeof(Speaker))]
        public async Task<IHttpActionResult> GetSpeaker(Guid id)
        {
            Speaker speaker = await db.Speakers.FindAsync(id);
            if (speaker == null)
            {
                return NotFound();
            }

            return Ok(speaker);
        }

        // POST api/Speakers/AddNewSpeaker
        [Route("AddNewSpeaker")]
        [HttpPost]
        public async Task<IHttpActionResult> AddNewSpeaker(NewSpeaker data)
        {
            try
            {
                string userId = User.Identity.GetUserId();

                var speakerUid = data.Uid != null ? Guid.Parse(data.Uid) : Guid.NewGuid();

                var newSpeaker = new Speaker()
                {
                    Uid = speakerUid,
                    SpeakerGroupUid = Guid.Parse(data.SpeakerGroupUid),
                    Type = data.Type,
                    Name = data.Name,
                    IPAddress = data.IPAddress.Trim(),
                    SatelliteID = data.SatelliteID,
                    GeneralVolume = data.GeneralVolume,
                    EmergencyVolume = data.EmergencyVolume,
                    Longitude = data.longitude,
                    Latitude = data.latitude,
                    UserCreated = userId,
                    TimeCreated = DateTime.Now,
                    UserModified = userId,
                    TimeModified = DateTime.Now,
                    MacAddress = data.MacAddress,
                    NetworkName = data.NetworkName,
                    PredefinedSound = 1
                };

                await LradAccessManager.CreateLrad(newSpeaker);

                db.Speakers.Add(newSpeaker);
                var userSpeakerGroup = db.UserSpeakerGroups.SingleOrDefault(w => w.SpeakerGroupUid == newSpeaker.SpeakerGroupUid && w.UserUid == userId);
                if (userSpeakerGroup != null)
                {
                    var area = userSpeakerGroup.Area;
                    area.UserModified = userId;
                    area.TimeModified = DateTime.Now;
                }
                db.SaveChanges();

                return Ok(newSpeaker);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult GetSpeakerTypes()
        {
            try
            {
                return Ok(db.SpeakerTypes.Select(st => new { st.Id, st.Name, st.MaxCoverageRadius }).ToDictionary(st => st.Id));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult GetUnassignedSpeakers()
        {
            try
            {
                return
                    Ok(
                        db.Speakers.Where(w => w.SpeakerGroupUid == null)
                            .Select(
                                s =>
                                    new
                                    {
                                        s.Uid,
                                        IPAddress = s.IPAddress.Trim(),
                                        s.SatelliteID,
                                        s.Type,
                                        s.GeneralVolume,
                                        s.EmergencyVolume,
                                        s.Latitude,
                                        s.Longitude,
                                        s.TimeCreated,
                                        s.UserCreated,
                                        s.TimeModified,
                                        s.UserModified,
                                        s.Name,
                                        s.SoundUid,
                                        s.StartTime,
                                        s.RepeatTimes,
                                        s.Repeat,
                                        Alerts = s.SpeakerAlerts.Select(al => new { al.Id, al.SpeakerUid, al.Text, al.Datetime })
                                    }));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("UpdateSpeaker")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateSpeaker(NewSpeaker data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var id = Guid.Parse(data.Uid);
            var speaker = db.Speakers.SingleOrDefault(s => s.Uid == id);
            if (speaker == null)
                return NotFound();
            var userId = User.Identity.GetUserId();
            speaker.EmergencyVolume = data.EmergencyVolume;
            speaker.GeneralVolume = data.GeneralVolume;
            speaker.SpeakerGroupUid = Guid.Parse(data.SpeakerGroupUid);
            speaker.Type = data.Type;
            speaker.IPAddress = data.IPAddress.Trim();
            speaker.Longitude = data.longitude;
            speaker.Latitude = data.latitude;
            speaker.Name = data.Name;
            speaker.SatelliteID = data.SatelliteID;
            speaker.TimeModified = DateTime.Now;
            speaker.UserModified = userId;
            speaker.MacAddress = data.MacAddress;
            speaker.Repeat = data.Repeat;
            if (data.PredefinedSound != 0)
                speaker.PredefinedSound = data.PredefinedSound;

            if (!String.IsNullOrEmpty(data.SoundUid))
            {
                speaker.SoundUid = Guid.Parse(data.SoundUid);
            }
            else
            {
                speaker.SoundUid = null;
            }

            if (!String.IsNullOrEmpty(data.RepeatTimes))
            {
                speaker.RepeatTimes = Int32.Parse(data.RepeatTimes);
            }

            speaker.StartTime = data.StartTime;

            if (!String.IsNullOrEmpty(data.PlayMode))
            {
                speaker.PlayMode = Byte.Parse(data.PlayMode);
            }
            else
            {
                speaker.PlayMode = null;
            }

            if (!String.IsNullOrEmpty(data.TTSUid))
            {
                speaker.TTSUid = Guid.Parse(data.TTSUid);
            }
            else
            {
                speaker.TTSUid = null;
            }

            db.Entry(speaker).State = EntityState.Modified;

            
            var userSpeakerGroup = db.UserSpeakerGroups.SingleOrDefault(w => w.SpeakerGroupUid == speaker.SpeakerGroupUid && w.UserUid == userId);
            if (userSpeakerGroup != null)
            {
                var area = userSpeakerGroup.Area;
                area.UserModified = userId;
                area.TimeModified = DateTime.Now;
            }

            try
            {
                await LradAccessManager.UpdatedLrad(speaker);
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SpeakerExists(id))
                {
                    return NotFound();
                }
                throw;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        // POST: api/Speakers
        [ResponseType(typeof(Speaker))]
        public async Task<IHttpActionResult> PostSpeaker(Speaker speaker)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Speakers.Add(speaker);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SpeakerExists(speaker.Uid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = speaker.Uid }, speaker);
        }
        
        [Route("DeleteSpeaker")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteSpeaker(Guid id)
        {
            Speaker speaker = await db.Speakers.FindAsync(id);
            if (speaker == null)
            {
                return NotFound();
            }
            try
            {
                await LradAccessManager.DeleteLrad(id);
            }
            catch (Exception e)
            {
                BadRequest(e.Message);
            }
            finally
            {
                db.Speakers.Remove(speaker);
                var userId = User.Identity.GetUserId();
                var userSpeakerGroup = db.UserSpeakerGroups.SingleOrDefault(w => w.SpeakerGroupUid == speaker.SpeakerGroupUid && w.UserUid == userId);
                if (userSpeakerGroup != null)
                {
                    var area = userSpeakerGroup.Area;
                    area.UserModified = userId;
                    area.TimeModified = DateTime.Now;
                }
                db.SaveChangesAsync();
            }

            return Ok(speaker);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SpeakerExists(Guid id)
        {
            return db.Speakers.Count(e => e.Uid == id) > 0;
        }

        [HttpGet]
        public List<ApiSpeaker> SpeakerExists(string speakers)
        {
            var speakersToConvertToJSON = LradAccessManager.ConvertDataToJArray(speakers);
            return LradAccessManager.SpeakerExists(speakersToConvertToJSON.ToObject<List<ApiSpeaker>>());
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetDiscoveredSpeakers()
        {
            try
            {
                return Ok(await LradAccessManager.GetDiscoveredSpeakers());
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> Play(List<PlayDevice> devices)
        {
            try
            {
                var result = await LradAccessManager.Play(devices);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> Stop(List<PlayDevice> devices)
        {
            try
            {
            return Ok(await LradAccessManager.Stop(devices));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetAllLradDevices()
        {
            try
            {
                return Ok(await LradAccessManager.GetAllSpeakers());
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetDeviceById(Guid id)
        {
            try
            {
                return Ok(await LradAccessManager.GetSpeakersById(id));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}