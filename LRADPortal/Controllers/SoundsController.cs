﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LRADPortal;
using LRADPortal.Models;
using Microsoft.AspNet.Identity;
using System.Windows.Forms;

namespace LRADPortal.Controllers
{
    [Authorize]
    [RoutePrefix("api/Sounds")]
    public class SoundsController : ApiController
    {
        private LRADMNSEntities db = new LRADMNSEntities();

        // GET: api/Sounds
        public IHttpActionResult GetSounds()
        {
            var list = db.Sounds.Select(s => new
            {
                Uid = s.Uid,
                Name = s.Name.Trim(),
                Filename = s.Filename.Trim(),
                Type = s.Type
            }).ToList();

            return Ok(list);
        }

        // GET: api/Sounds/GetGeneralSounds
        [Route("GetGeneralSounds")]
        [HttpGet]
        public IHttpActionResult GetGeneralSounds()
        {
            var list = db.Sounds.Select(s => new
            {
                Uid = s.Uid,
                Name = s.Name,
                Filename = s.Filename,
                Type = s.Type
            }).Where(t => t.Type == 0).ToList();

            return Ok(list);
        }

        // GET: api/Sounds/GetEmergencySounds
        [Route("GetEmergencySounds")]
        [HttpGet]
        public IHttpActionResult GetEmergencySounds()
        {
            var list = db.Sounds.Select(s => new
            {
                Uid = s.Uid,
                Name = s.Name,
                Filename = s.Filename,
                Type = s.Type
            }).Where(t => t.Type == 1).ToList();

            return Ok(list);
        }

        // GET: api/Sounds/GetCustomSounds
        [Route("GetCustomSounds")]
        [HttpGet]
        public IHttpActionResult GetCustomSounds(int type)
        {
            var list = db.Sounds.Select(s => new {
                Uid = s.Uid,
                Name = s.Name,
                Filename = s.Filename,
                Type = s.Type
            }).Where(t => t.Type == type).ToList();

            return Ok(list);
        }

        protected void uploadFile(string file)
        {
            var path = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Uploads/Sounds/");
            System.IO.Directory.CreateDirectory(path);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FileName = file;

            Boolean trueFile = new Boolean();
            trueFile = String.IsNullOrEmpty(file);
            if (!trueFile)
            {
                bool doesSoundExist = File.Exists(path + file) ? true : false;
                if (!doesSoundExist)
                {
                    Stream s = File.OpenWrite(path + file);
                }
            }
        }

        // POST api/speakergroups/AddNewGroup
        [Route("AddNewSounds")]
        [HttpPost]
        public IHttpActionResult AddNewSounds(List<Sound> data)
        {
            try
            {
                string userId = User.Identity.GetUserId();

                data.ForEach(s =>
                {
                    var newSound = new Sound()
                    {
                        Uid = Guid.NewGuid(),
                        Name = s.Name,
                        Filename = s.Filename,
                        Type = (short)s.Type,
                        Description = s.Description,
                        UserCreated = userId,
                        TimeCreated = DateTime.Now,
                        UserModified = userId,
                        TimeModified = DateTime.Now
                    };

                    db.Sounds.Add(newSound);

                    Boolean fileName = new Boolean();
                    fileName = String.IsNullOrEmpty(s.Filename);
                    string newFileName = s.Filename;

                    if (!fileName)
                    {
                        this.uploadFile(newFileName);
                    }
                });

                db.SaveChanges();

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET: api/Sounds/5
        [ResponseType(typeof(Sound))]
        public async Task<IHttpActionResult> GetSound(Guid id)
        {
            Sound sound = await db.Sounds.FindAsync(id);
            if (sound == null)
            {
                return NotFound();
            }

            return Ok(sound);
        }

        // GET: api/Sounds/GetSoundApi
        [Route("GetSoundApi")]
        [HttpGet]
        public IHttpActionResult GetSoundApi(Guid id)
        {
            var result = db.Sounds.Select(s => new
            {
                Uid = s.Uid,
                Name = s.Name.Trim(),
                Filename = s.Filename.Trim(),
                Type = s.Type
            }).Where(a => a.Uid == id);

            return Ok(result);
        }

        // PUT: api/Sounds/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSound(Guid id, Sound sound)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sound.Uid)
            {
                return BadRequest();
            }

            db.Entry(sound).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SoundExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sounds
        [ResponseType(typeof(Sound))]
        public async Task<IHttpActionResult> PostSound(Sound sound)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Sounds.Add(sound);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SoundExists(sound.Uid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = sound.Uid }, sound);
        }

        // DELETE: api/Sounds/5
        //[ResponseType(typeof(Sound))]
        //public async Task<IHttpActionResult> DeleteSound(Guid id)
        //{
        //    Sound sound = await db.Sounds.FindAsync(id);
        //    if (sound == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Sounds.Remove(sound);
        //    await db.SaveChangesAsync();

        //    return Ok(sound);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SoundExists(Guid id)
        {
            return db.Sounds.Count(e => e.Uid == id) > 0;
        }

        // GET: api/Sounds
        [HttpGet]
        public IHttpActionResult SoundManagement()
        {
            var soundList = db.Sounds.Join(db.AspNetUsers, S => S.UserCreated, U => U.Id, (S, U) => new
            {
                S,
                UserCreated = U.UserInfo.FirstName + " " + U.UserInfo.LastName
            }).Join(db.AspNetUsers, S => S.S.UserModified, U => U.Id, (S, U) => new
            {
                S.S.Uid,
                S.S.Name,
                S.S.Type,
                S.S.Filename,
                S.S.Description,
                S.S.TimeCreated,
                S.S.TimeModified,
                S.UserCreated,
                UserModifed = U.UserInfo.FirstName + " " + U.UserInfo.LastName
            });

            return Ok(soundList);
        }

        // GET: api/SoundTypes
        [HttpGet]
        public IHttpActionResult SoundTypes()
        {
            var soundTypes = db.SoundTypes.Select(s => new
            {
                Id = s.Id,
                Name = s.Name.Trim()
            }).ToList();

            return Ok(soundTypes);
        }
    }
}