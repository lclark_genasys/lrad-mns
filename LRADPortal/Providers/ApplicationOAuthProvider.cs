﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using LRADPortal.Models;

namespace LRADPortal.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }
            
            //Check if the user is avtive or inactive
            UserStatus status = UserStatus.Inactive;
            UserInfo info = null;

            using (var db = new LRADMNSEntities())
            {
                var aspUser = db.AspNetUsers.FirstOrDefault(i => i.UserName == context.UserName);
                if (aspUser != null)
                {
                    info = aspUser.UserInfo;
                    status = (UserStatus)aspUser.UserInfo.Status;
                }
            }

            if (status == UserStatus.Inactive)
            {
                context.SetError("invalid_grant", "This user is not active. Please see the administrator.");
                return;
            }


            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
                CookieAuthenticationDefaults.AuthenticationType);

            AuthenticationProperties properties = CreateProperties(user.UserName, info);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {

            context.Properties.ExpiresUtc = DateTime.UtcNow.AddDays(1);

            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },
            };
            return new AuthenticationProperties(data);
        }

        public static AuthenticationProperties CreateProperties(string userName, UserInfo info)
        {
            string firstName = (info == null) ? string.Empty : info.FirstName;
            string lastName = (info == null) ? string.Empty : info.LastName;
            string accessLevel = (info == null) ? "1" : info.AcessLevel.ToString();

            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },
                { "firstName", firstName },
                { "lastName", lastName },
                { "accessLevel", accessLevel}
            };
            return new AuthenticationProperties(data);
        }

    }
}