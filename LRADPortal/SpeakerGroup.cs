//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LRADPortal
{
    using System;
    using System.Collections.Generic;
    
    public partial class SpeakerGroup
    {
        public SpeakerGroup()
        {
            this.SoundSpeakerGroups = new HashSet<SoundSpeakerGroup>();
            this.UserSpeakerGroups = new HashSet<UserSpeakerGroup>();
            this.Speakers = new HashSet<Speaker>();
        }
    
        public System.Guid Uid { get; set; }
        public System.Guid AreaUid { get; set; }
        public string MultiCastGroup { get; set; }
        public string Color { get; set; }
        public System.DateTime TimeCreated { get; set; }
        public string UserCreated { get; set; }
        public System.DateTime TimeModified { get; set; }
        public string UserModified { get; set; }
        public Nullable<double> LabelLatitude { get; set; }
        public Nullable<double> LabelLongitude { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<SoundSpeakerGroup> SoundSpeakerGroups { get; set; }
        public virtual ICollection<UserSpeakerGroup> UserSpeakerGroups { get; set; }
        public virtual ICollection<Speaker> Speakers { get; set; }
        public virtual Area Area { get; set; }
    }
}
