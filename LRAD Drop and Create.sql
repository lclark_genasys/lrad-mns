USE [master]
GO

IF EXISTS (SELECT name FROM sys.databases WHERE name = N'LRADMNS')
BEGIN
ALTER DATABASE [LRADMNS] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
DROP DATABASE [LRADMNS] ;
END
GO

/****** Object:  Database [LRADMNS]    Script Date: 6/29/2017 7:13:43 PM ******/
CREATE DATABASE [LRADMNS]
GO

ALTER DATABASE [LRADMNS] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LRADMNS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LRADMNS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LRADMNS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LRADMNS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LRADMNS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LRADMNS] SET ARITHABORT OFF 
GO
ALTER DATABASE [LRADMNS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LRADMNS] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [LRADMNS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LRADMNS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LRADMNS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LRADMNS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LRADMNS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LRADMNS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LRADMNS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LRADMNS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LRADMNS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LRADMNS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LRADMNS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LRADMNS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LRADMNS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LRADMNS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LRADMNS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LRADMNS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LRADMNS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LRADMNS] SET  MULTI_USER 
GO
ALTER DATABASE [LRADMNS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LRADMNS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LRADMNS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LRADMNS] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

USE [LRADMNS]
GO
ALTER TABLE [dbo].[UserSpeakerGroup] DROP CONSTRAINT [FK_UserSpeakerGroup_SpeakerGroup]
GO
ALTER TABLE [dbo].[UserSpeakerGroup] DROP CONSTRAINT [FK_UserSpeakerGroup_AspNetUsers]
GO
ALTER TABLE [dbo].[UserSpeakerGroup] DROP CONSTRAINT [FK_UserSpeakerGroup_Area]
GO
ALTER TABLE [dbo].[SpeakerGroup] DROP CONSTRAINT [FK_SpeakerGroup_Area]
GO
ALTER TABLE [dbo].[SpeakerAlerts] DROP CONSTRAINT [FK_SpeakerAlerts_Speaker]
GO
ALTER TABLE [dbo].[Speaker] DROP CONSTRAINT [FK_Speaker_SpeakerGroup]
GO
ALTER TABLE [dbo].[Speaker] DROP CONSTRAINT [FK_Speaker_Sound]
GO
ALTER TABLE [dbo].[Speaker] DROP CONSTRAINT [FK_Speaker_PredefinedSound]
GO
ALTER TABLE [dbo].[Speaker] DROP CONSTRAINT [FK_Speaker_Play_Mode]
GO
ALTER TABLE [dbo].[Speaker] DROP CONSTRAINT [FK_Speaker_0]
GO
ALTER TABLE [dbo].[SoundSpeakerGroup] DROP CONSTRAINT [FK_SoundSpeakerGroup_SpeakerGroup]
GO
ALTER TABLE [dbo].[SoundSpeakerGroup] DROP CONSTRAINT [FK_SoundSpeakerGroup_Sound1]
GO
ALTER TABLE [dbo].[Sound] DROP CONSTRAINT [FK_Sound_SoundType]
GO
ALTER TABLE [dbo].[AspNetUsers] DROP CONSTRAINT [FK_dbo.AspNetUsers_dbo.UserInfoes_UserInfo_Uid]
GO
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AreaImage] DROP CONSTRAINT [FK_AreaImage_Area]
GO
/****** Object:  Table [dbo].[UserSpeakerGroup]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[UserSpeakerGroup]
GO
/****** Object:  Table [dbo].[UserInfoes]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[UserInfoes]
GO
/****** Object:  Table [dbo].[SpeechToText]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[SpeechToText]
GO
/****** Object:  Table [dbo].[SpeakerType]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[SpeakerType]
GO
/****** Object:  Table [dbo].[SpeakerState]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[SpeakerState]
GO
/****** Object:  Table [dbo].[SpeakerGroup]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[SpeakerGroup]
GO
/****** Object:  Table [dbo].[SpeakerAlerts]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[SpeakerAlerts]
GO
/****** Object:  Table [dbo].[Speaker]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[Speaker]
GO
/****** Object:  Table [dbo].[SoundType]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[SoundType]
GO
/****** Object:  Table [dbo].[SoundSpeakerGroup]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[SoundSpeakerGroup]
GO
/****** Object:  Table [dbo].[Sound]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[Sound]
GO
/****** Object:  Table [dbo].[PredefinedSound]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[PredefinedSound]
GO
/****** Object:  Table [dbo].[PlayMode]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[PlayMode]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[AspNetUsers]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[AspNetUserRoles]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[AspNetUserLogins]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[AspNetUserClaims]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[AspNetRoles]
GO
/****** Object:  Table [dbo].[AreaImage]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[AreaImage]
GO
/****** Object:  Table [dbo].[Area]    Script Date: 6/29/2017 1:01:10 PM ******/
DROP TABLE [dbo].[Area]
GO
/****** Object:  Table [dbo].[Area]    Script Date: 6/29/2017 1:01:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Area](
	[Uid] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Area__Uid__0CBAE877]  DEFAULT (newid()),
	[TimeCreated] [datetime] NOT NULL CONSTRAINT [DF__Area__TimeCreate__0DAF0CB0]  DEFAULT (getdate()),
	[UserCreated] [nvarchar](128) NULL,
	[TimeModified] [datetime] NOT NULL CONSTRAINT [DF__Area__TimeModifi__0EA330E9]  DEFAULT (getdate()),
	[UserModified] [nvarchar](128) NULL,
	[Name] [nvarchar](150) NULL,
	[CenterLatitude] [float] NULL,
	[CenterLongitude] [float] NULL,
	[Zoom] [int] NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[AreaImage]    Script Date: 6/29/2017 1:01:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AreaImage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaUid] [uniqueidentifier] NOT NULL,
	[LowerLeftLatitude] [float] NOT NULL,
	[LowerLeftLongitude] [float] NOT NULL,
	[UpperRightLatitude] [float] NOT NULL,
	[UpperRightLongitude] [float] NOT NULL,
	[ImageFilename] [text] NOT NULL,
 CONSTRAINT [PK_AreaImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 6/29/2017 1:01:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 6/29/2017 1:01:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 6/29/2017 1:01:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 6/29/2017 1:01:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 6/29/2017 1:01:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[UserInfo_Uid] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[PlayMode]    Script Date: 6/29/2017 1:01:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayMode](
	[Id] [tinyint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[PredefinedSound]    Script Date: 6/29/2017 1:01:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PredefinedSound](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Sound]    Script Date: 6/29/2017 1:01:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sound](
	[Uid] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Sound__Uid__108B795B]  DEFAULT (newid()),
	[Name] [nchar](50) NOT NULL,
	[Type] [smallint] NOT NULL,
	[Description] [nchar](256) NOT NULL,
	[Filename] [nchar](256) NOT NULL,
	[Text] [ntext] NULL,
	[TimeCreated] [datetime] NOT NULL CONSTRAINT [DF__Sound__TimeCreat__117F9D94]  DEFAULT (getdate()),
	[UserCreated] [nvarchar](128) NOT NULL,
	[TimeModified] [datetime] NOT NULL CONSTRAINT [DF__Sound__TimeModif__1273C1CD]  DEFAULT (getdate()),
	[UserModified] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Sound] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[SoundSpeakerGroup]    Script Date: 6/29/2017 1:01:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoundSpeakerGroup](
	[Uid] [uniqueidentifier] NOT NULL CONSTRAINT [DF__SoundSpeake__Uid__1367E606]  DEFAULT (newid()),
	[SoundUid] [uniqueidentifier] NOT NULL,
	[SpeakerGroupUid] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_SoundSpeakerGroup] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[SoundType]    Script Date: 6/29/2017 1:01:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoundType](
	[Id] [smallint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Speaker]    Script Date: 6/29/2017 1:01:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Speaker](
	[Uid] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Speaker__Uid__145C0A3F]  DEFAULT (newid()),
	[SpeakerGroupUid] [uniqueidentifier] NULL,
	[IPAddress] [nvarchar](45) NULL,
	[SatelliteID] [nchar](50) NULL,
	[Type] [tinyint] NOT NULL,
	[GeneralVolume] [float] NOT NULL,
	[EmergencyVolume] [float] NOT NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[TimeCreated] [datetime] NOT NULL CONSTRAINT [DF__Speaker__TimeCre__15502E78]  DEFAULT (getdate()),
	[UserCreated] [nvarchar](128) NULL,
	[TimeModified] [datetime] NOT NULL CONSTRAINT [DF__Speaker__TimeMod__164452B1]  DEFAULT (getdate()),
	[UserModified] [nvarchar](128) NULL,
	[Name] [nvarchar](150) NULL,
	[SoundUid] [uniqueidentifier] NULL,
	[RepeatTimes] [int] NULL CONSTRAINT [ColumnDefault_c104ef43-f5a0-48c3-9cfe-100270681490]  DEFAULT ((1)),
	[StartTime] [datetime] NULL,
	[PlayMode] [tinyint] NULL,
	[TTSUid] [uniqueidentifier] NULL,
	[MacAddress] [nvarchar](20) NULL,
	[NetworkName] [nvarchar](150) NULL,
	[PredefinedSound] [int] NULL DEFAULT ((1)),
	[Repeat] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_Speaker] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[SpeakerAlerts]    Script Date: 6/29/2017 1:01:13 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpeakerAlerts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpeakerUid] [uniqueidentifier] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Datetime] [datetime] NOT NULL,
 CONSTRAINT [PrimaryKey_7983c12e-cc0c-4919-966d-42c69821ab10] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[SpeakerGroup]    Script Date: 6/29/2017 1:01:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SpeakerGroup](
	[Uid] [uniqueidentifier] NOT NULL CONSTRAINT [DF__SpeakerGrou__Uid__173876EA]  DEFAULT (newid()),
	[AreaUid] [uniqueidentifier] NOT NULL,
	[MultiCastGroup] [nchar](50) NULL,
	[Color] [char](7) NULL,
	[TimeCreated] [datetime] NOT NULL CONSTRAINT [DF__SpeakerGr__TimeC__182C9B23]  DEFAULT (getdate()),
	[UserCreated] [nvarchar](128) NULL,
	[TimeModified] [datetime] NOT NULL CONSTRAINT [DF__SpeakerGr__TimeM__1920BF5C]  DEFAULT (getdate()),
	[UserModified] [nvarchar](128) NULL,
	[LabelLatitude] [float] NULL,
	[LabelLongitude] [float] NULL,
	[Name] [nvarchar](150) NULL,
 CONSTRAINT [PK_SpeakerGroup] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SpeakerState]    Script Date: 6/29/2017 1:01:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpeakerState](
	[Uid] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[SpeakerUid] [uniqueidentifier] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[AmpTemp] [float] NULL,
	[BatteryLevel] [float] NULL,
	[Door] [smallint] NULL,
	[Connection] [smallint] NULL,
	[Activity] [smallint] NULL,
	[Status] [smallint] NULL,
	[SatelliteStatus] [smallint] NULL,
 CONSTRAINT [PK_SpeakerState] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[SpeakerType]    Script Date: 6/29/2017 1:01:14 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpeakerType](
	[Id] [tinyint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[MaxCoverageRadius] [int] NOT NULL,
 CONSTRAINT [PrimaryKey_1bdfbcb5-f56f-44f2-88a2-792fa59c867f] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[SpeechToText]    Script Date: 6/29/2017 1:01:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpeechToText](
	[Uid] [uniqueidentifier] NOT NULL,
	[Body] [nvarchar](1024) NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[UserCreated] [nvarchar](128) NOT NULL,
	[Title] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[UserInfoes]    Script Date: 6/29/2017 1:01:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInfoes](
	[Uid] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[AcessLevel] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[TimeCreated] [datetime] NULL,
	[UserCreated] [nvarchar](128) NULL,
	[TimeModified] [datetime] NULL,
	[UserModified] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.UserInfoes] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[UserSpeakerGroup]    Script Date: 6/29/2017 1:01:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSpeakerGroup](
	[Uid] [uniqueidentifier] NOT NULL CONSTRAINT [DF__UserSpeaker__Uid__1CF15040]  DEFAULT (newid()),
	[UserUid] [nvarchar](128) NOT NULL,
	[AreaUid] [uniqueidentifier] NOT NULL,
	[SpeakerGroupUid] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_UserSpeakerGroup] PRIMARY KEY CLUSTERED 
(
	[Uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
ALTER TABLE [dbo].[AreaImage]  WITH CHECK ADD  CONSTRAINT [FK_AreaImage_Area] FOREIGN KEY([AreaUid])
REFERENCES [dbo].[Area] ([Uid])
GO
ALTER TABLE [dbo].[AreaImage] CHECK CONSTRAINT [FK_AreaImage_Area]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUsers_dbo.UserInfoes_UserInfo_Uid] FOREIGN KEY([UserInfo_Uid])
REFERENCES [dbo].[UserInfoes] ([Uid])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_dbo.AspNetUsers_dbo.UserInfoes_UserInfo_Uid]
GO
ALTER TABLE [dbo].[Sound]  WITH CHECK ADD  CONSTRAINT [FK_Sound_SoundType] FOREIGN KEY([Type])
REFERENCES [dbo].[SoundType] ([Id])
GO
ALTER TABLE [dbo].[Sound] CHECK CONSTRAINT [FK_Sound_SoundType]
GO
ALTER TABLE [dbo].[SoundSpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_SoundSpeakerGroup_Sound1] FOREIGN KEY([SoundUid])
REFERENCES [dbo].[Sound] ([Uid])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SoundSpeakerGroup] CHECK CONSTRAINT [FK_SoundSpeakerGroup_Sound1]
GO
ALTER TABLE [dbo].[SoundSpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_SoundSpeakerGroup_SpeakerGroup] FOREIGN KEY([SpeakerGroupUid])
REFERENCES [dbo].[SpeakerGroup] ([Uid])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SoundSpeakerGroup] CHECK CONSTRAINT [FK_SoundSpeakerGroup_SpeakerGroup]
GO
ALTER TABLE [dbo].[Speaker]  WITH CHECK ADD  CONSTRAINT [FK_Speaker_0] FOREIGN KEY([Type])
REFERENCES [dbo].[SpeakerType] ([Id])
GO
ALTER TABLE [dbo].[Speaker] CHECK CONSTRAINT [FK_Speaker_0]
GO
ALTER TABLE [dbo].[Speaker]  WITH CHECK ADD  CONSTRAINT [FK_Speaker_Play_Mode] FOREIGN KEY([PlayMode])
REFERENCES [dbo].[PlayMode] ([Id])
GO
ALTER TABLE [dbo].[Speaker] CHECK CONSTRAINT [FK_Speaker_Play_Mode]
GO
ALTER TABLE [dbo].[Speaker]  WITH CHECK ADD  CONSTRAINT [FK_Speaker_PredefinedSound] FOREIGN KEY([PredefinedSound])
REFERENCES [dbo].[PredefinedSound] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Speaker] CHECK CONSTRAINT [FK_Speaker_PredefinedSound]
GO
ALTER TABLE [dbo].[Speaker]  WITH CHECK ADD  CONSTRAINT [FK_Speaker_Sound] FOREIGN KEY([SoundUid])
REFERENCES [dbo].[Sound] ([Uid])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Speaker] CHECK CONSTRAINT [FK_Speaker_Sound]
GO
ALTER TABLE [dbo].[Speaker]  WITH CHECK ADD  CONSTRAINT [FK_Speaker_SpeakerGroup] FOREIGN KEY([SpeakerGroupUid])
REFERENCES [dbo].[SpeakerGroup] ([Uid])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Speaker] CHECK CONSTRAINT [FK_Speaker_SpeakerGroup]
GO
ALTER TABLE [dbo].[SpeakerAlerts]  WITH CHECK ADD  CONSTRAINT [FK_SpeakerAlerts_Speaker] FOREIGN KEY([SpeakerUid])
REFERENCES [dbo].[Speaker] ([Uid])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SpeakerAlerts] CHECK CONSTRAINT [FK_SpeakerAlerts_Speaker]
GO
ALTER TABLE [dbo].[SpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_SpeakerGroup_Area] FOREIGN KEY([AreaUid])
REFERENCES [dbo].[Area] ([Uid])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SpeakerGroup] CHECK CONSTRAINT [FK_SpeakerGroup_Area]
GO
ALTER TABLE [dbo].[UserSpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserSpeakerGroup_Area] FOREIGN KEY([AreaUid])
REFERENCES [dbo].[Area] ([Uid])
GO
ALTER TABLE [dbo].[UserSpeakerGroup] CHECK CONSTRAINT [FK_UserSpeakerGroup_Area]
GO
ALTER TABLE [dbo].[UserSpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserSpeakerGroup_AspNetUsers] FOREIGN KEY([UserUid])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[UserSpeakerGroup] CHECK CONSTRAINT [FK_UserSpeakerGroup_AspNetUsers]
GO
ALTER TABLE [dbo].[UserSpeakerGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserSpeakerGroup_SpeakerGroup] FOREIGN KEY([SpeakerGroupUid])
REFERENCES [dbo].[SpeakerGroup] ([Uid])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserSpeakerGroup] CHECK CONSTRAINT [FK_UserSpeakerGroup_SpeakerGroup]
GO
INSERT [dbo].[UserInfoes] ([Uid], [FirstName], [LastName], [AcessLevel], [Status], [TimeCreated], [UserCreated], [TimeModified], [UserModified]) VALUES (N'5e419330-5e0b-4500-8cc3-a27a47c5fb83', N'Jason', N'Wright', 0, 0, CAST(N'2017-06-29 12:45:47.897' AS DateTime), N'2017-06-29 12:45:47.8982156', CAST(N'2017-06-29 12:45:47.897' AS DateTime), N'2017-06-29 12:45:47.8982156')
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [UserInfo_Uid]) VALUES (N'7232f57e-faf1-407e-8b09-7061250b0ffa', N'jasonwright@lradx.com', 0, N'AC2hgmVlcYF19fPX+dfq20ej+ztRp4HtgEXpViGGtHiE8m6uVnbjoKDiSnv0skDqqg==', N'6b9cc36c-ed85-4046-b13d-3a76dfdb39e1', N'(858) 676-0575', 0, 0, NULL, 0, 0, N'jasonwright@lradx.com', N'5e419330-5e0b-4500-8cc3-a27a47c5fb83')
GO
INSERT [dbo].[PlayMode] ([Id], [Name]) VALUES (0, N'General')
GO
INSERT [dbo].[PlayMode] ([Id], [Name]) VALUES (1, N'Emergency')
GO
INSERT [dbo].[PlayMode] ([Id], [Name]) VALUES (2, N'Speech To Text')
GO
INSERT [dbo].[PlayMode] ([Id], [Name]) VALUES (3, N'Microphone')
GO
INSERT [dbo].[PlayMode] ([Id], [Name]) VALUES (4, N'OFF')
GO
SET IDENTITY_INSERT [dbo].[PredefinedSound] ON 

GO
INSERT [dbo].[PredefinedSound] ([Id], [Name]) VALUES (1, N'General')
GO
INSERT [dbo].[PredefinedSound] ([Id], [Name]) VALUES (2, N'General Custom')
GO
INSERT [dbo].[PredefinedSound] ([Id], [Name]) VALUES (3, N'Emergency')
GO
INSERT [dbo].[PredefinedSound] ([Id], [Name]) VALUES (4, N'Emergency Custom')
GO
SET IDENTITY_INSERT [dbo].[PredefinedSound] OFF
GO
INSERT [dbo].[SoundType] ([Id], [Name]) VALUES (0, N'General')
GO
INSERT [dbo].[SoundType] ([Id], [Name]) VALUES (1, N'Emergency')
GO
INSERT [dbo].[SoundType] ([Id], [Name]) VALUES (2, N'General Custom')
GO
INSERT [dbo].[SoundType] ([Id], [Name]) VALUES (3, N'Emergency Custom')
GO
INSERT [dbo].[SpeakerType] ([Id], [Name], [MaxCoverageRadius]) VALUES (0, N'360X Single', 560)
GO
INSERT [dbo].[SpeakerType] ([Id], [Name], [MaxCoverageRadius]) VALUES (1, N'360X Double', 850)
GO
INSERT [dbo].[SpeakerType] ([Id], [Name], [MaxCoverageRadius]) VALUES (2, N'360X Quad', 1300)
GO
INSERT [dbo].[SpeakerType] ([Id], [Name], [MaxCoverageRadius]) VALUES (3, N'360X Eight', 2000)
GO
INSERT [dbo].[SpeakerType] ([Id], [Name], [MaxCoverageRadius]) VALUES (4, N'360Xm Single', 250)
GO
INSERT [dbo].[SpeakerType] ([Id], [Name], [MaxCoverageRadius]) VALUES (5, N'360Xm Double', 450)
GO
